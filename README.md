# SMEFTgen

[[_TOC_]]

Collection of scripts to prepare ATHENA JOs for SMEFT sample gen

## Setting up 

```
git clone https://gitlab.cern.ch/tiduong/SMEFTgen.git
cd SMEFTgen && mkdir build && cd build && cmake .. && source setup.sh && cd ../;
```

When using the package in a fresh session don't forget to setup by, 
```
source build/setup.sh
```

## Generating EFT parameterisation : simple example

The script `scripts/genJO.py` takes care of preparing the JOs for different parameters and 
production modes and if needed across different number of jobs. For instance, this line 
can be used to generate the WH jobs for 3 parameters where each jobs runs 10k samples per 
parameter.

If you are generating EFT samples by default it will generate the SM-EFT interference, EFT square term or 
EFT cross term. To disable some of the contributions you can make use of the dedicated flags,
- ```--no-interference-terms```
- ```--no-square-terms```
- ```--no-cross-terms```

For instance, this should give you the interfernce terms of the Wilson coefficents with the Standard Model and 
additionally providing SM in the ```--coefficients``` flag allows to obtain the SM contribution.
```
python scripts/genJO.py \
    --out EVNT_outdir  \
    --prodmodes WH  \
    --run-production \
    --coefficients cHl3 cHDD cHW SM \
    --symmetry topU3l  \
    --n-jobs 10  \
    --writeSubmit bsub  \
    --jobFile out_WH_topU3l.sh \
    --interference-terms \
    --no-square-terms \
    --no-cross-terms \
    --standalone \
    --nevents 10
```

For other options you can take a look at the flags by running 
```
python scripts/genJO.py --help
```

The EVNs are further processed with rivet. If you have split the generation in in jobs, then 
you would need to merge the rivet yoda files adding this flag to the genJO line ```--mergeRivet path/to/output/folder```.

Once this is finished you can merge the yoda files and convert it into the recommended ```yaml``` format as showing in 
the following example,

```
python2 scripts/extract_param.py --input-folder gen/ttH --output out.yaml
```

where, the ```--input-folder``` points to the input folder with the merged yoda files, for instance the contents can be something 
along the lines of,
```
SM.yoda
cHl311.yoda
cHl322.yoda
cll1221.yoda
```
Where the SM is used to normalise the EFT variation.


## Finding all parameters affecting your process

It is useful to be able to loop over all wilson coefficients to identify the subset affecting your process. This will 
allow to focus the generation on the smaller set of parameters

This can be launched by either providing an empty ```--coefficients``` flag or not giving one altogether. This is an 
example based on 4-Higgs production mode for the top scheme.

```
python scripts/genJO.py --out EVNT_outdir  --prodmodes WH  --run-production --symmetry top  --n-jobs 1  --writeSubmit bsub  --jobFile out_WH_top.sh --interference-terms --no-square-terms --no-cross-terms --nevents 1000 --standalone
python scripts/genJO.py --out EVNT_outdir  --prodmodes ZH  --run-production --symmetry top  --n-jobs 1  --writeSubmit bsub  --jobFile out_ZH_top.sh --interference-terms --no-square-terms --no-cross-terms --nevents 1000 --standalone
python scripts/genJO.py --out EVNT_outdir  --prodmodes VBF  --run-production --symmetry top  --n-jobs 1  --writeSubmit bsub  --jobFile out_VBF_top.sh --interference-terms --no-square-terms --no-cross-terms --nevents 1000 --standalone
python scripts/genJO.py --out EVNT_outdir  --prodmodes TTH  --run-production --symmetry top  --n-jobs 1  --writeSubmit bsub  --jobFile out_TTH_top.sh --interference-terms --no-square-terms --no-cross-terms --nevents 1000 --standalone
```

You can quickly check the parameters which have an effecting by greppping for ```Cross-section``` over the logs,
```
for x in EVNT_outdir/TTH/*int*/test/log.generate; do grep -r "Cross-section" $x && echo $x >> tth.log ; done;
for x in EVNT_outdir/WH/*int*/test/log.generate; do grep -r "Cross-section" $x && echo $x >> wh.log ; done;
for x in EVNT_outdir/ZH/*int*/test/log.generate; do grep -r "Cross-section" $x && echo $x >> zh.log ; done;
for x in EVNT_outdir/VBF/*int*/test/log.generate; do grep -r "Cross-section" $x && echo $x >> vbf.log ; done;
```

You can also directly store this information and the corresponding cross-section using,
```
# python scripts/run_diagnostics.py .. .. .(To be added)
```

## Adding custom process and settings

It is possible to customize the process and the Madgraph settings by providing a dedicated input file. For instance, provide a json file ```inputs.json``` with the following contents,
```
{
  "process name": "DY neutral current ee",
  "process": "generate p p > e+ e- {NPprop} {NPsqtag}",
  "settings": {
    "drll": 0.05,
    "ptj": 20,
    "ptl": 0.01
  },
  "pythia8": "genSeq.Pythia8.Commands += [' PartonLevel:MPI = off']\ngenSeq.Pythia8.Commands += [' SpaceShower:QEDshowerByL = off']\n",
  "metadata": {
    "description": "'Madgraph SMEFT for DY'",
    "contact email": "['xyz@cern.ch']",
    "keywords": "['DY', 'QED', 'EFT']",
    "description": "'DY neutral current ee production with QED showering and EFT effects.'",
    "generators": "['MadGraph5_aMC@NLO', 'Pythia8']"
  }
}
```

can be used to configure the desired process as follows,
```
python scripts/genJO.py --out EVNT_outdir --symmetry top  --n-jobs 5  --writeSubmit bsub  --jobFile run.sh --interference-terms --no-square-terms --no-cross-terms --nevents 1000 --standalone --custom-input inputs.json
```

## Custom Rivet routine

You can specify you Rivet routine by adding ```--rivet-file MY_ROUTINE``` the routine must be placed in  ̀``/SMEFTgen/rivet/`` 

exemple :
```
python scripts/genJO.py --out EVNT_outdir --symmetry U35  --n-jobs 1  --writeSubmit bsub  --jobFile U35.sh --interference-terms --no-square-terms --no-cross-terms --nevents 10000 --standalone --custom-input inputs.json --rivet-file MY_ANALYSIS
```

## Run the generation

The genJO scripts creates a .sh you can run, you can use ``convertToCERNBatch.py`` to run it on lxplus7 :

```
python convertToCERNBatch.py runfile.sh name
```
This will create .sh for every poi, in ``SMEFTgen/batch_submission/runfile/`` where you can submit to the batch or run interactively.

Running the .sh will create a yoda file in ```SMEFTgen/EVNT_outdir/U35_DY_neutral_current_ee/poi_name/test/poi.yoda```

Also converted to a Root file (useful for cross-check) with ```SMEFTgen/scripts/yoda2root.py```

## Cross check

You can compare the 1D m_ll distribution with Ricardo's old samples:

```
python Norm_Ratio.py path/to/old/sample path/to/your/sample name
```
This creates a directory ```SMEFTgen/Cross_check_name/```

## Reweighting vs. standalone generation

You can configure between generating separate variations or directly storing the weights 
of the matrix elements using the reweighting method, by using either the ```--standalone``` 
vs ```--re-weighting``` flag.

Note that currently ```--re-weighting``` option supports the generation of interference, 
quadratic, and cross-terms separately.

For the case of generating cross-terms by activating the flag, ```--cross-terms```, it is 
instructive to run this over separately jobs as the number of weights can be prohibitively 
large for a single reweighting jobs. To be able to generate the corresponding ```genJO.py``` 
lines to split the cross-term re-weighting, please provide the option 
```--out-cross-term-lines genJO_splitlines.sh ```.


## STXS parameterisation merging

To merge existing yaml files of the parameterisation weighted by the cross-section, you can 
make use of the following script 
```
python scripts/get_merged_parameterisation.py --input-param fine.yaml --merging-map map.json --output coarse.yaml
```

Here ```map.json``` is a map that defines the merging based on the bin-names,
```
{
    "fine_bin0": "coarse_bin0",
    "fine_bin1": "coarse_bin0",
    "fine_bin2": "coarse_bin1",
}
```


