import glob, os
from SMEFTgen.stxs_defs import *
from SMEFTgen.param_helpers import *
import pprint
import math, sys
import yaml

#offset = [0,1,18,29,35,41,47,53,55,57]
#offset=[0,1,29,54,70,86,102,109,111,113]
#stxsdict = stxsdict_fine 

def extractParam(args):

    files = glob.glob(args.infolder+"/*.*") 
    print(files)
    debug = args.debug
    c_factor = 1.0/args.coeff_val
    print("Interference terms provided for c = {0}".format(args.coeff_val))
    apply_int_map = args.applyintmap
    # reqd info. for translating the STXStag to binpos. of RIVET histo.

    histoname = args.histoname
    if len(args.name) == 0:
        args.name = args.histoname

    vals = {}#stxsbin:{} for stxsbin in stxsdict_fine}
    for filename in files:
        print("Reading {0}...".format(filename))
        print(filename)
        if filename.endswith(".root"):
            if args.reweight:
                vals_sample = readvals_from_ROOT_reweight(filename,histoname,apply_int_map,debug)
            else: 
                vals_sample = readvals_from_ROOT(filename,histoname,apply_int_map,debug)

        elif filename.endswith(".yoda"): 
            if args.reweight:
                vals_sample = readvals_from_yoda_reweight(filename,histoname,apply_int_map,debug)
            else:
                vals_sample = readvals_from_yoda(filename,histoname,apply_int_map,stxs_dict = {},debug=debug)

        else:
            print("Can handle histograms provided only in .root or .yoda format")

        if not len(vals):
            vals = {bin:{} for bin in vals_sample}

        for stxs_bin, parval in vals_sample.items():
            #print(stxs_bin)
            for par,val in parval.items():
                if par == "SM" : factor = 1.0
                else : factor = c_factor
                if "_int" in par:
                    par = par.replace("_int","")
                elif "_square" in par:
                    par = par.replace("_square","")
                    par = "{0}*{0}".format(par)
                elif "_cross" in par:
                    par = par.replace("_cross","")
                    par = par.replace("_","*")
                    
                vals[stxs_bin][par] = {"val": factor * val["val"] , "err": factor * val["err"]}

    printpar(vals,print_unc=args.printunc,val_threshold=args.threshold)
    writepar(vals,args.name,args.outfilename,threshold=args.threshold)

if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser(
        "prepare JOs for Higgs production"
    )
    parser.add_argument(
        "--input-folder",
        type=str,
        dest="infolder",
        help="input folder",
        required=True
    )
    parser.add_argument(
        "--output-parameterisation",
        type=str,
        dest="outfilename",
        help="out.yaml",
        default="out.yaml",
        required=False
    )
    parser.add_argument(
        "--histogram-name",
        type=str,
        dest="histoname",
        help="name of the histogram",
        required=False,
        default="/HiggsTemplateCrossSectionsStage12/STXS_stage1_2_fine_pTjet30"
    )
    parser.add_argument(
        "--print-unc",
        action="store_true",
        dest="printunc",
        help="print the unc. of the parameterisation",
        required=False,
        default=False
    )
    parser.add_argument(
        "--no-print-unc",
        action="store_false",
        dest="printunc",
        help="print the unc. of the parameterisation",
        required=False,
        default=False
    )
    parser.add_argument(
        "--param-name",
        type=str,
        dest="name",
        help="name of the parameterisation",
        required=False,
        default=""
    )
    parser.add_argument(
        "--debug",
        action="store_true",
        dest="debug",
        help="enable verbose printing for debugging",
        required=False,
        default=""
    )
    parser.add_argument(
        "--map-int-parameter",
        action="store_true",
        dest="applyintmap",
        help="enable if the files have integers mapped to EFT params",
        required=False,
        default=False
    )
    parser.add_argument(
        "--coeff-val",
        type=float,
        dest="coeff_val",
        help="coefficient value of sample",
        required=False,
        default=1.0
    )
    parser.add_argument(
        "--threshold",
        type=float,
        dest="threshold",
        help="threshold value of sample",
        required=False,
        default=1e-3
    )
    parser.add_argument(
        "--re-weight",
        action="store_true",
        dest="reweight",
        help="if output files are reweighted sample",
        required=False,
        default=False
    )


    args = parser.parse_args()
    extractParam(args)
