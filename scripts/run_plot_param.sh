#!/bin/bash

# plot_param.py 
# plot_param.py -r -l

# plot_param.py -i input/top/GG2HLL_top.yaml -it top  -c  input/topU3l/GG2HLL_topU3l.yaml -ct topU3l  -o output/TopVsTopU3l_new/
# plot_param.py -i input/top/GG2H_top.yaml -it top  -c  input/topU3l/GG2H_topU3l.yaml -ct topU3l  -o output/TopVsTopU3l_new/
# plot_param.py -i input/top/QQ2HLL_top.yaml -it top  -c  input/topU3l/QQ2HLL_topU3l.yaml -ct topU3l  -o output/TopVsTopU3l_new/
# plot_param.py -i input/top/QQ2HLNU_top.yaml -it top  -c  input/topU3l/QQ2HLNU_topU3l.yaml -ct topU3l  -o output/TopVsTopU3l_new/
# plot_param.py -i input/top/QQ2HQQ_top.yaml -it top  -c  input/topU3l/QQ2HQQ_topU3l.yaml -ct topU3l  -o output/TopVsTopU3l_new/
# plot_param.py -i input/top/THJB_top.yaml -it top  -c  input/topU3l/THJB_topU3l.yaml -ct topU3l  -o output/TopVsTopU3l_new/
# plot_param.py -i input/top/THW_top.yaml -it top  -c  input/topU3l/THW_topU3l.yaml -ct topU3l  -o output/TopVsTopU3l_new/
# plot_param.py -i input/top/TTH_top.yaml -it top  -c  input/topU3l/TTH_topU3l.yaml -ct topU3l  -o output/TopVsTopU3l_new/
# plot_param.py -i input/top/ZHLL_top.yaml -it top  -c  input/topU3l/ZHLL_topU3l.yaml -ct topU3l  -o output/TopVsTopU3l_new/

# plot_param.py -i input/top/GG2HLL_top.yaml -it top  -c  input/topU3l/GG2HLL_topU3l.yaml -r -l -ct topU3l  -o output/TopVsTopU3l_new/
# plot_param.py -i input/top/GG2H_top.yaml -it top  -c  input/topU3l/GG2H_topU3l.yaml -r -l -ct topU3l  -o output/TopVsTopU3l_new/
# plot_param.py -i input/top/QQ2HLL_top.yaml -it top  -c  input/topU3l/QQ2HLL_topU3l.yaml -r -l -ct topU3l  -o output/TopVsTopU3l_new/
# plot_param.py -i input/top/QQ2HLNU_top.yaml -it top  -c  input/topU3l/QQ2HLNU_topU3l.yaml -r -l -ct topU3l  -o output/TopVsTopU3l_new/
# plot_param.py -i input/top/QQ2HQQ_top.yaml -it top  -c  input/topU3l/QQ2HQQ_topU3l.yaml -r -l -ct topU3l  -o output/TopVsTopU3l_new/
# plot_param.py -i input/top/THJB_top.yaml -it top  -c  input/topU3l/THJB_topU3l.yaml -r -l -ct topU3l  -o output/TopVsTopU3l_new/
# plot_param.py -i input/top/THW_top.yaml -it top  -c  input/topU3l/THW_topU3l.yaml -r -l -ct topU3l  -o output/TopVsTopU3l_new/
# plot_param.py -i input/top/TTH_top.yaml -it top  -c  input/topU3l/TTH_topU3l.yaml -r -l -ct topU3l  -o output/TopVsTopU3l_new/
# plot_param.py -i input/top/ZHLL_top.yaml -it top  -c  input/topU3l/ZHLL_topU3l.yaml -r -l -ct topU3l  -o output/TopVsTopU3l_new/

# plot_param.py -i input/diff_best/differential_ggf_pth_lin_hyy.yaml  -it best -c input/diff_ref/differential_ggf_pth_lin_hyy.yaml -ct ref -o output/Differential/
# plot_param.py -i input/diff_best/differential_ggzh_pth_lin_hyy.yaml  -it best -c input/diff_ref/differential_ggzh_pth_hyy.yaml -ct ref -o output/Differential/
# plot_param.py -i input/diff_best/differential_tth_pth_lin_hyy.yaml  -it best -c input/diff_ref/differential_tth_pth_lin_hyy.yaml -ct ref -o output/Differential/
# plot_param.py -i input/diff_best/differential_vbf_pth_lin_hyy.yaml  -it best -c input/diff_ref/differential_vbf_pth_lin_hyy.yaml -ct ref -o output/Differential/
# plot_param.py -i input/diff_best/differential_wh_pth_lin_hyy.yaml  -it best -c input/diff_ref/differential_wh_pth_lin_hyy.yaml -ct ref -o output/Differential/

# plot_param.py -i input/diff_best/differential_ggf_pth_lin_hyy.yaml  -it best -c input/diff_ref/differential_ggf_pth_lin_hyy.yaml -ct ref -o output/Differential/ -r -l
# plot_param.py -i input/diff_best/differential_ggzh_pth_lin_hyy.yaml  -it best -c input/diff_ref/differential_ggzh_pth_hyy.yaml -ct ref -o output/Differential/ -r -l
# plot_param.py -i input/diff_best/differential_tth_pth_lin_hyy.yaml  -it best -c input/diff_ref/differential_tth_pth_lin_hyy.yaml -ct ref -o output/Differential/ -r -l
# plot_param.py -i input/diff_best/differential_vbf_pth_lin_hyy.yaml  -it best -c input/diff_ref/differential_vbf_pth_lin_hyy.yaml -ct ref -o output/Differential/ -r -l
# plot_param.py -i input/diff_best/differential_wh_pth_lin_hyy.yaml  -it best -c input/diff_ref/differential_wh_pth_lin_hyy.yaml -ct ref -o output/Differential/ -r -l




# plot_param.py -i input/top_noJetMatching/QQ2HLL_top_only.yaml -it standalone -c input/top/QQ2HLL_top_linear_prop.yaml -ct reweighting -o output/
# plot_param.py -i input/top_noJetMatching/QQ2HLL_top_only.yaml -it standalone -c input/top/QQ2HLL_top_linear_prop.yaml -ct reweighting -o output/ -r -l

# plot_param.py -i input/top_noJetMatching/QQ2HLNU_top_only.yaml -it standalone -c input/top/QQ2HLNU_top_linear_prop.yaml -ct reweighting -o output/
# plot_param.py -i input/top_noJetMatching/QQ2HLNU_top_only.yaml -it standalone -c input/top/QQ2HLNU_top_linear_prop.yaml -ct reweighting -o output/ -r -l

# plot_param.py -i input/top_noJetMatching/QQ2HQQ_top_only.yaml -it standalone -c input/top/QQ2HQQ_top_linear_prop.yaml -ct reweighting -o output/
# plot_param.py -i input/top_noJetMatching/QQ2HQQ_top_only.yaml -it standalone -c input/top/QQ2HQQ_top_linear_prop.yaml -ct reweighting -o output/ -r -l



# plot_param.py -i input/top_noJetMatching/QQ2HLL_top_only.yaml -it standalone_top -c input/topU3l/QQ2HLL_topU3l.yaml -ct standalone_topU3l -o output/TopVsTopU3l
# plot_param.py -i input/top_noJetMatching/QQ2HLL_top_only.yaml -it standalone_top -c input/topU3l/QQ2HLL_topU3l.yaml -ct standalone_topU3l -o output/TopVsTopU3l -r -l

# plot_param.py -i input/top_noJetMatching/QQ2HLNU_top_only.yaml -it standalone_top -c input/topU3l/QQ2HLNU_topU3l.yaml -ct standalone_topU3l -o output/TopVsTopU3l
# plot_param.py -i input/top_noJetMatching/QQ2HLNU_top_only.yaml -it standalone_top -c input/topU3l/QQ2HLNU_topU3l.yaml -ct standalone_topU3l -o output/TopVsTopU3l -r -l

# plot_param.py -i input/top_noJetMatching/QQ2HQQ_top_only.yaml -it standalone_top -c input/topU3l/QQ2HQQ_topU3l.yaml -ct standalone_topU3l -o output/TopVsTopU3l
# plot_param.py -i input/top_noJetMatching/QQ2HQQ_top_only.yaml -it standalone_top -c input/topU3l/QQ2HQQ_topU3l.yaml -ct standalone_topU3l -o output/TopVsTopU3l -r -l

plot_param.py -i input/top/QQ2HLL_top_linear_prop.yaml -it reweight_top -c input/topU3l/QQ2HLL_topU3l.yaml -ct standalone_topU3l -o output/TopReweightVsTopU3l
plot_param.py -i input/top/QQ2HLL_top_linear_prop.yaml -it reweight_top -c input/topU3l/QQ2HLL_topU3l.yaml -ct standalone_topU3l -o output/TopReweightVsTopU3l -r -l

plot_param.py -i input/top/QQ2HLNU_top_linear_prop.yaml -it reweight_top -c input/topU3l/QQ2HLNU_topU3l.yaml -ct standalone_topU3l -o output/TopReweightVsTopU3l
plot_param.py -i input/top/QQ2HLNU_top_linear_prop.yaml -it reweight_top -c input/topU3l/QQ2HLNU_topU3l.yaml -ct standalone_topU3l -o output/TopReweightVsTopU3l -r -l

plot_param.py -i input/top/QQ2HQQ_top_linear_prop.yaml -it reweight_top -c input/topU3l/QQ2HQQ_topU3l.yaml -ct standalone_topU3l -o output/TopReweightVsTopU3l
plot_param.py -i input/top/QQ2HQQ_top_linear_prop.yaml -it reweight_top -c input/topU3l/QQ2HQQ_topU3l.yaml -ct standalone_topU3l -o output/TopReweightVsTopU3l -r -l


# QQ2HLL_top_only.yaml
# QQ2HLNU_top_only.yaml
# QQ2HQQ_top_only.yaml

# QQ2HLL_top_linear_prop.yaml
# QQ2HLNU_top_linear_prop.yaml
# QQ2HQQ_top_linear_prop.yaml