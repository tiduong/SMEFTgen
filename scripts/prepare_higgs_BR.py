from SMEFTgen.param_helpers import load_param, checkifdir
from senstiEFT.matrix import load_map,load_matrix,matrix,load_parameterisation
from senstiEFT.matrix_helpers import get_padded_matrix
from collections import OrderedDict
import json,yaml
import pprint

            
higgs_decay = "eft2022/EFTparameterisation/parameterisation/decay/top/Higgs_decay_linear.yaml" 

def prepare_higgs_BR(out="out.json"):
     higgs_decay_inputs = load_param(higgs_decay)
     xpars = higgs_decay_inputs["coefficient terms"]
     xpars = sorted(list(set([ x for x in xpars if "*" not in x])))     
     poinames = [ypar for ypar in higgs_decay_inputs["observables"]]
     parmat = matrix("comb_par", [[0.0 for x in xpars] for y in poinames], xpars=xpars, ypars=poinames)
     pardict = OrderedDict()
    
     poinames =  [ "H4L",
                   "HAA",
                   "HBB",
                   "HCC",
                   "HLVLV",
                   "HMUMU",
                   "HTAUTAU",
                   "HZA"]
    
     for poi in poinames:
         pardict[poi] = OrderedDict()
     
     for ypar in poinames:
         yp = ypar
         ytw  = "HTOT"
         # linear         
         for x1 in xpars:
            Apw_x1, Atw_x1, Aperr_x1 = 0.0, 0.0, 0.0
            if x1 in higgs_decay_inputs['parameterisation'][yp]:
                Apw_x1 = higgs_decay_inputs['parameterisation'][yp][x1]["val"]
            if x1 in higgs_decay_inputs['parameterisation'][ytw]:
                Atw_x1 = higgs_decay_inputs['parameterisation'][ytw][x1]["val"]

            lin_term = Apw_x1 - Atw_x1 
            lin_term_err = Aperr_x1 
     
            if lin_term  != 0.0: pardict[ypar][x1] = {"val":lin_term, "err":lin_term_err}


     outfilename = args.out 
     checkifdir(outfilename)
     outdict = {"name" : "Higgs_BR",
               "parameterisation": pardict,
               "coefficient terms": xpars,
               "observables": poinames,
               "sm xsec": {binname : {"val" : 1.0, "err": 0.0} for binname in pardict}
               }

     with open(outfilename, "w") as f:
        if outfilename.endswith(".json"):
            json.dump(outdict, f, indent=2)
            print("Write parameterisation of {0} to {1}".format(name, outfilename))
        elif outfilename.endswith(".yaml") or outfilename.endswith(".yml"):
            yaml.dump(outdict, f, allow_unicode=False, default_flow_style=False)
            print("Write parameterisation of {0} to {1}".format("Higgs total width", outfilename))
        else:
            print("Please provide yaml or json as output, Nothing written !")
            sys.exit()

 
if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser("prepare BR parametersiation for Higgs")
    parser.add_argument( "-o","--output" , type=str,dest="out",help="path to save parameterisation plot",required=False,default="out.eps")

    args = parser.parse_args()
    prepare_higgs_BR(args.out)
