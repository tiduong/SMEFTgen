import os, sys

def writeJOs(args):
    
    sym, scheme = args.symmetry, args.scheme
    merge_rivet_outloc = args.mergeRivet
    if len(merge_rivet_outloc) and rivet:
        print(
            "Provided both the --mergeRivet and --writeRivet flag !\nPlease provide only one"
        )
        sys.exit()
    inputs = {}
    inputs["run-rivet"] = args.run_rivet
    inputs["SMHLOOP"] = args.smhloop
    model_loc = os.getenv("MODELSPATH")
    inputs["model"] = get_model(model_loc, sym, scheme)
    inputs["random_seed"] = args.random_seed
    inputs["propagtor_corrections"] = args.propcorr
    inputs["mass_tag"] = "massless"
    if args.nomass:
        inputs["mass_tag"] = "mass"
    inputs["interference"] = args.interference
    inputs["square"] = args.square
    inputs["cross"] = args.cross

    if not args.run_decay and not args.run_production:
        print("Not provided if you are generating production or decay\n")
        print("Please provide the necessary info.\n")
        sys.exit()

    if len(args.coefficients) == 0:
        print("Choosing all coefficients")
        coeff_names = get_coeff_names(sym)
    else:
        coeff_names = args.coefficients

    # pars = load_json("non_zero_pars.json")
    if os.getenv("PACKAGEPATH") == None:
        print("Please run build/setup.sh, $PACKAGEPATH not set!\n")
        sys.exit()

    if args.run_decay and not len(args.decaymodes):
        cases, inputs["run type"] = decaymodes, "decay"

    elif args.run_decay and len(args.decaymodes):
        cases, inputs["run type"] = args.decaymodes, "decay"

    elif args.run_production and not len(args.prodmodes):
        cases, inputs["run type"] = prodmodes, "production"

    elif args.run_production and len(args.prodmodes):
        cases, inputs["run type"] = args.prodmodes, "production"

    set_run_settings(inputs)

    setup_loc = os.getenv("PACKAGEPATH")
    setup_generation_path = setup_loc + "/setupGeneration.sh"
    setup_rivet_path = setup_loc + "/setupGeneration.sh"
    outloc = os.path.abspath(args.outpath)
    inputs["settings"]["nevents"] = args.nevents

    if len(args.jobFile) == 0:
        out_job = "out_{0}_{1}.sh".format(case, sym)

    else:
        out_job = args.jobFile
    out = open(out_job, "w")

    for case in cases:
        #   inputs["smeft"]={par:1.0 for par in pars[sym][prodmod] if par != "SM"}
        #   inputs["yukawa"]={'ymb': 1.0,}
        #   inputs["params"]={"SMEFT":"smeft","YUKAWA":"yukawa"}
        print(case)
        inputs["case"] = case
        out_path = "{0}/{1}/".format(outloc, case)

        wd = "{0}/JOs/{1}/".format(outloc, case)
        mkdir(wd)

        inputs["writeSubmit"] = args.writeSubmit
        out.write("mkdir -vp {0};\n".format(out_path))

        if args.standalone:
            inputs["generation mode"] = "standalone"
            for coeff, icoeff in zip(coeff_names, range(len(coeff_names))):
                partags, coeffs, typetags = [], [], []
                if coeff[0] == "c":
                    if inputs["interference"]:
                        partags.append("SMHLOOP={0} NP=1 NP^2==1".format(args.smhloop))
                        coeffs.append([coeff])
                        typetags.append("_int")
                    if inputs["square"]:
                        partags.append("SMHLOOP={0} NP=1 NP^2==2".format(args.smhloop))
                        coeffs.append([coeff])
                        typetags.append("_square")
                    if inputs["cross"]:
                        remaining_coeffs = coeff_names[coeff_names.index(coeff) + 1 :]
                        for coeff_cross in remaining_coeffs:
                            partags.append(
                                "SMHLOOP={0} NP=1 NP^2==2 NP{1}^2==1 NP{2}^2==1".format(
                                    args.smhloop, coeff, coeff_cross
                                )
                            )
                            coeffs.append([coeff, coeff_cross])
                            typetags.append("_cross")

                    # else:
                    #   print("You provided a Wilson coefficient : {0} but did not mention interference, cross-term, or square terms\n Please provide one option with the flag! \n".format(coeff))
                    #   sys.exit()
                else:
                    partags.append("SMHLOOP={0} NP=0".format(args.smhloop))

                    coeffs.append(["SM"])
                    typetags.append("")

                for partag, coeff, typetag in zip(partags, coeffs, typetags):
                    coeff = "_".join(coeff)
                    inputs["process"] = get_genlines(
                        inputs, case, partag, inputs["propagtor_corrections"]
                    )
                    print(inputs["process"])
                    inputs["par"] = "{0}_{1}".format(inputs["mass_tag"], coeff)
                    jo_file_name = (
                        wd
                        + "mc.MGPy8EG_SMEFT_test_{0}_{1}_{2}{3}.py".format(
                            sym, coeff, case, typetag
                        )
                    )

                    tag = "10000"
                    jo_folder = tag[0 : 6 - len(str(icoeff))] + str(icoeff)
                    dest_path = out_path + coeff + typetag

                    inputs["paths"] = {
                        "setupGeneration": setup_generation_path,
                        "setupRivet": setup_rivet_path,
                        "out_dir": dest_path,
                        "current_dir": wd,
                        "jo_folder": jo_folder,
                        "jo_file": jo_file_name,
                        "model": model_loc,
                    }

                    if len(args.mergeRivet):
                        write_rivetmerge(out, inputs, merge_rivet_outloc)
                        continue

                    with open(jo_file_name, "w") as f:
                        print("Writing JO file : {0}".format(jo_file_name))
                        writeJO(f, inputs)

                    if args.njobs:
                        ini_seed = inputs["random_seed"]
                        for ijob in range(args.njobs):
                            tag = ini_seed + ijob
                            inputs["random_seed"] = ini_seed + 1 + ijob
                            inputs["paths"]["out_dir"] = (
                                inputs["paths"]["out_dir"].split(".")[0]
                                + "."
                                + str(ijob)
                            )
                            write_runline(out, inputs)

                    else:
                        write_runline(out, inputs)

        if args.reweight:
            inputs["par"] = coeff_names
            if args.out_crossterms != "":
                from sys import argv

                with open(args.out_crossterms,"w") as f:
                    ini_argv = " ".join(argv)
                    pars_to_run = []
                    for par1 in inputs["par"]:
                        idx = inputs["par"].index(par1)
                        tmp = []
                        for par2 in inputs["par"][idx:]:
                            tmp.append(par2)
                        if len(tmp) >=2 : 
                            pars_to_run.append(" ".join(tmp))

                    for par_run,i in zip(pars_to_run,range(len(pars_to_run))):
                        for par_rm in argv:
                            argv_out = ini_argv.replace(par_rm,"")
                            argv_out = ini_argv.replace("--coefficents","")
                        argv_out = "{0} --coefficients {1}".format(argv_out, par_run)
                        argv_out = argv_out.replace("--out-cross-term-lines","")
                        argv_out = argv_out.replace(args.out_crossterms,"")
                        argv_out = argv_out.replace(".sh ","_{0}.sh ".format(i))
                        argv_out = argv_out + " --run-tag {0}".format(i)
                        f.write("python2 {0} \n".format(argv_out))
                    print("written run lines to {0}, you can run this to launch cross-term jobs ! ".format(args.out_crossterms))
                    sys.exit()

            inputs["run tag"] = ""
            if args.run_tag != "":
                inputs["run tag"]  = "_" + args.run_tag
            
            inputs["generation mode"] = "reweight"
            if inputs["interference"]:
                partag_reweight = "SMHLOOP={0} NP=1 NP^2==1 ".format(args.smhloop)
                typetag = "reweight_int"
            if inputs["square"]:
                partag_reweight = "SMHLOOP={0} NP=1 NP^2==2 ".format(args.smhloop)
                typetag = "reweight_square"
            if inputs["cross"]:
                cross_tag = " ".join(["NP{0}^2==1".format(coeff) for coeff in coeff_names])
                partag_reweight = "SMHLOOP={0} NP=1 NP^2==2 {1}".format(args.smhloop,cross_tag)
                typetag = "reweight_cross"
            if not inputs["interference"] and inputs["square"] and inputs["cross"]:
                partag_reweight = "NP=0 NPprop=0"
                typetag = "reweight_sm"

            partag = "NP=0 NPprop=0"

            jo_file_name = wd + "/"+"mc.MGPy8EG_SMEFT_test_{0}_{1}_{2}{3}.py".format(
                sym, case, typetag, inputs["run tag"]
            )
            inputs["process"] = get_genlines(
                inputs, case, partag, inputs["propagtor_corrections"])
            print(inputs["process"])
            
            inputs["reweight process"] = get_genlines(
                inputs, case, partag_reweight, inputs["propagtor_corrections"]
            )

            jo_folder = "100000"

            dest_path = out_path + typetag + inputs["run tag"]

            inputs["paths"] = {
                "setupGeneration": setup_generation_path,
                "setupRivet": setup_rivet_path,
                "out_dir": dest_path,
                "current_dir": wd,
                "jo_folder": jo_folder,
                "jo_file": jo_file_name,
                "model": model_loc,
            }

            # if len(args.mergeRivet):
            #    write_rivetmerge(out, inputs, merge_rivet_outloc)
            #    continue

            with open(jo_file_name, "w") as f:
                print("Writing JO file : {0}".format(jo_file_name))
                writeJO(f, inputs)

            if args.njobs:
                ini_seed = inputs["random_seed"]
                for ijob in range(args.njobs):
                    tag = ini_seed + ijob
                    inputs["random_seed"] = ini_seed + 1 + ijob
                    inputs["paths"]["out_dir"] = (
                        inputs["paths"]["out_dir"].split(".")[0] + "." + str(ijob)
                    )
                    write_runline(out, inputs)

            else:
                write_runline(out, inputs)

    out.close()
    print("written run file {0}".format(out_job))
    import yaml

    with open("inputs.yaml", "w") as f:
        yaml.dump(inputs, f, default_flow_style=False)


if __name__ == "__main__":
    from argparse import ArgumentParser

    parser = ArgumentParser("prepare JOs for Higgs production")
    parser.add_argument(
        "--symmetry",
        type=str,
        dest="symmetry",
        help="flavour symmetry : U35, topU3l, top",
        required=False,
        default="U35",
    )
    parser.add_argument(
        "--scheme",
        type=str,
        dest="scheme",
        help="Input parameter scheme : Mw, alpha",
        required=False,
        default="Mw",
    )
    parser.add_argument(
        "--prodmodes",
        type=str,
        dest="prodmodes",
        help="production modes to generate JO",
        metavar="production modes",
        nargs="+",
        default=[],
    )
    parser.add_argument(
        "--decaymodes",
        type=str,
        dest="decaymodes",
        help="decay modes to generate JO",
        metavar="production modes",
        nargs="+",
        default=[],
    )
    parser.add_argument(
        "--run-decay",
        action="store_true",
        dest="run_decay",
        default=False,
        required=False,
    )
    parser.add_argument(
        "--run-production",
        action="store_true",
        dest="run_production",
        default=False,
        required=False,
    )
    parser.add_argument(
        "--coefficients",
        type=str,
        dest="coefficients",
        help="coefficients to generate JO",
        metavar="coeff names",
        nargs="+",
        default=[],
    )
    parser.add_argument(
        "--out",
        type=str,
        dest="outpath",
        help="path to write out job outputs",
        metavar="path",
    )

    parser.add_argument(
        "--nevents",
        type=int,
        dest="nevents",
        help="number of events",
        default=20000,
        metavar="no. of events",
    )
    parser.add_argument(
        "--random-seed", type=int, dest="random_seed", help="random seed", default=1
    )
    parser.add_argument(
        "--n-jobs", type=int, dest="njobs", help="number of jobs", default=0
    )
    parser.add_argument(
        "--writeSubmit",
        type=str,
        dest="writeSubmit",
        help="command for job submission",
        default="",
    )
    parser.add_argument(
        "--run-rivet",
        action="store_true",
        dest="run_rivet",
        help="option to run rivet",
        default=True,
    )
    parser.add_argument(
        "--no-run-rivet",
        action="store_false",
        dest="run_rivet",
        help="option to not run rivet",
        default=False,
    )
    parser.add_argument(
        "--mergeRivet",
        type=str,
        dest="mergeRivet",
        help="destination for merged rivet files",
        default="",
    )
    parser.add_argument(
        "--jobFile",
        type=str,
        dest="jobFile",
        help="filename for job submission",
        default="",
    )
    parser.add_argument(
        "--cross-terms",
        action="store_true",
        dest="cross",
        help="generate cross terms",
        default=True,
    )
    parser.add_argument(
        "--interference-terms",
        action="store_true",
        dest="interference",
        help="generate EFT-SM intereference terms",
        default=True,
    )
    parser.add_argument(
        "--square-terms",
        action="store_true",
        dest="square",
        help="generate square terms",
        default=True,
    )
    parser.add_argument(
        "--no-cross-terms",
        action="store_false",
        dest="cross",
        help="do not generate cross terms",
        default=True,
    )
    parser.add_argument(
        "--no-interference-terms",
        action="store_false",
        dest="interference",
        help="do not generate EFT-SM intereference terms",
        default=True,
    )
    parser.add_argument(
        "--no-square-terms",
        action="store_false",
        dest="square",
        help="do not generate square terms",
        default=False,
    )
    parser.add_argument(
        "--no-massless",
        action="store_true",
        dest="nomass",
        help="set mass to light particles : mu, charm, tau",
        default=False,
    )
    parser.add_argument(
        "--propagator-corrections",
        action="store_true",
        dest="propcorr",
        help="estimate propagator corrections",
        default="",
    )
    parser.add_argument("--SMHLOOP", dest="smhloop", default="0")
    parser.add_argument(
        "--standalone",
        action="store_true",
        dest="standalone",
        help="standalone generation",
        default=False,
    )
    parser.add_argument(
        "--re-weight",
        action="store_true",
        dest="reweight",
        help="reweighte generation",
        default=False,
    )
    parser.add_argument(
        "--out-cross-term-lines",
        type=str,
        dest="out_crossterms",
        help="prepare all cross term",
        default=""
    )
    parser.add_argument(
        "--run-tag",
        type=str,
        dest="run_tag",
        help="run tag",
        default=""
    )
    
    args = parser.parse_args()
    writeJOs(args)

    
