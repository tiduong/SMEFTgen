import yoda
import os
from ROOT import TCanvas,vector, TGraph, TAxis, gPad, TLegend, TLatex, TH1F
import sys

typ=sys.argv[1]

def read_cross_sections(coeff_file):
    cross_sections = {}
    data = yoda.read(coeff_file)
    try:
        for item in data.values():
           
	    
            if isinstance(item, yoda.core.Scatter1D) and item.path() == "/_XSEC":
                print("XSEC found")
               
	        print(abs(item[0].x()))
                cross_sections[coeff_file] = abs(item.point(0).x())  # Assuming the cross-section value is the first y-value
                break
    finally:
        data = None
    return cross_sections

def calculate_ratios(coeff_files, sm_coeff_file):
    sm_cross_section = read_cross_sections(sm_coeff_file)
    ratios = {}
    for coeff_file in coeff_files:
        coeff_name = os.path.basename(coeff_file).split('.')[0]  # Extract coefficient name from filename
        coeff_cross_section = read_cross_sections(coeff_file)
        if coeff_cross_section:
            ratios[coeff_name] = coeff_cross_section[coeff_file] / sm_cross_section[sm_coeff_file]
        else:
            print("No cross-section data found in {} file".format(coeff_name))
    return ratios

def create_root_plot(ratios):
    nPoints = len(ratios)
    values = list(ratios.values())
    coefficients = list(ratios.keys())   
    hist = TH1F("hist", "Values Associated with Coefficients", nPoints, 0.5, nPoints + 0.5)
    canvas = TCanvas("canvas", "ROOT Plot", 800, 600)
    # Set bin contents and labels
    for i, (val, coeff) in enumerate(zip(values, coefficients), start=1):
        hist.SetBinContent(i, val)
        hist.GetXaxis().SetBinLabel(i, coeff)

    # Set y-axis title
    hist.GetYaxis().SetTitle("#frac{|#sigma_{EFT}|}{#sigma_{SM}}")

    # Set y-axis range and scale
    hist.SetMinimum(10e-6)
    hist.SetMaximum(10e4)
    gPad.SetLogy()

    # Draw grid for y-axis
    gPad.SetGrid()
    hist.SetMarkerSize(1.5)
    #hist.SetMarkerType(20)    
# Draw histogram with "P" option to show points
    hist.Draw("*H")

    
    # Create legend
    legend = TLegend(0.7, 0.7, 0.9, 0.9)
    legend.SetHeader("Legend")
    legend.AddEntry(hist, "82<m_{ll}<102 GeV", "p")
    legend.Draw("SAME")
    
    # Show plot
    canvas.Draw()
    canvas.SaveAs("output_ratio_{}.pdf".format(typ))


def main():
    
    # Define paths to coefficient files
    if typ =="Fermion":

        coeff_files = ["/afs/cern.ch/user/t/tiduong/private/SMEFTgen/EVNT_outdir/U35_DY_neutral_current_ee/clu_1p0_.0_82_102/test/clu_.yoda", "/afs/cern.ch/user/t/tiduong/private/SMEFTgen/EVNT_outdir/U35_DY_neutral_current_ee/cqe_1p0_.0_82_102/test/cqe_.yoda", "/afs/cern.ch/user/t/tiduong/private/SMEFTgen/EVNT_outdir/U35_DY_neutral_current_ee/clq3_1p0_.0_82_102/test/clq3_.yoda", "/afs/cern.ch/user/t/tiduong/private/SMEFTgen/EVNT_outdir/U35_DY_neutral_current_ee/clq1_1p0_.0_82_102/test/clq1_.yoda", "/afs/cern.ch/user/t/tiduong/private/SMEFTgen/EVNT_outdir/U35_DY_neutral_current_ee/cll1_1p0_.0_82_102/test/cll1_.yoda", "/afs/cern.ch/user/t/tiduong/private/SMEFTgen/EVNT_outdir/U35_DY_neutral_current_ee/cld_1p0_.0_82_102/test/cld_.yoda", "/afs/cern.ch/user/t/tiduong/private/SMEFTgen/EVNT_outdir/U35_DY_neutral_current_ee/ced_1p0_.0_82_102/test/ced_.yoda", "/afs/cern.ch/user/t/tiduong/private/SMEFTgen/EVNT_outdir/U35_DY_neutral_current_ee/ceu_1p0_.0_82_102/test/ceu_.yoda" ]  # Add more files as needed
    elif typ =="Higgs":
        coeff_files = ["/afs/cern.ch/user/t/tiduong/private/SMEFTgen/EVNT_outdir/U35_DY_neutral_current_ee/cHDD_1p0_.0_82_102/test/cHDD_.yoda","/afs/cern.ch/user/t/tiduong/private/SMEFTgen/EVNT_outdir/U35_DY_neutral_current_ee/cHWB_1p0_.0_82_102/test/cHWB_.yoda","/afs/cern.ch/user/t/tiduong/private/SMEFTgen/EVNT_outdir/U35_DY_neutral_current_ee/cHl1_1p0_.0_82_102/test/cHl1_.yoda","/afs/cern.ch/user/t/tiduong/private/SMEFTgen/EVNT_outdir/U35_DY_neutral_current_ee/cHl3_1p0_.0_82_102/test/cHl3_.yoda","/afs/cern.ch/user/t/tiduong/private/SMEFTgen/EVNT_outdir/U35_DY_neutral_current_ee/cHe_1p0_.0_82_102/test/cHe_.yoda","/afs/cern.ch/user/t/tiduong/private/SMEFTgen/EVNT_outdir/U35_DY_neutral_current_ee/cHq1_1p0_.0_82_102/test/cHq1_.yoda","/afs/cern.ch/user/t/tiduong/private/SMEFTgen/EVNT_outdir/U35_DY_neutral_current_ee/cHq3_1p0_.0_82_102/test/cHq3_.yoda","/afs/cern.ch/user/t/tiduong/private/SMEFTgen/EVNT_outdir/U35_DY_neutral_current_ee/cHu_1p0_.0_82_102/test/cHu_.yoda","/afs/cern.ch/user/t/tiduong/private/SMEFTgen/EVNT_outdir/U35_DY_neutral_current_ee/cHd_1p0_.0_82_102/test/cHd_.yoda"] 

    sm_coeff_file = "/afs/cern.ch/user/t/tiduong/private/SMEFTgen/EVNT_outdir/U35_DY_neutral_current_ee/SM_1p0_.0_82_102/test/SM_.yoda"  # Path to the SM coefficient file

    # Calculate ratios
    ratios = calculate_ratios(coeff_files, sm_coeff_file)

    # Save ratios to files
    create_root_plot(ratios)

if __name__ == "__main__":
    main()

