import json, yaml, sys
import pprint 
from SMEFTgen.param_helpers import *

def get_merging_map(map, merged_binnames):
    merge_pardict = {bin:[] for bin in merged_binnames}
    for finepar, mappedpar in map.items():
        if mappedpar in merge_pardict.keys():
            merge_pardict[mappedpar].append(finepar)
    return merge_pardict
        
def merge_input_params(paramfiles):
    merged_param = {"coefficient terms":[], 
              "name":"merged param",
              "observables":[],
              "parameterisation":{},
              "sm xsec":{}}

    for paramfile in paramfiles:
        with open(paramfile,"r") as f:
            data = yaml.safe_load(f)
            merged_param["coefficient terms"] = merged_param["coefficient terms"] + data["coefficient terms"]
            merged_param["observables"] = merged_param["observables"] + data["observables"]
            for bin,param in data["parameterisation"].items():
                merged_param["parameterisation"][bin] = {}            
                for par, val in param.items():
                    merged_param["parameterisation"][bin][par] = val 

            for bin,smval in data["sm xsec"].items():
                merged_param["sm xsec"][bin] = smval

    merged_param["observables"] = list(set(merged_param["observables"]))
    merged_param["coefficient terms"] = list(set(merged_param["coefficient terms"]))

#    print(merged_param["observables"])
#    print(merged_param["coefficient terms"])
    return merged_param 

def load_xsec(paramfiles):

    xsec = {}
    data = merge_input_params(paramfiles)
    param_dict = data["parameterisation"]

    for bin in param_dict : xsec[bin] = {}
    ## create empty dict with same structure
    for bin,bin_param in param_dict.items():
        for param in bin_param:
            xsec[bin][param] = {"val":0.0,"err":0.0}    

    for bin,bin_param in xsec.items():
        #print(bin)
        if bin not in data["sm xsec"].keys(): continue
        smval = data["sm xsec"][bin]["val"]
        xsec[bin]["SM"] = {"val": data["sm xsec"][bin]["val"],"err":data["sm xsec"][bin]["err"]}
        for param in bin_param:
            #print(param)
            #print(param_dict[bin])
            if param == "SM": continue
            if param not in param_dict[bin]: continue 
            val = param_dict[bin][param]["val"]
            err = param_dict[bin][param]["err"]
            xsec[bin][param] = { "val": smval*val, "err": err*smval }

#    pprint.pprint(xsec)
    return xsec 

def prepare_merged_xsec(xsec, merging_map):
#    pprint.pprint(xsec)
#    print(merging_map)
    pars = list(set([par for bin in xsec for par in xsec[bin]]))
    merged_xsec = {bin:{ par:{"val":0.0,"err":0.0} for par in pars} for bin in merging_map.keys()}
#    print(xsec.keys())
    for bin, subbins in merging_map.items():
#        print(bin, subbins,merged_xsec[bin])
        for par in merged_xsec[bin]:
            for subbin in subbins:
                if subbin not in xsec.keys(): continue
                if par in xsec[subbin].keys():
                    merged_xsec[bin][par] = tupleadd(merged_xsec[bin][par], xsec[subbin][par])

    return merged_xsec

def prepare_merged_param(args):
    debug = args.debug
    #print(args.param)
    map = load_map(args.map,args.histoname)
    xsec = load_xsec(args.param)
#    pprint.pprint(xsec)
    merged_param_names = list(set(map.values()))
    merged_param_names.remove("one")

    merged_xsec = {merged_par : {} for merged_par in merged_param_names}

    if debug:
        print("This is the provided map")
        for finepar, mergepar in map.items():
            print("{0} ---> {1}".format(finepar,mergepar))

    merging_map = get_merging_map(map, merged_param_names)

    if debug:
        print("This is the mapping merged --> [fine0, fine1, fine2, ..., ...]")
        pprint.pprint(merging_map)

    merged_xsecs = prepare_merged_xsec(xsec, merging_map)
    writepar(merged_xsecs,args.name,args.outfilename,threshold=1e-3)

if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser(
        "merge STXS param production"
    )
    parser.add_argument(
        "--input-param",
        type=str,
        nargs="+",
        dest="param",
        help="input parameterisation",
        required=True
    )
    parser.add_argument(
        "--merging-map",
        type=str,
        dest="map",
        help="infile.yaml",
        default=None,
        required=False
    )
    parser.add_argument(
        "--histogram-name",
        type=str,
        dest="histoname",
        help="name of the histogram",
        required=False,
        default="STXS_stage1_2_fine_pTjet30"
    )
    parser.add_argument(
        "--output",
        type=str,
        dest="outfilename",
        help="output yaml filename",
        required=False,
        default="out.yaml"
    )

    parser.add_argument(
        "--print-unc",
        action="store_true",
        dest="printunc",
        help="print the unc. of the parameterisation",
        required=False,
        default=False
    )
    parser.add_argument(
        "--no-print-unc",
        action="store_false",
        dest="printunc",
        help="print the unc. of the parameterisation",
        required=False,
        default=False
    )
    parser.add_argument(
        "--param-name",
        type=str,
        dest="name",
        help="name of the parameterisation",
        required=False,
        default="merged param"
    )
    parser.add_argument(
        "--debug",
        action="store_true",
        dest="debug",
        help="enable verbose printing for debugging",
        required=False,
        default=""
    )
    parser.add_argument(
        "--map-int-parameter",
        action="store_true",
        dest="applyintmap",
        help="enable if the files have integers mapped to EFT params",
        required=False,
        default=False
    )
    parser.add_argument(
        "--coeff-val",
        type=float,
        dest="coeff_val",
        help="coefficient value of sample",
        required=False,
        default=1.0
    )

    args = parser.parse_args()
    prepare_merged_param(args)
