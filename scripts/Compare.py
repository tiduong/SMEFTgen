import ROOT
import sys
import os



os.environ["DISPLAY"] = ":0"
# Specify the path to the ROOT file
file1path =str(sys.argv[1])

file2path=str(sys.argv[2])

ext=str(sys.argv[3])
output_directory = "/afs/cern.ch/user/t/tiduong/private/SMEFTgen/Compare_"+ext+"/"
if not os.path.exists(output_directory):
    os.makedirs(output_directory)

# Open the ROOT file
f1 = ROOT.TFile.Open(file1path, "READ")
f2 = ROOT.TFile.Open(file2path, "READ")

coef=os.path.basename(file2path).split(".")[0]
# Check if the file is successfully opened
if f1.IsOpen():
    print("File1 opened successfully.")
else:
    print("Failed to open file1.")
    exit(1)  # Exit the script if file opening failed
if f2.IsOpen():
    print("File2 opened successfully.")
else:
    print("Failed to open file2.")
    exit(1)  # Exit the script if file opening failed


keys1 = f1.GetListOfKeys()
keys2 = f2.GetListOfKeys()

#Specify the string you're looking for in the histogram name
desired_string = coef

# Loop through the keys to find the histogram with the desired string in its name
h1 = None
h2 = None
for key1,key2 in zip(keys1,keys2):
    print(key1)
    print(key2)
    if desired_string in key1.GetName() and "mass" in key1.GetName():
        if h1 is None:
            h1 = key1.ReadObj()
            print("Histogram '{}' found".format(key1))
    if desired_string and "mll" in key2.GetName():
        if h2 is None:
            h2 = key2.ReadObj()
            print("Histogram '{}' found".format(key2))
        
 

# Check if the histogram is successfully retrieved
if h1:
    print("Histogram 1 with '{}' in its name retrieved successfully.".format(desired_string))
else:
    print("Failed to retrieve histogram 1 with '{}' in its name.".format(desired_string))

if h2:
    print("Histogram 2 with '{}' in its name retrieved successfully.".format(desired_string))
else:
    print("Failed to retrieve histogram 2 with '{}' in its name.".format(desired_string))

# Loop over each bin in the histograms
for i in range(1, h1.GetNbinsX() + 1):
    # Divide bin content by bin width for h3
    bin_content_h1= h1.GetBinContent(i) / h1.GetBinWidth(i)
    h1.SetBinContent(i, bin_content_h1)

    # Divide bin content by bin width for h2
    bin_content_h2 = h2.GetBinContent(i) / h2.GetBinWidth(i)
    h2.SetBinContent(i, bin_content_h2)


max_x1 = h1.GetXaxis().GetXmax()
max_x2 = h2.GetXaxis().GetXmax()
max_x = max(max_x1, max_x2)

max_y1 = h1.GetMaximum()
max_y2 = h2.GetMaximum()
max_y = max(max_y1, max_y2)



c1 = ROOT.TCanvas("canvas", "Histogram Canvas", 800, 600)
legend = ROOT.TLegend(0.65, 0.7, 0.9, 0.9)

legend.AddEntry(h1,"Ricardo's sample" , "l")
legend.AddEntry(h2, "New sample", "l")

h1.SetLineColor(ROOT.kBlue)
h2.SetLineColor(ROOT.kRed)
h1.SetStats(0)
h2.SetStats(0)

h1.Sumw2()
h2.Sumw2()

h2.Draw("SAME")

h1.Draw("SAME")

h1.GetXaxis().SetRangeUser(0, max_x)
h2.GetXaxis().SetRangeUser(0, max_x)

h1.GetYaxis().SetRangeUser(0, 2*max_y)
h2.GetYaxis().SetRangeUser(0, 2*max_y)
print(h1[1])
print(h2[1])

legend.Draw()
png_filename = os.path.join(output_directory,"{}.png".format(desired_string))
#c1.SaveAs("~/private/SMEFTgen/Cross_check/{}.png".format(desired_string))
c1.SaveAs(png_filename)
c1.Close()
c2 = ROOT.TCanvas("canvas", "Histogram ratio", 800, 600)
legend2 = ROOT.TLegend(0.65, 0.7, 0.9, 0.9)
c2.SetBatch(True)

h_ratio = h1.Clone()
h_ratio.Sumw2()
legend2.AddEntry(h_ratio,"Ratio Ricardo/New" , "l")

# Divide the bin contents of the first histogram by the corresponding bin contents of the second histogram
h_ratio.Divide(h2)
max_ratio=h1[1]/h2[1]
h_ratio.GetYaxis().SetRangeUser(0,1.4*max_ratio )
#print(h_ratio.FindBin(h_ratio.GetMaximum()))
print(max_ratio)

# Draw the ratio histogram
line = ROOT.TLine(h_ratio.GetXaxis().GetXmin()-100, 1, h_ratio.GetXaxis().GetXmax()+100, 1)

# Set line color and style (optional)
line.SetLineColor(ROOT.kBlack)
line.SetLineStyle(2)  # Dashed line

# Draw the line on the canvas

h_ratio.Draw("E")
line.Draw("SAME")

legend2.Draw()

h_ratio.SetTitle(desired_string)
h_ratio.GetXaxis().SetTitle("m_ll")
h_ratio.GetYaxis().SetTitle("Ratio")
png_filename2 = os.path.join(output_directory,"{}_Ratio.png".format(desired_string))
#c2.SaveAs("~/private/SMEFTgen/Cross_check/{}_Ratio.png".format(desired_string))
c2.SaveAs(png_filename2)
c2.Close()
# Close the ROOT file when done
f1.Close()
f2.Close()

