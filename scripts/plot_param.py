#!/usr/bin/env python
import argparse
import ROOT as root
import yaml
from pathlib import Path
import functools

def setupArgs():
    parser = argparse.ArgumentParser(description='Plot and compare Xsecs and EFT parameterisations')
    parser.add_argument('-i','--input', 
                        default="../input_topU3l/GG2H_topU3l.yaml",
                        type=str,
                        help='Give the relative path to input yaml file')
    parser.add_argument('-it','--inputTag', 
                        default="input",
                        type=str,
                        help='Give the tag of the sample (enty in TLegend)')
    parser.add_argument('-c','--compare', 
                        default="../input_top/GG2H_top.yaml",
                        type=str,
                        help='Give the relative path to input yaml file for comparison')
    parser.add_argument('-ct','--compareTag', 
                        default="comp",
                        type=str,
                        help='Give the tag of the sample (enty in TLegend)')
    parser.add_argument('-y','--yamlkey', 
                        default="sm xsec",
                        type=str,
                        help='Give the yaml key you want to plot/compare. Options: sm xsec, parameterisation  (default: sm xsec)')
    parser.add_argument('-l','--linear', 
                        default=False,
                        action='store_true',
                        help='Plot with linear Y scale. (default: off)')
    parser.add_argument('-n','--norm', 
                        default=False,
                        action='store_true',
                        help='Plot normalised to have unit area. (default: off)')
    parser.add_argument('-r','--ratio', 
                        default=False,
                        action='store_true',
                        help='Perfomr a ratio Plot. (default: off)')
    parser.add_argument('-o','--output', 
                        default="output/",
                        type=str,
                        help='Give the output path (default: output)')
    return parser

# def listsorted(elem):
#     list=['pTH_Hyy_0_5', 'pTH_Hyy_5_10', 'pTH_Hyy_10_15', 'pTH_Hyy_15_20', 'pTH_Hyy_20_25', 'pTH_Hyy_25_30', 'pTH_Hyy_30_35', 'pTH_Hyy_35_45', 'pTH_Hyy_45_60', 'pTH_Hyy_60_80', 'pTH_Hyy_80_100', 'pTH_Hyy_100_120', 'pTH_Hyy_120_140', 'pTH_Hyy_140_170', 'pTH_Hyy_170_200', 'pTH_Hyy_200_250', 'pTH_Hyy_250_350', 'pTH_Hyy_350_450', 'pTH_Hyy_450_650', 'pTH_Hyy_650_1000']
#     for i in range(len(list)):
#         if(elem==list[i]):
#             return i

def load_param(paramfile):
    with open(paramfile,'r') as f:
        data = yaml.safe_load(f)
    return data

# def print_listofbins(args):
#     data = load_param(args.input)
#     listofbins=sorted(list(data[args.yamlkey].keys()))
#     # listofbins.sort(key=listsorted)
#     print(listofbins)

def make_histo(key,inputpath,hist,color,norm=False):
    data = load_param(inputpath)
    listofbins=sorted(list(data[key].keys()))
    print(listofbins)
    # sort=listofbins.sort(key=listsorted)
    # print(sort)
    hist = root.TH1F(hist, "", len(listofbins), 0, len(listofbins))
   
    if(key=="sm xsec"): 
        hist.GetYaxis().SetTitle("SM cross section")
        # Fill the histrogram according to values in list
        for i, item in enumerate(listofbins):
            hist.Fill(item, data[key][item]['val'])
            hist.SetBinError(i, data[key][item]['err'])

    if(key=="parameterisation"): 
        hist.GetYaxis().SetTitle("Parameterisation")
        # Fill the histrogram according to values in list
        for i, item in enumerate(listofbins):
            hist.Fill(item, data[key][item]['val'])
            hist.SetBinError(i, data[key][item]['err'])
   
    # Get rid of the StatBox
    hist.SetStats(0)
    
    # Set axis titles
    hist.GetXaxis().SetTitle("STXS bins")
   
    # Set Marker Style & Color
    hist.SetMarkerStyle(21)
    hist.SetMarkerColor(color)
    hist.SetLineColor(color)

    # Set Lable Style
    hist.GetXaxis().LabelsOption("v")
    hist.GetXaxis().SetTitleOffset(9.0)

    if(norm==True):
        hist.Scale(1/hist.Integral())
        hist.GetYaxis().SetTitle("normalized")

    return hist

def make_legend(THist=[]):
    leg = root.TLegend(0.7,0.90,0.85,0.98)
    leg.SetBorderSize(0)
    leg.SetFillColor(0)
    leg.SetFillStyle(0)
    leg.SetTextFont(42)
    leg.SetTextSize(0.03)
    for h in range(len(THist)):
        leg.AddEntry(THist[h], THist[h].GetName(),"lep")
    return leg

def create_canvas(linearY=False):
    # Construct the canvas
    canvas = root.TCanvas("c1", "c1", 0, 0, 800, 800)
    canvas.SetBottomMargin(0.6)
    canvas.SetLeftMargin(0.15)
    if not(linearY==True): canvas.SetLogy()
    return canvas

def get_ident(args,idx):
    input_filename=(Path(args.input).stem).split("_")
    ident=input_filename[idx]
    return ident

def plot_param(args):

    c1=create_canvas(args.linear)

    input_proc=get_ident(args,0) 
    input_param=get_ident(args,1)

    input_hist=make_histo(args.yamlkey, args.input, args.inputTag, 1, args.norm)
    comp_hist=make_histo(args.yamlkey, args.compare, args.compareTag, 2, args.norm)

    legend=make_legend([input_hist,comp_hist])

    if(args.ratio):
        ratio_hist=input_hist.Clone()
        ratio_hist.Sumw2()
        ratio_hist.Divide(comp_hist)
        ratio_hist.SetAxisRange(0.5, 1.5,"Y")
        ratio_hist.GetYaxis().SetTitle("ratio")
        ratio_hist.Draw()
    else:
        input_hist.Draw("e1")
        comp_hist.Draw("e1 same")
    
    legend.Draw()

    # Save the plot as a PDF
    c1.Update()
    if(args.ratio): c1.SaveAs(args.output + input_proc + "_" + input_param + "_ratio.pdf")
    else: c1.SaveAs(args.output + input_proc + "_" + input_param + ".pdf")

if __name__ == '__main__':
    args = setupArgs().parse_args()
    plot_param(args)
    # print_listofbins(args)

 
