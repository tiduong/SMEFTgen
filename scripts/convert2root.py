
from array import array
import ROOT as rt
import yoda
import sys

def convert_to_root(yoda_file, root_file):
    yoda_data = yoda.read(yoda_file)
    rt_file = rt.TFile(root_file, 'recreate')

    for name, yoda_hist in yoda_data.items():
        if isinstance(yoda_hist, yoda.Histo1D):
            rt_hist = rt.TH1D(name, '', yoda_hist.numBins(), array('d', yoda_hist.xEdges()))
            rt_hist.Sumw2()

            for i in range(yoda_hist.numBins()):
                rt_hist.SetBinContent(i + 1, yoda_hist.bin(i).sumW())

            rt_hist.Write()

    rt_file.Close()

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python convert_to_root.py input.yoda output.root")
        sys.exit(1)

    yoda_file = sys.argv[1]
    root_file = sys.argv[2]
    convert_to_root(yoda_file, root_file)

