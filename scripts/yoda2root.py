
from array import array
import ROOT as rt
import yoda, sys


def get_histogram_name(yoda_name):
    # Splitting the YODA histogram name by line breaks
    lines = yoda_name.split("\n")
    # Searching for the line containing "Path:"
    for line in lines:
        print(line)
        if "Path" in line:
            # Extracting the part after "Path:"
            print(line)
            path_part = line.split("Path:")[1].strip()
            # Using path_part as the desired part for the ROOT histogram name
            return fName + path_part
    # If "Path:" is not found, return a default name
    return "histogram_default"+str(i)


fName = str(sys.argv[1])
yodaAOs = yoda.read(fName)
rtFile = rt.TFile(fName[:fName.find('.yoda')] + '.root','recreate')
outparts = {}
i = 0
for name in yodaAOs:
  yodaAO = yodaAOs[name];
  rtAO = None
  
  if 'Histo1D' in str(yodaAO):
    h_name=name.split("/")[1]+" "+name.split("/")[2]
    print(i)
    print(h_name)
    outparts[str(i)] = rt.TH1D(h_name, str(h_name), yodaAO.numBins(), array('d', yodaAO.xEdges()))
    outparts[str(i)].Sumw2() 
    outparts[str(i)+str(i)+str(i)] = outparts[str(i)].GetSumw2()

    for k in range(outparts[str(i)].GetNbinsX()):
      outparts[str(i)].SetBinContent(k + 1, yodaAO.bin(k).sumW())
      outparts[str(i)+str(i)+str(i)].AddAt(yodaAO.bin(k).sumW2(), k+1)
  else:
    continue
  
  key_name = h_name.split(":")[0] +' '+ fName
  keyn=str(key_name)[:-5]+" "+str(h_name.split(" ")[1])
  rtFile.WriteObject(outparts[str(i)], keyn)
  #outparts[str(i)].Write(name[1:])
  i = i +1
rtFile.Close()



