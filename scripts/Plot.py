import subprocess
import sys

exten=str(sys.argv[1])
exten2=str(sys.argv[2])
# List of arguments for your script
arguments_list = [
    ["~/private/histos/HMDY_13TeV_EFT/NCDY/SM_NPeq0.root", "~/private/SMEFTgen/EVNT_outdir/U35_DY_neutral_current_ee/SM_1p0.0_"+exten+"/test/SM.root",exten2],
    ["~/private/histos/HMDY_13TeV_EFT/NCDY/linear/EFT_cHDD_NPSQeq1.root", "~/private/SMEFTgen/EVNT_outdir/U35_DY_neutral_current_ee/cHDD_1p0.0_"+exten+"/test/cHDD.root",exten2],
    ["~/private/histos/HMDY_13TeV_EFT/NCDY/linear/EFT_cHl1_NPSQeq1.root", "~/private/SMEFTgen/EVNT_outdir/U35_DY_neutral_current_ee/cHl1_1p0.0_"+exten+"/test/cHl1.root",exten2],
    ["~/private/histos/HMDY_13TeV_EFT/NCDY/linear/EFT_cHe_NPSQeq1.root", "~/private/SMEFTgen/EVNT_outdir/U35_DY_neutral_current_ee/cHe_1p0.0_"+exten+"/test/cHe.root",exten2],
    ["~/private/histos/HMDY_13TeV_EFT/NCDY/linear/EFT_clu_NPSQeq1.root", "~/private/SMEFTgen/EVNT_outdir/U35_DY_neutral_current_ee/clu_1p0.0_"+exten+"/test/clu.root",exten2],    
    # Add more sets of arguments as needed
]

# Specify the number of times to run the script with different arguments
num_runs = len(arguments_list)

# Loop to run the script multiple times with different arguments
for args in arguments_list:
    # Call the script using subprocess
    subprocess.call(["python", "Norm_Ratio.py"] + args)

