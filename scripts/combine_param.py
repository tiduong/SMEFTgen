import glob, os
from SMEFTgen.stxs_defs import *
from SMEFTgen.param_helpers import *
import pprint
import math, sys
import yaml

def combineParam(args):

    params = {file: load_param(file) for file in args.input}
    print(load_param(args.input[0]).keys())
    out = { 'name':[],
            'coefficient terms':[],
            'observables':[],
            'parameterisation':{},
            'sm xsec':{} }

    for file0,info in params.items():
        out["coefficient terms"] = out["coefficient terms"] + info["coefficient terms"]
        out["observables"]       = out["observables"]       + info["observables"]

    out["observables"]       = list(set(out["observables"]))
    out["coefficient terms"] = [ x for x in list(set(out["coefficient terms"])) if x not in args.removepars]
    first_file = args.input[0]
    out['sm xsec'] = {obs:{} for obs in out["observables"]} 
    out['parameterisation'] = {obs:{ par : {} for par in out["coefficient terms"]} for obs in out["observables"]} 

    for obs in out['sm xsec']:
        out['sm xsec'][obs] = tupleaverage([info['sm xsec'][obs] for file,info in params.items() if obs in info['sm xsec'].keys()])
        for par in out['parameterisation'][obs]:
            for file,info in params.items():
                if obs in info['parameterisation'].keys():
                    for par in out["coefficient terms"] :
                        if par in info['parameterisation'][obs]:
                            out['parameterisation'][obs][par] = info["parameterisation"][obs][par]

    skim_param = {obs:{ par : out['parameterisation'][obs][par] for par in out["coefficient terms"] if len(out['parameterisation'][obs][par])} for obs in out["observables"]}

    pars = list(set([par  for obs,param in skim_param.items() for par in param]))
    outfilename = args.outfilename
    out["name"] = args.name 
    name = out["name"]
    out["coefficient terms"] = pars
    out["parameterisation"] = skim_param

    with open(outfilename, "w") as f:
        if outfilename.endswith(".json"):
            json.dump(out, f, indent=2)
            print("Write parameterisation of {0} to {1}".format(name, outfilename))
        elif outfilename.endswith(".yaml") or outfilename.endswith(".yml"):
            yaml.dump(out, f, allow_unicode=False, default_flow_style=False)
            print("Write parameterisation of {0} to {1}".format(name, outfilename))
        else:
            print("Please provide yaml or json as output, Nothing written !")
            sys.exit()

#    printpar(out,print_unc=args.printunc)
#    writepar(out,args.name,args.outfilename,threshold=1e-3)

#    histoname = args.histoname
#    if len(args.name) == 0:
#        args.name = args.histoname#

#    vals = {stxsbin:{} for stxsbin in stxsdict}#

#    for filename in files:
#        print("Reading {0}...".format(filename))#

#        if filename.endswith(".root"):
#            vals_sample = readvals_from_ROOT(filename,histoname,apply_int_map,debug)#

#        elif filename.endswith(".yoda"): 
#            vals_sample = readvals_from_yoda(filename,histoname,apply_int_map,debug)
#        
#        else:
#            print("Can handle histograms provided only in .root or .yoda format")#

#        for stxs_bin, parval in vals_sample.items():
#            for par,val in parval.items():
#                if par == "SM" : factor = 1.0
#                else : factor = c_factor
#                vals[stxs_bin][par] = {"val": factor * val["val"] , "err": factor * val["err"]}#

#    printpar(vals,print_unc=args.printunc)
#    writepar(vals,args.name,args.outfilename,threshold=1e-3)

if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser(
        "prepare JOs for Higgs production"
    )
    parser.add_argument(
        "--input",
        type=str,
        nargs="+",
        dest="input",
        help="input yamls",
        required=True
    )
    parser.add_argument(
        "--output-parameterisation",
        type=str,
        dest="outfilename",
        help="out.yaml",
        default="out.yaml",
        required=False
    )
    parser.add_argument(
        "--histogram-name",
        type=str,
        dest="name",
        help="name of the histogram",
        required=False,
        default="/HiggsTemplateCrossSectionsStage12/STXS_stage1_2_fine_pTjet30"
    )
    parser.add_argument(
        "--print-unc",
        action="store_true",
        dest="printunc",
        help="print the unc. of the parameterisation",
        required=False,
        default=False
    )
    parser.add_argument(
        "--no-print-unc",
        action="store_false",
        dest="printunc",
        help="print the unc. of the parameterisation",
        required=False,
        default=False
    )
    parser.add_argument(
        "--param-name",
        type=str,
        dest="name",
        help="name of the parameterisation",
        required=False,
        default=""
    )
    parser.add_argument(
        "--debug",
        action="store_true",
        dest="debug",
        help="enable verbose printing for debugging",
        required=False,
        default=""
    )
    parser.add_argument(
        "--remove-pars",
        dest="removepars",
        help="remove parameters from the parameterisation",
        nargs="+",
        required=False,
        default=[]
    )


    args = parser.parse_args()
    combineParam(args)
