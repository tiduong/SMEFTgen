import json, yaml
import pprint 
from SMEFTgen.param_helpers import *


def print_param(args):

    for input in args.input:
        param = load_param(input)
        printpar(param["parameterisation"],val_threshold=args.val_threshold,print_unc=args.printunc,rel_unc_threshold=args.unc_threshold,select_param=args.select_param)

if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser(
        "merge STXS param production"
    )
    parser.add_argument(
        "--input-param",
        type=str,
        nargs="+",
        dest="input",
        help="input parameterisation",
        required=True
    )
    parser.add_argument(
        "--unc-threshold",
        type=float,
        dest="unc_threshold",
        help="uncertainity threshold above which to isolate the param, typically to check param",
        required=False,
        default = None
    )
    parser.add_argument(
        "--val-threshold",
        type=float,
        dest="val_threshold",
        help="val threshold to print param",
        required=False,
        default = 1e-4
    )
    parser.add_argument(
        "--merging-map",
        type=str,
        dest="map",
        help="infile.yaml",
        default=None,
        required=False
    )
    parser.add_argument(
        "--histogram-name",
        type=str,
        dest="histoname",
        help="name of the histogram",
        required=False,
        default="STXS_stage1_2_fine_pTjet30"
    )
    parser.add_argument(
        "--print-unc",
        action="store_true",
        dest="printunc",
        help="print the unc. of the parameterisation",
        required=False,
        default=False
    )
    parser.add_argument(
        "--no-print-unc",
        action="store_false",
        dest="printunc",
        help="print the unc. of the parameterisation",
        required=False,
        default=False
    )
    parser.add_argument(
        "--select-param",
        nargs="+",
        dest="select_param",
        required=False,
        default=[]
    )


    args = parser.parse_args()
    print_param(args)
