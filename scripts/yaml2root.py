import yaml
import ROOT
import sys

filename=str(sys.argv[0])
# Load the YAML file


with open(filename, "r") as file:
    data = yaml.load(file, Loader=yaml.SafeLoader)
    

values = []
for entry in data['header']['dependent_variables']:
    values.extend([value['value'] for value in entry['values']])

# Determine the range and number of bins for the histogram
min_value = min(values)
max_value = max(values)
num_bins = len(values)

print(min_value,max_value,num_bins)

# Create a ROOT histogram with error bars
histogram = ROOT.TH1F("SM Histo", "SM Histo", num_bins, min_value, max_value)

# Iterate over each entry in the data
for entry in data['header']['dependent_variables']:
    values = entry['values']
    
    # Iterate over each value for the current variable
    for value_entry in values:
        value = value_entry['value']
        total_error = value_entry['errors']['total']
        
        # Fill the histogram with values and errors
        bin_index = histogram.FindBin(value)
        histogram.SetBinContent(bin_index, value)
        histogram.SetBinError(bin_index, total_error)

# Create a ROOT file to save the histogram
root_file = ROOT.TFile("SM.root", "RECREATE")

# Write the histogram to the ROOT file
histogram.Write()

# Close the ROOT file
root_file.Close()

