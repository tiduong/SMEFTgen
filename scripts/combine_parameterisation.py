from SMEFTgen.param_helpers import *
import sys
par1 = load_param(sys.argv[1])
par2 = load_param(sys.argv[2])
outfilename = sys.argv[3]
scale_cuHRe = sys.argv[4]

remove_pars = ["cHe", "cHl1", "cHl3", "cll1"]

name = "out_name"
all_obs = list(set(par1["observables"] + par2["observables"]))
terms   = list(set(par1["coefficient terms"] + par2["coefficient terms"]))


for par in remove_pars:
   if par in terms: terms.remove(par)

#print(all_obs, terms) 
param = {obs : {} for obs in all_obs}

param1 = par1["parameterisation"]
param2 = par2["parameterisation"]
for obs in all_obs:
  for term in terms:
      val1 = {"val":0.0,"err":0.0}
      val2 = {"val":0.0,"err":0.0}

      if obs in param1:
         if term in param1[obs] :
            val1 = param1[obs][term]

      if obs in param2 :
         if term in param2[obs] :
            val2 = param2[obs][term]

      res = tupleadd(val1, val2)
      if res["val"] != 0.0:
         if term == "cH": 
            res["val"] = res["val"] * 0.465
         if scale_cuHRe=="1":
            if term == "cuHRe":
               print(res["val"])
               res["val"] = res["val"] * 98.1098039216
               print(res["val"])
         if term in remove_pars : continue
         param[obs][term] = res

merged_param = {"coefficient terms":terms,
              "name":"merged param",
              "observables":all_obs,
              "parameterisation":param,
              "sm xsec":par1["sm xsec"]}

with open(outfilename, "w") as f:
    yaml.dump(merged_param, f, allow_unicode=False, default_flow_style=False)
    print("Write parameterisation of {0} to {1}".format(name, outfilename))



