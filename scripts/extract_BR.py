from SMEFTgen.param_helpers import *
import pprint
if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser(
        "prepare JOs for Higgs production"
    )
    parser.add_argument(
        "--input",
        type=str,
        dest="input",
        help="input logfile",
        nargs="+",
        required=True,
    )
    parser.add_argument(
        "--output",
        type=str,
        dest="output",
        help="output yaml file",
        required=False,
        default="out.yaml"
    )
    parser.add_argument(
        "--print-unc",
        action="store_true",
        dest="printunc",
        help="print uncertainity",
        required=False,
        default=False
    )
    parser.add_argument(
        "--no-print-unc",
        action="store_false",
        dest="printunc",
        help="print uncertainity",
        required=False,
        default=False
    )
    args = parser.parse_args()
    allvals = extract_width_info(args.input)
    modes = []
    parameters = []
    #{'mode': 'h_za', 'par': 'cuHRe', 'case': 'int', 'err': 0.0, 'unit': 'GeV', 'val': 0.0}
    #{'mode': 'h_za', 'par': 'cW', 'case': 'int', 'err': 0.0, 'unit': 'GeV', 'val': 0.0}
    for case in allvals:
        modes.append(allvals[case]["mode"])

    for case in allvals:
        parameters.append(allvals[case]["par"])

    print(list(set(parameters)))
    print(list(set(modes)))

    vals_per_mode = {mode : {} for mode in modes}

    for case,info in allvals.items():        
        vals_per_mode[info["mode"]][info["par"]] = {"val": info["val"], "err":info["err"]}

#    pprint.pprint(vals_per_mode)
    
    writepar(vals_per_mode,"Higgs width",args.output,threshold=1e-3)
    printpar(vals_per_mode,print_unc=args.printunc)

   
