from SMEFTgen.param_helpers import load_param, checkifdir
from senstiEFT.matrix import load_map,load_matrix,matrix,load_parameterisation
from senstiEFT.matrix_helpers import get_padded_matrix
from collections import OrderedDict
import json,yaml
import pprint

decaymodes = ["ZZ","YY","BB","CC","WW","MUMU","TAUTAU","ZY"]

mode_map = { "ZZ" : "H4L",
             "MLZZ" : "H4L",
             "YY" : "HAA",
             "BB" : "HBB",
             "CC" : "HCC",
             "WW" : "HWWLVLV",
             "MLWW" : "HWWLVLV",
             "VHWW" : "HWWLVLV",
             "MUMU" : "HMUMU",
             "TAUTAU" : "HTAUTAU",
             "MLTT" : "HTAUTAU",
             "ZY" : "HZA"}

def get_acc_corr(acceptance, observable, decay, term):
    Aacc_term = 0
    if acceptance!= None: 
        acceptance = load_param(args.acceptance)
        accmat = acceptance["parameterisation"]
        if "H4L" in accmat.keys():            
            if decay != "H4L":
                print("ERROR: acceptance file is for H4L, but you are using {0}".format(decay))
                sys.exit(1)
                
            if term in accmat["H4L"]:
                #print(term, accmat["H4L"][term])
                if abs(accmat["H4L"][term]["val"]) > accmat["H4L"][term]["err"]:
                    Aacc_term = accmat["H4L"][term]["val"]
        elif "QQ2HQQ|HWWLVLV" in accmat.keys():
            if decay != "HWWLVLV":
                print("ERROR: acceptance file is for HWWLVLV, but you are using {0}".format(decay))
                sys.exit(1)
            ### additional check for the production mode
            if observable.startswith("mu_QQ2HQQ"):
                accdict = accmat["QQ2HQQ|HWWLVLV"]
            elif observable.startswith("mu_GG2H"):
                accdict = accmat["GG2H|HWWLVLV"]
            if term in accdict:
                if abs(accdict[term]["val"]) > accdict[term]["err"]:
                    Aacc_term = accdict[term]["val"]

    return Aacc_term            

def prepare_param(args):
    higgs_decay = args.decay
    higgs_prod = args.production
    inputs = {args.case: {"prod" : higgs_prod, "dec" : higgs_decay}}
    inmats = {x:load_param(mat) for x,mat in inputs[args.case].items()}
    terms = []
    acceptance = args.acceptance 
    for case,mats in inmats.items(): terms = terms + mats["coefficient terms"]    
    xpars = sorted(list(set([ x for x in terms if "*" not in x])))

    ####
    ####
    #### the taylor expansion of production x pw / tw for two parameters
    #### (two parameter case generalises to n parameters)
    ####
    #### Ap1 -> linear coefficient (A) of production (p) for parameters c1 (1)
    ####     .. pw -> partial width, tw -> total width
    ####
    #### B -> pure square term, C -> cross term 
    ####
    ####
 
    ## Linear
    #1 + Ap1 c1 + Apw1 c1 - Atw1 c1 
    #   + Ap2 c2 + Apw2 c2 - Atw2 c2 
    #
    ## Quadratic square
    #+ Ap1 Apw1 c1^2 - Ap1 Atw1 c1^2 - Apw1 Atw1 c1^2 
    #+ Atw1^2 c1^2 + Bp1 c1^2 + Bpw1 c1^2 - Btw1 c1^2 
    #
    #+ Ap2 Apw2 c2^2 - Ap2 Atw2 c2^2 - Apw2 Atw2 c2^2 
    #+ Atw2^2 c2^2 + Bp2 c2^2 + Bpw2 c2^2 - Btw2 c2^2 
    #  
    # Quadratic cross
    #+ c1 c2 Cp12 + c1 c2 Cpw12 - c1 c2 Ctw12 + Ap2 Apw1 c1 c2 
    #+ Ap1 Apw2 c1 c2 - Ap2 Atw1 c1 c2 - Apw2 Atw1 c1 c2 
    #- Ap1 Atw2 c1 c2 - Apw1 Atw2 c1 c2 + 2 Atw1 Atw2 c1 c2 
    #
    ####
     
    poinames = [ypar.replace("eft_int","mu") for ypar in inmats["prod"]["observables"]]
    parmat = matrix("comb_par", [[0.0 for x in xpars] for y in poinames], xpars=xpars, ypars=poinames)
    pardict = OrderedDict()
     
    for poi in poinames:
        pardict[poi] = OrderedDict()
     
    for ypar in parmat.ypars:
        yp = ypar
        ypw  = mode_map[args.case]
        ytw  = "HTOT"
        intmat = inmats["prod"]["parameterisation"][yp]
        decmat = inmats["dec"]["parameterisation"][ypw]
        twmat = inmats["dec"]["parameterisation"][ytw]

        for x1 in xpars:
            Ap_x1, Apw_x1, Atw_x1, Aperr_x1, Aperr_x1sq, Aacc_x1 = 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
            Aacc_x1 = get_acc_corr(acceptance, yp, ypw, x1)
            if Aacc_x1 != 0.0:
                print("Acceptance correction for {0} in {1} is {2}".format(x1,ypar,Aacc_x1))

            if x1 in intmat:
                Ap_x1  = intmat[x1]["val"]
                Aperr_x1  = intmat[x1]["err"]

            if x1 in decmat:
                Apw_x1 = decmat[x1]["val"]

            if x1 in twmat:
                Atw_x1 = twmat[x1]["val"]

            # Linear
            # + Ap1 c1 + Apw1 c1 - Atw1 c1 
            lin_term = Ap_x1 + Apw_x1 + Aacc_x1 
            if args.taylortw:
                lin_term = lin_term - Atw_x1
                
            lin_term_err = Aperr_x1 
            if abs(lin_term)  >= 1e-3: pardict[ypar][x1] = {"val":lin_term, "err":lin_term_err}

            if not args.linear:
                Bp_x1, Bpw_x1, Btw_x1, Bp_x1_err, Bp_x1_err, Bpw_x1_err, Btw_x1_err = 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
                x1_sq = "{0}*{0}".format(x1)
                if x1_sq in intmat.keys():
                    Bp_x1, Bp_x1_err  = intmat[x1_sq]["val"], intmat[x1_sq]["err"]
                    
                if x1_sq in decmat.keys():
                    Bpw_x1  = decmat[x1_sq]["val"]
                    Bpw_x1_err  = decmat[x1_sq]["err"]
                
                if x1_sq in twmat.keys():
                    Btw_x1  = twmat[x1_sq]["val"]
                    Btw_x1_err  = twmat[x1_sq]["err"]
            
                Bacc_x1 = get_acc_corr(acceptance, yp, ypw, x1_sq)
                
                if Bacc_x1 != 0.0:
                    print("Acceptance correction for {0} in {1} is {2}".format(x1_sq,ypar,Bacc_x1))

                ## Quadratic square
                #+ Ap1 Apw1 c1^2 - Ap1 Atw1 c1^2 - Apw1 Atw1 c1^2 
                #+ Atw1^2 c1^2 + Bp1 c1^2 + Bpw1 c1^2 - Btw1 c1^2 

                quad_term = Ap_x1*(Apw_x1+Aacc_x1) + Bp_x1 + (Bpw_x1 + Bacc_x1) 

                if args.taylortw:
                    quad_term = quad_term - Ap_x1*Atw_x1 - (Apw_x1+Aacc_x1)*Atw_x1 + Atw_x1*Atw_x1 - Btw_x1 

                quad_term_err = 0.0                
                if abs(quad_term) >= 1e-5: pardict[ypar][x1_sq] = {"val": quad_term, "err" : quad_term_err}

                for x2 in xpars[xpars.index(x1)+1:]:
                    Ap_x2, Apw_x2, Atw_x2, Cp, Cpw, Ctw = 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
                    Aacc_x2 = get_acc_corr(acceptance, yp, ypw, x2)
                    x1_x2   = "{0}*{1}".format(x1,x2)
                    if x2 in intmat: Ap_x2   = intmat[x2]["val"]
                    if x2 in decmat: Apw_x2  = decmat[x2]["val"]
                    if x2 in twmat : Atw_x2  = twmat[x2]["val"]
                    if x1_x2 in intmat: Cp      = intmat[x1_x2]["val"]
                    if x1_x2 in decmat: Cpw     =  decmat[x1_x2]["val"]
                    if x1_x2 in twmat:  Ctw     =  twmat[x1_x2]["val"]                
                    Aacc_x1_x2 = get_acc_corr(acceptance, yp, ypw, x1_x2)
                    
                    # Quadratic cross
                    #+ c1 c2 Cp12 + c1 c2 Cpw12 - c1 c2 Ctw12 + Ap2 Apw1 c1 c2 
                    #+ Ap1 Apw2 c1 c2 - Ap2 Atw1 c1 c2 - Apw2 Atw1 c1 c2 
                    #- Ap1 Atw2 c1 c2 - Apw1 Atw2 c1 c2 + 2 Atw1 Atw2 c1 c2 
#                    cross_term = Cp + (Cpw + Aacc_x1_x2) - Ctw + Ap_x2*(Apw_x1 + Aacc_x1) + Ap_x1*(Apw_x2 + Aacc_x2) - Ap_x2*Atw_x1 - (Apw_x2 + Aacc_x2)*Atw_x1 - Ap_x1*Atw_x2 - (Apw_x1 + Aacc_x1)*Atw_x2 + 2*Atw_x1*Atw_x2
                    cross_term = Cp + (Cpw + Aacc_x1_x2) + Ap_x2*(Apw_x1 + Aacc_x1) + Ap_x1*(Apw_x2 + Aacc_x2)
                    if args.taylortw:             
                        cross_term = cross_term - Ctw - Ap_x2*Atw_x1 - (Apw_x2 + Aacc_x2)*Atw_x1 - Ap_x1*Atw_x2 - (Apw_x1 + Aacc_x1)*Atw_x2 + 2*Atw_x1*Atw_x2

                    if abs(cross_term) >= 1e-5: pardict[ypar][x1_x2] = {"val": cross_term, "err" : 0.0}

    outfilename = args.out 
    name = args.case
    checkifdir(outfilename)

    outdict = {"name" : name,
              "parameterisation": pardict,
              "coefficient terms": terms,
              "observables": poinames,
              "sm xsec": {binname : 1.0 for binname in pardict}
              }

    with open(outfilename, "w") as f:
       if outfilename.endswith(".json"):
           json.dump(outdict, f, indent=2)
           print("Write parameterisation of {0} to {1}".format(name, outfilename))
       elif outfilename.endswith(".yaml") or outfilename.endswith(".yml"):
           yaml.dump(outdict, f, allow_unicode=False, default_flow_style=False)
           print("Write parameterisation of {0} to {1}".format(name, outfilename))
       else:
           print("Please provide yaml or json as output, Nothing written !")
           sys.exit()

 
if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser("prepare full parametersiation for Higgs")
    parser.add_argument("--decay-mode",type=str,dest="case",help="decay mode to prepare the parameterisation",required=True,metavar="path/to/file")
    parser.add_argument( "-o","--output" , type=str,dest="out",help="path to save parameterisation plot",required=False,default="out.yaml")
    parser.add_argument("--decay-file", type=str,dest="decay",help="path to decay file",required=False,default="./EFTparameterisation/parameterisation/v1/decay/top/linear_SMHLOOP0.yaml")
    parser.add_argument("--production-file", type=str,dest="production",help="path to production file",required=False,default="./EFTparameterisation/parameterisation/v1/STXS_channels/top/Hbb_top.yaml")
    parser.add_argument("--acceptance-correction-file", type=str,dest="acceptance",help="path to file containing acceptance correction",required=False,default=None)
    parser.add_argument("--linear",action="store_true",dest="linear",help="keep only linear terms",required=False,default=True)
    parser.add_argument("--include-quadratic",action="store_false",dest="linear",help="keep quad terms",required=False,default=True)
    parser.add_argument("--taylor-expand-total-width",action="store_true",dest="taylortw",help="taylor expand total width",required=False,default=False)
    parser.add_argument("--no-taylor-expand-total-width",action="store_false",dest="taylortw",help="do not taylor expand total width",required=False,default=False)


    args = parser.parse_args()
    prepare_param(args)
