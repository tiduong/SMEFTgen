from SMEFTgen.param_helpers import *
import pprint

lines = ["Hyy 00 9.574e-06 7.441e-15",
         "Hyy 21 -9.0599e-06 7.0414e-15",
         "Hyy 24 1.16037e-06 9.0185e-16",
         "Hyy 25 -2.3140e-06 1.7985e-15",
         "Hyy 28 -0.0001252 9.7328e-14",
         "Hyy 30 -0.0003844 2.9876e-13",
         "Hyy 32 0.00021446 1.6668e-13",
         "Hyy 35 3.30016e-07 2.5649e-16",
         "Hyy 40 -1.1020e-05 8.5646e-15",
         "Hyy 41 -2.0584e-05 1.5998e-14",
         "Hyy 46 -3.48206e-06 2.7063e-15",
         "Hyy 54 1.74151e-06 1.3535e-15"]

parmap = {  "00" : 'SM',
    "1" : 'cG',
    "2" : 'cW',
    "3" : 'cH',
    "4" : 'cHbox',
    "5" : 'cHDD',
    "6" : 'cHG',
    "7" : 'cHW',
    "8" : 'cHB',
    "9" : 'cHWB',
    "10" : 'cuHRe',
    "11" : 'ctHRe',
    "12" : 'cdHRe',
    "13" : 'cbHRe',
    "14" : 'cuGRe',
    "15" : 'ctGRe',
    "16" : 'cuWRe',
    "17" : 'ctWRe',
    "18" : 'cuBRe',
    "19" : 'ctBRe',
    "20" : 'cdGRe',
    "21" : 'cbGRe',
    "22" : 'cdWRe',
    "23" : 'cbWRe',
    "24" : 'cdBRe',
    "25" : 'cbBRe',
    "26" : 'cHj1',
    "27" : 'cHQ1',
    "28" : 'cHj3',
    "29" : 'cHQ3',
    "30" : 'cHu',
    "31" : 'cHt',
    "32" : 'cHd',
    "33" : 'cHbq',
    "34" : 'cHudRe',
    "35" : 'cHtbRe',
    "36" : 'cjj11',
    "37" : 'cjj18',
    "38" : 'cjj31',
    "39" : 'cjj38',
    "40" : 'cQj11',
    "41" : 'cQj18',
    "42" : 'cQj31',
    "43" : 'cQj38',
    "44" : 'cQQ1',
    "45" : 'cQQ8',
    "46" : 'cuu1',
    "47" : 'cuu8',
    "48" : 'ctt',
    "49" : 'ctu1',
    "50" : 'ctu8',
    "51" : 'cdd1',
    "52" : 'cdd8',
    "53" : 'cbb',
    "54" : 'cbd1'}

remap ={ 'SM'     :'SM' ,
         'cHd'    :'cHWB' ,
         'cHj3'   :'cHW' ,
         'cHtbRe' :'ctHRe' ,
         'cHu'    :'cHB' ,
         'cQj11'  :'ctWRe' ,
         'cQj18'  :'ctBRe' ,
         'cbBRe'  :'cHDD' ,
         'cbGRe'  :'cW' ,
         'cbd1'   :'cll1' ,
         'cdBRe'  :'cHbox' ,
         'cuu1'   :'cHl3' }
decay = {}
for line in lines:
    parts = line.split(" ")
    pprint.pprint(parmap[parts[1]])  
    decay[remap[parmap[parts[1]]]] = {"val": float(parts[2]), "err": float(parts[3]) }

decay["cll1221"] = {"val": decay["cll1"]["val"]*0.5, "err": decay["cll1"]["err"]*0.5}
decay["cHl311"]  = {"val": decay["cHl3"]["val"]*0.5, "err": decay["cHl3"]["err"]*0.5}
decay["cHl322"]  = {"val": decay["cHl3"]["val"]*0.5, "err": decay["cHl3"]["err"]*0.5}
for par in decay :
    print("echo \"09:19:21      Width :   {0} +- {1} GeV\" >> EVNT_outdir_top/h_aa/{2}_int.0/test/log.generate".format(decay[par]["val"], decay[par]["err"], par))
decay_param = {}
for par in decay:
    decay_param[par] = tupledivide(decay[par], decay["SM"])

pprint.pprint(decay)
pprint.pprint(decay_param)
