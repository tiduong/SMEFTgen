import ROOT
import sys
import os



os.environ["DISPLAY"] = ":0"
# Specify the path to the ROOT file
file1path =str(sys.argv[1])

file2path=str(sys.argv[2])

ext=str(sys.argv[3])
output_directory = "/afs/cern.ch/user/t/tiduong/private/SMEFTgen/Cross_check_"+ext+"/"
if not os.path.exists(output_directory):
    os.makedirs(output_directory)

print(file1path)
print(file2path)
# Open the ROOT file
f1 = ROOT.TFile.Open(file1path, "READ")
f2 = ROOT.TFile.Open(file2path, "READ")

coef=os.path.basename(file2path).split(".")[0]
# Check if the file is successfully opened
if f1.IsOpen():
    print("File1 opened successfully.")
else:
    print("Failed to open file1.")
    exit(1)  # Exit the script if file opening failed
if f2.IsOpen():
    print("File2 opened successfully.")
else:
    print("Failed to open file2.")
    exit(1)  # Exit the script if file opening failed


keys1 = f1.GetListOfKeys()
keys2 = f2.GetListOfKeys()

#Specify the string you're looking for in the histogram name
desired_string = "SM"

# Loop through the keys to find the histogram with the desired string in its name
h1 = None
h2 = None
for key1,key2 in zip(keys1,keys2):
    print(key1)
    print(key2)
    if desired_string in key1.GetName() and "mass" in key1.GetName():
        if h1 is None:
            h1 = key1.ReadObj()
            print("Histogram '{}' found".format(key1))
    

       
 

# Check if the histogram is successfully retrieved
if h1:
    print("Histogram 1 with '{}' in its name retrieved successfully.".format(desired_string))
else:
    print("Failed to retrieve histogram 1 with '{}' in its name.".format(desired_string))

if h2:
    print("Histogram 2 with '{}' in its name retrieved successfully.".format(desired_string))
else:
    print("Failed to retrieve histogram 2 with '{}' in its name.".format(desired_string))


h3=h1.Clone()
h3.Reset()
L=[0.35216987,0.16348827,0.07784387,0.04096829,0.02267105,0.01316641,0.00776613,0.00352139,0.00120573,0.00032709,0.00006992,0.00000994,0.00000027]

for i in range(len(L)):
   h3.SetBinContent(i + 1, L[i])
   print(L[i])
   print(h3.GetBinContent(i+1))

i2 = h1.Integral()
print("Int before {}".format(i2))
if i2 != 0:
    h1.Scale(1.0 / i2)
    print("Histogram 1 normalized to 1.")
    print(h1.Integral())
else:
    print("Histogram 1 integral is 0. Skipping normalization.")


i1 = h3.Integral()
print("Int before {}".format(i1))
if i1 != 0:
    h3.Scale(1.0 / i1)
    print("Histogram 3 normalized to 1.")
    print(h3.Integral())
else:
    print("Histogram 3 integral is 0. Skipping normalization.")


max_x1 = h3.GetXaxis().GetXmax()
max_x2 = h1.GetXaxis().GetXmax()
max_x = max(max_x1, max_x2)

max_y1 = h3.GetMaximum()
max_y2 = h1.GetMaximum()
max_y = max(max_y1, max_y2)



c1 = ROOT.TCanvas("canvas", "Histogram Canvas", 800, 600)
legend = ROOT.TLegend(0.65, 0.7, 0.9, 0.9)

#legend.AddEntry(h1,"Ricardo's sample" , "l")
#legend.AddEntry(h2, "New sample", "l")

h3.SetLineColor(ROOT.kBlue)
h1.SetLineColor(ROOT.kRed)
h3.SetStats(0)
h1.SetStats(0)

h3.Sumw2()
h1.Sumw2()

h1.Draw("SAME")

h3.Draw("SAME")

h3.GetXaxis().SetRangeUser(0, max_x)
h1.GetXaxis().SetRangeUser(0, max_x)

h3.GetYaxis().SetRangeUser(0, 2*max_y)
h1.GetYaxis().SetRangeUser(0, 2*max_y)
print(h3[1])
print(h1[1])

legend.Draw()
png_filename = os.path.join(output_directory,"{}.png".format(desired_string))
#c1.SaveAs("~/private/SMEFTgen/Cross_check/{}.png".format(desired_string))
c1.SaveAs(png_filename)
c1.Close()
c2 = ROOT.TCanvas("canvas", "Histogram ratio", 800, 600)
legend2 = ROOT.TLegend(0.65, 0.7, 0.9, 0.9)
c2.SetBatch(True)

h_ratio = h3.Clone()
h_ratio.Sumw2()
legend2.AddEntry(h_ratio,"Ratio Best Known/Ricardo's" , "l")

# Divide the bin contents of the first histogram by the corresponding bin contents of the second histogram
h_ratio.Divide(h1)
max_ratio=h_ratio.GetMaximum()
h_ratio.GetYaxis().SetRangeUser(0,7 )
#print(h_ratio.FindBin(h_ratio.GetMaximum()))
print(max_ratio)

# Draw the ratio histogram
line = ROOT.TLine(h_ratio.GetXaxis().GetXmin()-100, 1, h_ratio.GetXaxis().GetXmax()+100, 1)

# Set line color and style (optional)
line.SetLineColor(ROOT.kBlack)
line.SetLineStyle(2)  # Dashed line

# Draw the line on the canvas

h_ratio.Draw("E")
line.Draw("SAME")

legend2.Draw()

h_ratio.SetTitle("Ratio of Histogram1 over Histogram2")
h_ratio.GetXaxis().SetTitle("m_ll")
h_ratio.GetYaxis().SetTitle("Ratio")
png_filename2 = os.path.join(output_directory,"{}_Ratio.png".format(desired_string))
#c2.SaveAs("~/private/SMEFTgen/Cross_check/{}_Ratio.png".format(desired_string))
c2.SaveAs(png_filename2)
c2.Close()
# Close the ROOT file when done
f1.Close()
f2.Close()

