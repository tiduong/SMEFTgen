from SMEFTgen.param_helpers import *
import sys
import itertools

def merge_param(args):
    print("\n#####------Merging parameterisation------#####")
    outfilename = args.out
    name = args.tag
    args.inputs  = list(itertools.chain(*args.inputs))
    args.scale   = list(itertools.chain(*args.scale))
    args.exclude = list(itertools.chain(*args.exclude))

    inputs = [ load_param(input) for input in args.inputs]
    all_obs, terms = [], []
    for input in inputs: 
        all_obs = all_obs + input["observables"]
        terms = terms + input["coefficient terms"]

    all_obs = sorted(list(set(all_obs)))
    all_terms = sorted(list(set(terms)))

    scale_vals = { scale_inputs.split("=")[0] : float(scale_inputs.split("=")[-1]) for scale_inputs in args.scale}
   
    for exclude_term in args.exclude: 
        if exclude_term in all_terms : all_terms.remove(exclude_term)
    
    param = {obs : {} for obs in all_obs}
    
    for obs in all_obs:
      for term in all_terms:
          val = {"val":0.0,"err":0.0}
          for input in inputs:
             if obs in input["parameterisation"]:
                if term in input["parameterisation"][obs]: 
#                    print(obs,input["name"])
                    val = tupleadd(val, input["parameterisation"][obs][term])
 
          if abs(val["val"]) > args.threshold:
             for par,scale_val in scale_vals.items():
                if term == par: 
                   val["val"] = val["val"] * scale_val
                   val["err"] = val["err"] * scale_val
                elif par in term.split("*"):
                    if term.split("*") == [par, par]:
                       val["val"] = val["val"] * scale_val * scale_val
                       val["err"] = val["err"] * scale_val * scale_val
                    else :
                        val["val"] = val["val"] * scale_val
                        val["err"] = val["err"] * scale_val
                        
             param[obs][term] = val
    
    param_terms = []
    for obs in param:
       for par in param[obs]:
          param_terms.append(par)

    param_terms = sorted(set(list(param_terms)))

#    removed_pars = [ par for par in all_terms if par not in param_terms ]
#    if len(removed_pars) : 
#       print("Following parameters are removed by the threshold({0:.4f}) : {1}".format(args.threshold, ",".join(removed_pars)))


    if not args.nosm:
       sm_xsecs = {}
       for obs in all_obs:
          for input in reversed(inputs):
             if obs in input["sm xsec"]:
                 sm_xsecs[obs] = {"val" : input["sm xsec"][obs]["val"], "err": input["sm xsec"][obs]["err"]}
    else:
       sm_xsecs = {obs : 1.0 for obs in all_obs}

    sm_obs = sorted(list(set([obs for obs in sm_xsecs])))

    merged_param = {"coefficient terms":param_terms,
                    "name":"merged param",
                    "observables":sm_obs,
                    "parameterisation":param,
                    "sm xsec":sm_xsecs}
    #print("Using the SM xsec from {0}".format(args.inputs[0]))
    with open(outfilename, "w") as f:
        yaml.dump(merged_param, f, allow_unicode=False, default_flow_style=False)
        print("Write parameterisation of {0} to {1}".format(name, outfilename))
    
    
    
if __name__ == "__main__":
    from argparse import ArgumentParser

    parser = ArgumentParser(
       "merge parameterisation, keep SM from the first file provided as input" 
    )
    parser.add_argument(
        "-i",
        "--input",
        "--inputs",
        nargs="+",
        action="append",
        type=str,
        dest="inputs",
        help="input yaml files with the parameterisation",
        required=True,
        metavar="path/to/file.yaml",
    )
    parser.add_argument(
        "-o",
        "--output",
        type=str,
        dest="out",
        help="output parameterisation matrix",
        required=True,
        default="out.yaml",
    )
    parser.add_argument(
        "--threshold",
        dest="threshold",
        type=float,
        help="threshold to exclude terms from the param",
        required=False,
        default=0.00
    )
    parser.add_argument(
        "--exclude-term",
        nargs="+",
        dest="exclude",
        action="append",
        help="exclude parameters from the yaml",
        required=False,
        default=[]
    )
    parser.add_argument(
        "--scale-coefficients",
        nargs="+",
        dest="scale",
        action="append",
        help="scale parameters from the yaml",
        required=False,
        default=[],
        metavar="c1=5.0 c2=10.0"
    )
    parser.add_argument(
        "--tag",
        dest="tag",
        type=str,
        help="tag for the parameterisation",
        required=False,
        default="param"
    )
    parser.add_argument(
        "--no-store-sm",
        dest="nosm",
        action="store_true",
        help="do not save the SM values",
        required=False,
        default=False
    )

    args = parser.parse_args()
    merge_param(args)

