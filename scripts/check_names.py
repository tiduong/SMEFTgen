from senstiEFT.matrix import load_matrix, load_parameterisation, matrix
from SMEFTgen.param_helpers import *

param = load_param("eft2022/EFTparameterisation/parameterisation/STXS_channels/top/production_and_decay/param_STXS.yaml")
cov = load_matrix("covariance_STXSxBR_XSxBR_wVHcc.yaml")

print(param["parameterisation"].keys())
matarr = []
for ypar in param["observables"]:
    tmparr = []
    for xpar in param["coefficient terms"]:    
        if xpar not in param["parameterisation"][ypar].keys():
            tmparr.append(0.0)
        else: tmparr.append(param["parameterisation"][ypar][xpar])

    matarr.append(tmparr)

mat = matrix(param["name"], matarr, xpars = param["coefficient terms"], ypars = param["observables"])

for ypar in cov.ypars:
    if ypar not in mat.ypars:
        print(ypar)

    else:
        print("FOUND "+ypar)
