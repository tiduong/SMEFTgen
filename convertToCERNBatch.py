import os, sys

# Usage: python convertToCERNBatch.py <script_name>.sh
# Produces batch inputs in directory: batch_submission/<script_name>/
# To submit to CERN batch:
# cd batch_submission/<script_name>/
# condor_submit batch_<script_name>.submit



if len(sys.argv) != 3:
        print("Need to specify script name and extension")
        sys.exit(1)

extension = sys.argv[2]
                             
script_name = sys.argv[1]
batch_dir = 'batch_submission/'+script_name[:-3]

if(not os.path.exists('batch_submission')):
    os.mkdir('batch_submission')

if(not os.path.exists(batch_dir)):
    os.mkdir(batch_dir)

script = open(script_name,'r')

job_list = []

for line in script:
    
    if(line[0:4] == 'bsub'):
        
        njob = line.split()[5][-1:]

        job_id = line.split()[2][17:-3] + '.' + str(njob)
        # add job_id to job_list.txt
        job_list.append(job_id)

        # find start of quotation marks
        strt = line.find('"source')
        command = line[strt:]
        command = command[1:-2]

        # get rid of buggy quotation marks
        command = command.replace(r'\"','"')
	
	#fix *.* into .h
#	command = command.replace(r'HiggsTemplateCrossSectionsStage12*.*','HiggsTemplateCrossSectionsStage12.h')

	command = command.replace(r'HiggsTemplateCrossSectionsStage12.cc','/afs/cern.ch/user/t/tiduong/private/SMEFTgen/rivet/HiggsTemplateCrossSectionsStage12.cc')
         
        change=line.split()[2][:-3]

        command = command.replace(change,change+'_'+str(extension))

	command = command.replace(r'rivet-buildplugin','rivet-build')
        # fix directory paths
        fixed_dir  = ''
        for string in line.split():
            if('EVNT_outdir' in string):
                fixed_dir = string
                end = fixed_dir.find('mc.')
                fixed_dir = fixed_dir[:end] + job_id
                fixed_dir = fixed_dir.replace('JOs/','')
                break
	   
			

        command = command.replace('/afs/cern.'+str(njob),fixed_dir+"_"+str(extension))
        command = command.replace('rivetfile',job_id[:-6])
        longe=len(str(extension))
        
        command_parts = command.split('&&')
        #print(command_parts[2][:-9])
        indice=9+longe+4
        ind2=indice+len(job_id)+15
        #print(command_parts[2][:-indice])
        dir2=command_parts[2][4:-ind2]
        command_parts.insert(1, command_parts[2][:-indice]+'.py'+' '+dir2+change+'_'+str(extension)+'.py ')
        command = '&&'.join(command_parts)
        
        
        # write to .sh file
        out = open(batch_dir+'/'+job_id+'.sh','w')
        out.write('#!/bin/bash \n')
        out.write('cd /afs/cern.ch/user/t/tiduong/private/SMEFTgen/ \n')
        out.write('source build/setup.sh \n')
        #print(command)
        #print("///////")
        
        out.write(command)
        print(job_id)
        print(fixed_dir)
        out.write("cd "+fixed_dir+"_"+str(extension)+'/test && ')
        out.write("python ~/private/SMEFTgen/scripts/yoda2root.py "+job_id[:-6]+'.yoda')
        out.close()
     

# write jobs to job_list.txt
jlist = open(batch_dir+'/job_list.txt','w')
for job in job_list:
    jlist.write(job+'\n')
jlist.close()


submit="""
universe       = vanilla
executable     = $(jobname).sh
output         = $(jobname).out
error          = $(jobname).err
log            = $(jobname).log
stream_error   = True
stream_output  = True
+JobFlavour    = "espresso"
requirements   = OpSysAndVer == "CentOS7"
JobBatchName   = jobs_test.$(ClusterId).$(ProcId)
queue jobname from job_list.txt
"""

submission = open(batch_dir+'/batch_'+script_name[:-3]+'.submit','w')
submission.write(submit)
submission.close()

print("Made batch submission scripts in: "+batch_dir+'/')

