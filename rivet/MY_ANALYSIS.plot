BEGIN PLOT /MY_ANALYSIS/d01-x01-y01
#Title=[Insert title for histogram d01-x01-y01 here]
XLabel=$m_{\ell\ell}$ [GeV]
YLabel=$\frac{\text{d} \sigma}{\text{d} \text{d}m_{\ell\ell}}$ [pb/GeV]
# + any additional plot settings you might like, see make-plots documentation
END PLOT

# ... add more histograms as you need them ...
