// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include "Rivet/Projections/DirectFinalState.hh"
#include "Rivet/Projections/ZFinder.hh"
namespace Rivet {


  /// @brief Add a short analysis description here
  class MY_ANALYSIS : public Analysis {
  public:
  
    /// Constructor
    RIVET_DEFAULT_ANALYSIS_CTOR(MY_ANALYSIS);
  

    /// @name Analysis methods
    /// @{

    /// Book histograms and initialise projections before the run
    void init() {

      // Initialise and register projections

      // The basic final-state projection:
      // all final-state particles within
      // the given eta acceptance
      _mode = 0;
      if ( getOption("LMODE") == "EL" ) _mode = 0;
      if ( getOption("LMODE") == "MU" ) _mode = 1;
 
      // Configure projections
      FinalState fs;
      Cut cuts = Cuts::abseta < 2.5 && Cuts::pT > 30*GeV;
      ZFinder zfinder(fs, cuts, (_mode ? PID::MUON : PID::ELECTRON),
	                          116*GeV, 5000*GeV, 0.1, ZFinder::ClusterPhotons::NODECAY, ZFinder::AddPhotons::NO);
      declare(zfinder, _mode ? "ZFinder_mu" : "ZFinder_el");
      vector<double> mll_bins = {116, 130, 150, 175, 200, 230, 260, 300, 380, 500,700,1000,1500,5000};  
      vector<double> pT_bins = {0,10,20,30,40,50,60,70,80,90,100,200,300,400,500,600,700,800,900,1000};
      vector<double> eta_bins = {0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0,1.2,1.4,1.6,1.8,2.0,2.25,2.5,2.75,3,4,5,6,7,8,9};
      // Book histograms
      // specify custom binning
       //   book(_h["XXXX"], "myh1", 20, 0.0, 100.0);
       //book(_h["YYYY"], "myh2", logspace(20, 1e-2, 1e3));
       //book(_h["ZZZZ"], "myh3", {0.0, 1.0, 2.0, 4.0, 8.0, 16.0});
      // take binning from reference data using HEPData ID (digits in "d01-x01-y01" etc.)
      book(_h["mass"], "myh_mll", mll_bins);
      book(_h0["pT"], "myh_pT0", linspace(100,0,500));
      book(_h1["pT"], "myh_pT1", linspace(100,0,500));
      book(_hnc["mass_nc"], "myh_mll_nc", mll_bins);
      book(_e0["eta"], "myh_eta0", linspace(100,0,7));
      book(_e1["eta"], "myh_eta1", linspace(100,0,7));
      // book(_c["CCCC"], 3, 1, 1);

    }


    /// Perform the per-event analysis
    void analyze(const Event& event) {
      // Retrieve dressed leptons, sorted by pT

      //cout<<"Enter"<<endl;
      const ZFinder& zfinder = apply<ZFinder>(event, _mode ? "ZFinder_mu" : "ZFinder_el");
      //cout<<"bosons_size="<<zfinder.bosons().size()<<endl;
      

      
      if (zfinder.bosons().size() != 1 ) vetoEvent;
      //cout<<"boson_size Veto passed"<<endl;

      const Particle& Zboson = zfinder.boson();
      const double zmass = Zboson.mass();
      const double zrap  = Zboson.absrap();
      const Particles& leptons = zfinder.constituents();
      
      
     
      
      
 
      
    
      if (leptons.size() != 2 || leptons[0].charge3() * leptons[1].charge3() > 0) vetoEvent; 
      //cout<<"lepton_size & charge_X Veto pass"<<endl;
      //cout<<"lepton0_pT="<<leptons[0].pT()<<endl;

      //cout<<"lepton1_pT="<<leptons[1].pT()<<endl;

      if(leptons[0].pT()<30*GeV || leptons[1].pT()<30*GeV) vetoEvent;
      if(leptons[1].pT()<40*GeV && leptons[0].pT()<40*GeV) vetoEvent;

      //cout<<"lepton_pT Veto pass"<<endl;

      //cout<<"zmass="<<zmass<<endl;
      if(zmass<116) vetoEvent;
      //cout<<"zmass Veto pass"<<endl;
      // Fill histogram with leading b-jet pT
      //cout<<"Filling histogram"<<endl;
      _hnc["mass_nc"]->fill(zmass/GeV);
      _h["mass"]->fill(zmass/GeV);
      if (leptons[0].pT()>leptons[1].pT()) 
      {_h0["pT"]->fill(leptons[0].pT()/GeV);
      _h1["pT"]->fill(leptons[1].pT()/GeV);}
      else{_h0["pT"]->fill(leptons[1].pT()/GeV);
      _h1["pT"]->fill(leptons[0].pT()/GeV);}
      _e0["eta"]->fill(leptons[0].abseta());
      _e1["eta"]->fill(leptons[1].abseta());
    }


    /// Normalise histograms etc., after the run
    void finalize() {
      normalize(_e0["eta"]);
      normalize(_e1["eta"]); 
      normalize(_h0["pT"]); // normalize to unity
      normalize(_h1["pT"]);
      normalize(_hnc["mass_nc"], crossSection()/picobarn/sumW()); // normalize to generated cross-section in pb (no cuts)
      scale(_h["mass"], crossSection()/picobarn/sumW()); // norm to generated cross-section in pb (after cuts)

    }

    /// @}
protected:
    size_t _mode;
private:

    /// @name Histograms
    /// @{
    map<string, Histo1DPtr> _h;
    map<string, Profile1DPtr> _p;
    map<string, CounterPtr> _c;
    map<string, Histo1DPtr> _h0;
    map<string, Histo1DPtr> _h1;
    map<string, Histo1DPtr> _hnc;   
    map<string, Histo1DPtr> _e0;
    map<string, Histo1DPtr> _e1;
    
    /// @}


  };


  RIVET_DECLARE_PLUGIN(MY_ANALYSIS);

}
