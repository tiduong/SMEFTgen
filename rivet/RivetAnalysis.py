theApp.EvtMax = -1
#theApp.EvtMax = 5000

import AthenaPoolCnvSvc.ReadAthenaPool 
svcMgr.EventSelector.InputCollections = [inFileName] 

import os

from AthenaCommon.AlgSequence import AlgSequence
job = AlgSequence()

from Rivet_i.Rivet_iConf import Rivet_i

rivet = Rivet_i()
rivet.AnalysisPath = os.environ['PWD']
#rivet.Analyses += [ 'HiggsTemplateCrossSectionsStage12' ]
#rivet.Analyses += [ 'ATLAS_2020_I1790439' ]
rivet.Analyses += [ 'MY_ANALYSIS:LMODE=EL' ]
rivet.RunName = ""
rivet.HistoFile = histoFileName
job += rivet
rivet.SkipWeights=True
from GaudiSvc.GaudiSvcConf import THistSvc 
ServiceMgr += THistSvc() 
ServiceMgr.THistSvc.Output = ["Rivet DATAFILE='"+histoFileName+".root' OPT='RECREATE'"]

