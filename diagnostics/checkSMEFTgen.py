#! /usr/bin/env python

from __future__ import print_function

import optparse, sys, math, subprocess, os, fnmatch, shutil
from collections import OrderedDict
from csv import writer

parser = optparse.OptionParser(usage=__doc__)
parser.add_option("-p", "--path", default="log.generate", dest="INPUT_PATH", metavar="PATH",   help="input logfile")
parser.add_option("-N", "--Ntotal", default=0, dest="TOTAL_EVENTS", metavar="", help="Total number of events")
parser.add_option("-c", "--nocolour", action="store_true", dest="NO_COLOUR", default=False, help="Turn off colour for copying to file")
parser.add_option("-x", "--csv", action="store", dest="OUTPUT_CSV", default="test.csv", help="Output csv file")

opts, fileargs = parser.parse_args()

MCJobOptions='JobOptions' ## check why global definition?
topJO=''

def sherpaChecks(logFile):    
    file=open(logFile,"r")
    lines=file.readlines()    
    # check each line
    inside = 0
    numexceeds =0
    for line in lines:
        if "exceeds maximum by" in line:
            numexceeds +=1
            loginfo("- "+line.strip(),"")
        if "Retried events" in line:
            inside = 1
            continue
        if inside:
            if "}" in line:
                break
            if len(line.split('"')) == 1:
                break
            if len(line.split('->'))== 1:
                break
            name = line.split('"')[1]
            percent = line.split('->')[1].split("%")[0].strip()
            if float(percent) > 5.:
                logwarn("- retried events "+name+" = ",percent+" % <-- WARNING: more than 5% of the events retried")
            else:
                loginfo("- retried events "+name+" = ",percent+" %")    
    if opts.TOTAL_EVENTS:
        if numexceeds*33>int(opts.TOTAL_EVENTS):
            logwarn("","WARNING: be aware of: "+str(numexceeds*100./int(opts.TOTAL_EVENTS))+"% of the event weights exceed the maximum by a factor of ten")

def pythia8Checks(logFile,generatorName):
    file=open(logFile,"r")
    lines=file.readlines()
    usesShowerWeights = False
    usesMatchingOrMerging = False
    usesCorrectPowheg = False
    errors = False
    for line in lines:
        if "Pythia8_ShowerWeights.py" in line:
            usesShowerWeights = True
        if "Pythia8_aMcAtNlo.py" in line or "Pythia8_CKKWL_kTMerge.py" in line or "Pythia8_FxFx.py" in line:
            usesMatchingOrMerging = True
        if "Pythia8_Powheg_Main31.py" in line:
            usesCorrectPowheg = True
    if usesShowerWeights and usesMatchingOrMerging:
        logerr("ERROR:","Pythia 8 shower weights buggy when using a matched/merged calculation. Please remove the Pythia8_ShowerWeights.py include.")
        errors = True
    if "Powheg" in generatorName and not usesCorrectPowheg:
        logerr("ERROR:",generatorName+" used with incorrect include file. Please use Pythia8_Powheg_Main31.py")
        errors = True
    if not errors:
        loggood("INFO: Pythia 8 checks:","Passed")		

def herwig7Checks(logFile,generatorName,metaDataDict):
    errors = False
    allowed_tunes=['H7.1-Default', 'H7.1-SoftTune', 'H7.1-BaryonicReconnection']
    if "7.1" in generatorName:
        if metaDataDict['generatorTune'][0] not in allowed_tunes:
            logerr("ERROR:", "Metadata tune set to {0}, which is not in the list of allowed tunes: {1}".format(metaDataDict['generatorTune'][0], allowed_tunes))
            errors = True
        file=open(logFile,"r")
        lines=file.readlines()
        for line in lines:
           if "Herwig7_EvtGen.py" in line:
               logerr("ERROR:","Herwig 7.1 used with wrong include: Herwig7_EvtGen.py. Please use Herwig71_EvtGen.py instead.")
               errors = True	
               break       
    if not errors:
        loggood("INFO: Herwig 7 checks:","Passed")

def generatorChecks(logFile, generatorName,metaDataDict):
    print ("")
    print ("-------------------------")
    print ("Generator specific tests: ", generatorName)
    print ("-------------------------")
    if "Sherpa" in generatorName:
        sherpaChecks(logFile)
    if "Pythia8" in generatorName:
        pythia8Checks(logFile,generatorName)
    if "Herwig7" in generatorName:
       herwig7Checks(logFile,generatorName,metaDataDict)

def main():
    """checkSMEFTgen.py script for parsing log.generate files to check SMEFT generation settings <oliver.rieger@cern.ch> Sep 2022
    Based on logParser.py written by Josh McFayden <mcfayden@cern.ch> Nov 2016 """

    if opts.INPUT_PATH=="log.generate":
        print ("")
        print ("---------------------")
        print ("Path to log.generate:")
        print ("---------------------")
        print('Looking for log.generate in current working directory : ', os.getcwd())        
    
    # define dictionaries with keys as variables to be searched for and values to store the results
    JOsDict={
        'using release':[],
        'jobOptions':[]
        }
    
    testHepMCDict={
        'Events passed':[],
        'Efficiency':[]
        }
    
    countHepMCDict={
        'Events passing all checks and written':[]
        }
    
    evgenFilterSeqDict={
        'Weighted Filter Efficiency':[],
        'Filter Efficiency':[]
        }
    
    simTimeEstimateDict={
        'RUN INFORMATION':[]
        }
    
    metaDataDict={ 
        'physicsComment =':[],
        'generatorName =':[],
        'generatorTune':[],
        'keywords =':[],
        'specialConfig =':[],
        'contactPhysicist =':[],
#        'randomSeed':[],
        'genFilterNames = ':[],
        'cross-section (nb)':[],
        'generator =':[],
        'weights =':[],
        'PDF =':[],
        'GenFiltEff =':[],
        'sumOfNegWeights =':[],
        'sumOfPosWeights =':[]
        }
    
    generateDict={
        'minevents':[1000]
        }
    
    perfMonDict={
        'snapshot_post_fin':[],
        'jobcfg_walltime':[],
        'last -evt vmem':[]
        }

    testDict = {
       'TestHepMC':testHepMCDict,
       'EvgenFilterSeq':evgenFilterSeqDict,
       'CountHepMC':countHepMCDict,
       # 'SimTimeEstimate':simTimeEstimateDict
       }
    
    # open and read log file
    inputPATH="log.generate"
    if not opts.INPUT_PATH=="log.generate":
        inputPATH=opts.INPUT_PATH+"/log.generate"
        print(inputPATH)
        # inputPATH=os.getcwd()+"/"+opts.INPUT_PATH+"/log.generate"

    # if not os.path.isfile("log.generate"):
    #     print ("")
    #     print ("---------------------")
    #     print ("ERROR:  log.generate not found!")
    #     print ("---------------------")
    #     parser.print_help()
    #     return 

    file=open(inputPATH,"r")
    lines=file.readlines()
    
    # check each line
    for line in lines:
        checkLine(line,'Py:Athena',JOsDict,'INFO')
        checkLine(line,'MetaData',metaDataDict,'=')
        checkLine(line,'Py:Generate',generateDict,'=')
        checkLine(line,'Py:PerfMonSvc',perfMonDict,':')
        checkLine(line,'PMonSD',perfMonDict,'---')
        checkLine(line,'TestHepMC',testHepMCDict,'=')
        checkLine(line,'Py:EvgenFilterSeq',evgenFilterSeqDict,'=')
        checkLine(line,'CountHepMC',countHepMCDict,'=')
        # checkLine(line,'SimTimeEstimate',simTimeEstimateDict,'|')
    
    # print results
    JOsErrors=[]
    print ("")
    print ("---------------------")
    print ("jobOptions and release:")
    print ("---------------------")

    #Checking jobOptions
    JOsList=findJOs('*SMEFT*.py', '.')
    # print (len(JOsList))
    if not len(JOsList):
        print("No JobOptions found")
    else:    
        # topJO=''
        nTopJO=0
        # loginfo( '- jobOptions =',JOsList)
        for jo in JOsList:
            pieces=jo.replace('.py','').split('/')
            # print(pieces)
            if len(pieces) : 
                ##This is top JO
                nTopJO=nTopJO+1
                topJO=pieces[2]
                DSID=pieces[1]

    
        if nTopJO!=1:
            logerr( "","ERROR: !=1 (%i) \"top\" JO files found!"%nTopJO)
            raise RuntimeError("!= 1 \"top\" JO file found")
        else:
            loginfo( '- jobOptions =',topJO)
    
    
    #Checking release
    release="not found"
    if not len(JOsDict['using release']):
        JOsErrors.append(JOsDict['using release'])
    else:
        name='using release'
        tmp=JOsDict[name][0].replace('using release','').strip().split()[0]
        val=tmp.replace('[','').replace(']','')
        branch=val.split('-')[0]
        release=val.split('-')[1]
        blacklisted=checkBlackList(branch,release) 
        if blacklisted:
            logerr( '- '+name+' = ',"".join(val)+" <-- ERROR: %s"%blacklisted)
        else:
            loggood( '- '+name+' = ',"".join(val))   

    if len(JOsErrors):
        print ("---------------------")
        print ("MISSING JOs:")
        for i in JOsErrors:
            logwarn("","ERROR: %s is missing!"%i)
    
    
    ###
    generateErrors=[]
    print ("")
    print ("---------------------")
    print ("Generate params:")
    print ("---------------------")
    for key in generateDict:
        name=key
        val=generateDict[key]
    
        if not len(val):
            generateDict.append(name)
        else:
            if key == 'minevents':
                tmp=str(val[0]).split('#')[0].strip()
                generateDict['minevents']=tmp
                val=tmp
                    
            loginfo( '- '+name+' = ',"".join(val))
    
    if len(generateErrors):
        print ("---------------------")        
        print ("MISSING Generate params:")
        for i in generateErrors:
            logerr("","ERROR: %s is missing!"%i)
            
    
    ###
    metaDataErrors=[]
    print ("")
    print ("---------------------")
    print ("Metadata:")
    print ("---------------------")
    for key in metaDataDict:
        name=key.replace("=","").strip()
        val=metaDataDict[key]
        if not len(val):
            metaDataErrors.append(name)
        else:
            if name=="contactPhysicist":
                if '@' in "".join(val):
                    loggood( '- '+name+' = ',"".join(val))
                else:
                    logwarn( '- '+name+' = ',"".join(val)+"  <-- WARNING: No email found")
                continue
            elif name=="cross-section (nb)":
                if float(val[0]) < 0:
                    logwarn( '- '+name+' = ',"".join(val)+"  <-- WARNING: Cross-section is negative")
                    continue
            elif name=="GenFiltEff":
                if float(val[0]) < 1E-5:
                    logwarn( '- '+name+' = ',"".join(val)+"  <-- WARNING: Filter efficiency too low")
                    continue
                elif float(val[0]) < 5E-2:
                    logwarn( '- '+name+' = ',"".join(val)+"  <-- WARNING: Low filter efficiency")
                    continue                

            loginfo( '- '+name+' = ',"".join(val))
    
    if len(metaDataDict["sumOfPosWeights ="]) and len(metaDataDict["sumOfNegWeights ="]):
        ratio = float(metaDataDict["sumOfNegWeights ="][0])*1.0/(float(metaDataDict["sumOfPosWeights ="][0]) + float(metaDataDict["sumOfNegWeights ="][0]))
        if ratio>0.15:
            logwarn( '- sumOfNegWeights/(sumOfPosWeights+sumOfNegWeights) = ',str(ratio)+"  <-- WARNING: more than 15% of the weights are negative")


    if len(metaDataErrors):
        print ("---------------------")
        print ("MISSING Metadata:")
        for i in metaDataErrors:
            if i=="weights" or i=="genFilterNames" or i=="generator" or i=="PDF" or i=="sumOfNegWeights" or i=="sumOfPosWeights":
                loginfo("INFO:","%s is missing"%i)
            else:
                logerr("","ERROR: %s is missing!"%i)
            

    # Generator specific tests
    # First find generator first
    generatorName=metaDataDict['generatorName ='][0]
    generatorChecks(inputPATH, generatorName,metaDataDict)
    
    ####
    cpuPerJob=0.0
    perfMonErrors=[]
    print ("")
    print ("---------------------")
    print ("Performance metrics:")
    print ("---------------------")
    for key in perfMonDict:
        name=key
        val=perfMonDict[key]
        if not len(val):
            perfMonErrors.append(name)
        else:
    
            if key == 'snapshot_post_fin':
                name = 'CPU'
                tmp = 0.
                tmp=float(val[0].split()[3])
                if len(perfMonDict['jobcfg_walltime']):
                    tmp+=float(perfMonDict['jobcfg_walltime'][0].split()[1].split('=')[1])
                tmp=tmp/(1000.*60.*60.)
                cpuPerJob=tmp
                
                val = "%.2f hrs"%tmp
                if tmp > 18.: 
                    logerr( '- '+name+' = ',"".join(val))
                else:
                    loggood( '- '+name+' = ',"".join(val))
    
            if key == 'last -evt vmem':
                name = 'Virtual memory'
                tmp=float(val[0].split()[0])
                if tmp > 2048.: 
                    logerr( '- '+name+' = ',"".join(val))
                else:
                    loggood( '- '+name+' = ',"".join(val))
    
    
    if len(perfMonErrors):
        print ("---------------------")     
        print ("MISSING Performance metric:")
        for i in perfMonErrors:
            print ("ERROR: %s is missing!"%i)     
 
    
    testErrors=[]
    filt_eff=1.0
    print ("")
    print ("---------------------")
    print ("Event tests:")
    print ("---------------------")
    for dictkey in testDict:
        for key in testDict[dictkey]:
            name=key
            val=testDict[dictkey][key]
            if not len(val):
                testErrors.append("%s %s"%(dictkey,name))
            else:
                #Check final Nevents processed
                if dictkey=="CountHepMC":
                    allowed_minevents_lt1000 = [1, 2, 5, 10, 20, 25, 50, 100, 200, 500]
                    if int(val[0])%1000!=0 and not int(val[0]) in allowed_minevents_lt1000:
                        logerr( '- '+dictkey+" "+name+' = ',"".join(val)+"  <-- ERROR: Not an acceptable number of events for production")
                    elif int(val[0]) != int(generateDict['minevents']):
                        logerr( '- '+dictkey+" "+name+' = ',"".join(val)+"  <-- ERROR: This is not equal to minevents")
                    else:
                        loggood( '- '+dictkey+" "+name+' = ',"".join(val))
                    continue
		
                #Check filter efficiencies aren not too low
                if dictkey=="EvgenFilterSeq":
                    if name=="Weighted Filter Efficiency":
                        filt_eff=float(val[0].split()[0])
                    if float(val[0].split()[0])<0.01:
                        logerr( '- '+dictkey+" "+name+' = ',"".join(val))
                    else:
                        loggood( '- '+dictkey+" "+name+' = ',"".join(val))
                    continue
    
                if dictkey=="TestHepMC" and name=="Efficiency":
                    if float(val[0].replace('%',''))<100. and float(val[0].replace('%',''))>=98.:
                        logwarn( '- '+dictkey+" "+name+' = ',"".join(val))
                    elif float(val[0].replace('%',''))<100.:
                        logerr( '- '+dictkey+" "+name+' = ',"".join(val))
                    else:
                        loggood( '- '+dictkey+" "+name+' = ',"".join(val))
                    continue
    
    
                loginfo( '- '+dictkey+" "+name+' = ',"".join(val))
    
    # if len(testErrors):
    #     print ("---------------------")
    #     print ("Failed tests:")
    #     for i in testErrors:
    #         if i =="SimTimeEstimate RUN INFORMATION":
    #             logwarn("","WARNING: %s is missing!"%i)
    #         else:
    #             if "TestHepMC" in i and "Sherpa" in metaDataDict['generatorName ='][0]:
    #                 logwarn("","WARNING: %s is missing, but expected as it's Sherpa!"%i)
    #             else:
    #                 logerr("","ERROR: %s is missing!"%i)
    
    
    ## Add equivalent lumi information
    if opts.TOTAL_EVENTS:
        print ("")
        print ("")
        print ("---------------------") 
        print (" Others:")
        print ("---------------------")

    xs_nb=0.0
    eff_lumi_fb=0.0

    if opts.TOTAL_EVENTS:
        xs_nb=float(metaDataDict['cross-section (nb)'][0])
        eff_lumi_fb=float(opts.TOTAL_EVENTS)/(1.E+06*xs_nb*filt_eff)
        if eff_lumi_fb > 1000.:
            logwarn("- Effective lumi (fb-1):",str(eff_lumi_fb)+" <-- WARNING: very high effective luminosity")
        elif eff_lumi_fb < 40.:
            logwarn("- Effective lumi (fb-1):",str(eff_lumi_fb)+" <-- WARNING: low effective luminosity")
        else:
            loggood("- Effective lumi (fb-1):",str(eff_lumi_fb))
        minevents=int(generateDict['minevents'])
        # int(countHepMCDict['Events passing all checks and written'][0])
        loginfo("- Number of jobs:",int(opts.TOTAL_EVENTS)/minevents)
        if int(opts.TOTAL_EVENTS) <= 5000:
            logwarn("- Total no. of events:",opts.TOTAL_EVENTS+" <-- WARNING: This total is low enough that the mu profile may be problematic - INFORM MC PROD")
    
    # if not opts.TOTAL_EVENTS:
    #     print ("")
    #     print ("")
    #     print ("---------------------")
    #     print ("Incomplete tests:")
    #     logwarn("","WARNING: --Ntotal (-N) flag is not used - total number of events not given - impossible to calculated effective lumi.")

    # Print total number of Errors/Warnings
    print ("")
    print ("---------------------")
    print (" Summary:")
    print ("---------------------")
    if (LogCounts.Errors == 0):
        if (LogCounts.Warnings == 0):
            loggood("Errors : "+str(LogCounts.Errors)+" , Warnings : "+str(LogCounts.Warnings)," -> OK for production")
        else:
            loggood("Errors : "+str(LogCounts.Errors)+" , Warnings : "+str(LogCounts.Warnings)," -> Some warnings encountered, check that these are ok before submitting production!")
    else:
        logerr("Errors : "+str(LogCounts.Errors)+" , Warnings : "+str(LogCounts.Warnings)," -> Errors encountered! Not ready for production!")  
    print ("")


    #Write csv file output
    cols=['Brief desciption','JobOptions','CoM energy [GeV]','Events (Evgen-only)','Events (FullSim)','Events (Atlfast II)','Priority','Output formats','Cross section [pb]','Effective luminosity [fb-1]','Filter efficiency','Evgen CPU time/job [hr]','Input files','MC-tag','Release','Comments','Evgen tag','Simul tag','Merge tag','Digi tag','Reco tag','Rec Merge tag','Atlfast tag','Atlf Merge tag']
    row=[]
    row=pad(row,24,"")
    # print(topJO)
    row[cols.index('JobOptions')]=topJO
    #row[cols.index('CoM energy [GeV]')]=13000.
    #row[cols.index('Events (Evgen-only)')]=opts.TOTAL_EVENTS
    if opts.TOTAL_EVENTS:
        row[cols.index('Events (Evgen-only)')]=str(opts.TOTAL_EVENTS)+" (CHECK MANUALLY)"
    row[cols.index('Cross section [pb]')]=xs_nb*1000.
    if opts.TOTAL_EVENTS:
        row[cols.index('Effective luminosity [fb-1]')]=eff_lumi_fb
    row[cols.index('Filter efficiency')]=filt_eff
    row[cols.index('Evgen CPU time/job [hr]')]=cpuPerJob
    row[cols.index('Release')]=release
    #for n,c in enumerate(cols):
    #    print c,row[n]
    outCSV=open(opts.OUTPUT_CSV,'w')
    outCSVwriter=writer(outCSV)
    outCSVwriter.writerow(row)
    outCSV.close()
        
    return 
    
def findJOs(pattern, path):
    result = []
    for root, dirs, files in os.walk(path):
        for name in files:
            # print (name)
            if fnmatch.fnmatch(name, pattern):
                result.append(os.path.join(root, name))
                # print(result)
    return result

def checkBlackList(branch,release) :
    ## Black List Caches MC15 STATUS 12.9.2022
    tmpblackfile = ["AtlasProduction, 19.2.3.8, MadGraph",
                    "AtlasProduction, 19.2.3.9, Powheg",
                    "AtlasProduction, 19.2.3.10, Powheg",
                    "MCProd,          19.2.3.10.2, EvtGen",
                    "MCProd,          19.2.3.10.3, EvtGen",
                    "AtlasProduction, 19.2.4.1, EvtGen",
                    "MCProd,          19.2.4.3.1, MadGraph",
                    "AtlasProduction, 19.2.4.4, MadGraph",
                    "AtlasProduction, 19.2.4.5, MadGraph ",
                    "AtlasProduction, 19.2.4.11, Powheg, https://its.cern.ch/jira/browse/AGENE-1058",
                    "MCProd,          19.2.4.16.4, Pythia8pdf6plugin, pdf6plugin did not work, so still LHAPDF5 functionality",
                    "AtlasProduction, 19.2.5.1, Herwigpp",
                    "AtlasProduction, 19.2.5.2, Herwigpp",
                    "AtlasProduction, 19.2.5.3, Herwigpp",
                    "AtlasProduction, 19.2.5.4, Herwigpp",
                    "AtlasProduction, 19.2.5.5, Herwigpp",
                    "AtlasProduction, 19.2.5.6, Herwigpp",
                    "AtlasProduction, 19.2.5.7, Herwigpp",
                    "AtlasProduction, 19.2.5.8, Herwigpp",
                    "AtlasProduction, 19.2.5.9, Herwigpp",
                    "AtlasProduction, 19.2.5.10, Herwigpp",
                    "AtlasProduction, 19.2.5.11, Herwigpp",
                    "AtlasProduction, 19.2.5.12, Herwigpp",
                    "AtlasProduction, 19.2.5.13, Herwigpp",
                    "AtlasProduction, 19.2.5.14, Herwigpp",
                    "MCProd,          19.2.5.14.3, MadGraph,aMC@NLO, issue of afs access in case of aMC@NLO and MG5 which uses lhapdf and if they compile their process on-the-fly",
                    "MCProd,          19.2.4.10.2, MadGraph",
                    "MCProd,          19.2.4.16.3, MadGraph",
                    "MCProd,          19.2.4.16.4, MadGraph",
                    "MCProd,          19.2.4.16.5, MadGraph",
                    "MCProd,          19.2.4.16.6, MadGraph",
                    "MCProd,          19.2.4.19.2, MadGraph",
                    "AtlasProduction, 19.2.4.19,   MadGraph",
                    "MCProd,          19.2.4.8.1,  MadGraph",
                    "MCProd,          19.2.4.9.1,  MadGraph",
                    "MCProd,          19.2.4.9.3,  MadGraph",
                    "MCProd,          19.2.5.1.1,  MadGraph",
                    "MCProd,          19.2.5.1.2,  MadGraph",
                    "AtlasProduction, 19.2.5.10,   MadGraph",
                    "MCProd,          19.2.5.11.1, MadGraph",
                    "MCProd,          19.2.5.11.2, MadGraph",
                    "AtlasProduction, 19.2.5.11,   MadGraph",
                    "MCProd,          19.2.5.12.1, MadGraph",
                    "MCProd,          19.2.5.12.3, MadGraph",
                    "AtlasProduction, 19.2.5.12,   MadGraph",
                    "AtlasProduction, 19.2.5.1,    MadGraph",
                    "AtlasProduction, 19.2.5.2,    MadGraph",
                    "MCProd,          19.2.5.3.1,  MadGraph",
                    "MCProd,          19.2.5.3.2,  MadGraph",
                    "MCProd,          19.2.5.3.3,  MadGraph",
                    "AtlasProduction, 19.2.5.3,    MadGraph",
                    "AtlasProduction, 19.2.5.4,    MadGraph",
                    "AtlasProduction, 19.2.5.5,    MadGraph",
                    "AtlasProduction, 19.2.5.6,    MadGraph",
                    "MCProd,          19.2.5.7.1,  MadGraph",
                    "AtlasProduction, 19.2.5.7,    MadGraph",
                    "MCProd,          19.2.5.8.1,  MadGraph",
                    "AtlasProduction, 19.2.5.8,    MadGraph",
                    "MCProd,          19.2.5.9.1,  MadGraph",
                    "MCProd,          19.2.5.9.2,  MadGraph",
                    "AtlasProduction, 19.2.5.9,    MadGraph",
                    "MCProd,          19.2.5.16.1, Sherpa,    only for multicore running tests ",
                    "MCProd,          20.7.9.1.1,     , only for Tauolapp validation - AGENE1324",
                    "MCProd,          19.2.5.20.3, Sherpa,    only for multicore running tests",
                    "MCProd,          19.2.5.31.1, Herwig7,   the recompiletion of ThePEG forgotten",
                    "MCProd,          20.7.9.9.8,  Herwig7, problem in the transform that does not allow for uncompresed inputs",
                    "MCProd,          20.7.9.9.8,  Powheg, problem in the transform that does not allow for uncompresed inputs",
                    "AtlasProduction, 19.2.5.33,  , ATLMCPROD-5791 ATLASJT-375",
                    "MCProd,          19.2.5.31.1, , ATLMCPROD-5791 ATLASJT-375",
                    "MCProd,          19.2.5.32.1, , ATLMCPROD-5791 ATLASJT-375",
                    "MCProd,          19.2.5.32.2, , ATLMCPROD-5791 ATLASJT-375",
                    "MCProd,          19.2.5.33.1, , ATLMCPROD-5791 ATLASJT-375",
                    "MCProd,          19.2.5.33.2, , ATLMCPROD-5791 ATLASJT-375",
                    "MCProd,          20.7.9.9.22, MadGraph, MadPath not set correctly",
                    "AtlasProduction, 19.2.5.37, MadSpin",
                    "AthGeneration,   21.6.0,  , Duplication in event numbering",
                    "AthGeneration,   21.6.1,  , Duplication in event numbering",
                    "AthGeneration,   21.6.2,  , Duplication in event numbering",
                    "AthGeneration,   21.6.3,  , Duplication in event numbering",
                    "AthGeneration,   21.6.4,  , Duplication in event numbering",
                    "AthGeneration,   21.6.5,  , Duplication in event numbering",
                    "AthGeneration,   21.6.6,  , Duplication in event numbering",
                    "AthGeneration,   21.6.7,  , Duplication in event numbering",
                    "AthGeneration,   21.6.8,  , Duplication in event numbering",
                    "AthGeneration,   21.6.9,  , Duplication in event numbering",
                    "AthGeneration,   21.6.10,  , Duplication in event numbering",
                    "AthGeneration,   21.6.11,  , Duplication in event numbering",
                    "AthGeneration,   21.6.12,  , Duplication in event numbering",
                    "AthGeneration,   21.6.13,  , Duplication in event numbering",
                    "AthGeneration,   21.6.14,  , Duplication in event numbering",
                    "AthGeneration,   21.6.15,  , Duplication in event numbering",
                    "AthGeneration,   21.6.16, Herwig7 , problematic fix for matchbox+herwig7 muons that travel large distances",
                    "AthGeneration,   21.6.25,  , transform crashes when running with inputGeneratorFile parameter",
                    "AthGeneration,   21.6.27, Sherpa, buggy version",
                    "AthGeneration,   21.6.28, Sherpa, buggy version"]

    isError=None
    for line in tmpblackfile:
        if not line.strip():
            continue

        bad = "".join(line.split()).split(",")
        
        badgens=[bad[2]] 
        if bad[2]=="Pythia8":
            badgens.append("Py8")
        if bad[2]=="Pythia":
            badgens.append("Py")
        if bad[2]=="MadGraph":
            badgens.append("MG")
        if bad[2]=="Powheg":
            badgens.append("Ph")
        if bad[2]=="Herwigpp":
            badgens.append("Hpp")
        if bad[2]=="Herwig7":
            badgens.append("H7")
        if bad[2]=="Sherpa":
            badgens.append("Sh")
        if bad[2]=="Alpgen":
            badgens.append("Ag")
        if bad[2]=="EvtGen":
            badgens.append("EG")
        if bad[2]=="ParticleGun":
            badgens.append("PG")

        if any( bad[0] in topJO.split('-')[0] for branch in bad[0] ):
            cacheMatch=True               
            for i,c in enumerate(release.split('.')):
                print(bad[1].split('.')[i])
                if not c == bad[1].split('.')[i]:
                    cacheMatch=False
                    break
            if cacheMatch:            
                logerr("", "Combination %s_%s for %s it is blacklisted"%(bad[0],bad[1],bad[2]))
                isError = "%s_%s is blacklisted for %s with reason: %s"%(bad[0],bad[1],bad[2],bad[3])
                return isError
    return isError

# find identifiers and variables in log file lines
def checkLine(line, lineIdentifier, dict, splitby ):
    if lineIdentifier in line:
        for param in dict:
            # print (param)
            if param in line:
                if len(line.split(splitby))==0:
                    raise RuntimeError("Found bad entry %s"%line)
                else:
                    thing="".join(line.split(lineIdentifier)[1].split(splitby)[1:]).strip()     
                    dict[param].append(thing)
                break
		    
class bcolors:
    if not opts.NO_COLOUR:
        HEADER = '\033[95m'
        OKBLUE = '\033[94m'
        OKGREEN = '\033[92m'
        WARNING = '\033[93m'
        FAIL = '\033[91m'
        ENDC = '\033[0m'
    else:
        HEADER = ''
        OKBLUE = ''
        OKGREEN = ''
        WARNING = ''
        FAIL = ''
        ENDC = ''

    def disable(self):
        self.HEADER = ''
        self.OKBLUE = ''
        self.OKGREEN = ''
        self.WARNING = ''
        self.FAIL = ''
        self.ENDC = ''

class LogCounts:
    Errors = 0
    Warnings = 0

def loginfo(out1,out2):
    print (str(out1),bcolors.OKBLUE + str(out2) + bcolors.ENDC)
def loggood(out1,out2):
    print (str(out1),bcolors.OKGREEN + str(out2) + bcolors.ENDC)
def logerr(out1,out2):
    print (str(out1),bcolors.FAIL + str(out2) + bcolors.ENDC)
    LogCounts.Errors += 1
def logwarn(out1,out2):
    print (str(out1),bcolors.WARNING + str(out2) + bcolors.ENDC)
    LogCounts.Warnings += 1

def pad(seq, target_length, padding=None):
    length = len(seq)
    if length > target_length:
        return seq
    seq.extend([padding] * (target_length - length))
    return seq

if __name__ == "__main__":
    main()
