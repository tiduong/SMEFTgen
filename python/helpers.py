import glob, re, sys, os, json, yaml
import itertools
import pprint

# prodmodes=["GGF","VBF","ZH","WH"]#,"TTH","THJB","THWB"]
# prodmodes=["GGF","VBF"]#,"VBF","ZH","WH"]#,"TTH","THJB","THWB"]
def set_run_settings(inputs, setting_file_name):
    if setting_file_name:
        data = load_json(setting_file_name)
        if "settings" in data.keys():
            inputs["settings"] = data["settings"]
        else:
            inputs["settings"] = data
    else:
        if inputs["run type"] == "production":
            inputs["settings"] = {
                "lhe_version": "'3.0'",
                "cut_decays": "'T'",
                "pdlabel": "'lhapdf'",  # higgs production
                # 'pdlabel':"\'nn23lo1\'", # higgs decay
                "lhaid": "'90400'",
                # 'ickkw'       : 0,      # not used in default https://gitlab.cern.ch/atlas_higgs_combination/projects/eft2022/-/blob/master/parameterisation/cards/run_card_Higgs.dat
                "drll": 0.05,
                "drjj": 0.05,
                "drbb": 0.05,
                "drbj": 0.05,
                "ptb": 20.0,
                "ptj": 20.0,
                "ptl": 0.01,
                "etal": 10,
                # 'maxjetflavor': 5,
                # 'asrwgtflavor': 5,
                # 'ktdurham'    : 30,    # not used in default
                # "dparameter"  : 0.4,   # not used in default
                # "xqcut"       : 0.0,   # not used in default
                "pdfwgt": '"True"',
                "use_syst": '"False"',
                "nevents": int(1e3),
            }



        if inputs["run type"] == "decay":
            inputs["settings"] = {
                "ktdurham": 0.0,
                "lhe_version": "'3.0'",
                "ptj": 0.0,
                "drbj": 0.01,
                "pdlabel": "'nn23lo1'",
                "drll": 0.01,
                "dparameter": 0.0,
                "ptb": 0.0,
                "drbb": 0.01,
                "pdfwgt": "'T'",
                "nevents": 5000,
                "ptl": 0.0,
                "ickkw": 0,
                "xqcut": 0.0,
                "use_syst": '"False"',
                "drjl": 0.01,
                "draa": 0.01,
                "cut_decays": '"F"',
                "drjj": 0.01,
            }


def write_wc_restrict(inputs):
    """Write restrict card
    for direct generation : set parameter to 1.0,
    for reweighting : set parameter to 0.xxx"""
    import yaml
    
    re_wilson = re.compile("[ ](c.*)[ ]")
    coeffs = inputs["par"]
    coef_val = inputs["coefficient-value"]
    modelfile = inputs["model"].split("model ")[-1].split("-")[0] 
    if inputs["propagtor_corrections"]:
        prop_tag = "_prop"
    else:
        
        prop_tag = ""

    defaultfile = modelfile + "/restrict_{0}{1}.dat".format(
        inputs["mass_tag"], prop_tag
    )
    
    if inputs["generation mode"]  == "reweight" : 
        inputs["restrict tag"] = "_reweight{0}{1}".format(prop_tag,inputs["run tag"])
        
    elif inputs["generation mode"] == "standalone" :        
        inputs["restrict tag"] = "{0}".format(prop_tag)

    counter = 1
    if "coordinate" in inputs.keys():
        coords = inputs["coordinate"]
    else:
        coords = {par :1.0 for par in inputs["par"]}
    #print(coords)
    for k,v in coords.items():
        inputs["restrict tag"] = inputs["restrict tag"] + "_".join([k,str(v)]).replace(".","p").replace("-","m") + "_"    
        inputs["restrict tag"] = inputs["restrict tag"].replace("_","").replace("1p0","")
    if inputs["restrict tag"][-1] == "_":
        inputs["restrict tag"] = inputs["restrict tag"][:-1]
    destfile = modelfile + "/restrict_{0}.dat".format(inputs["restrict tag"].replace("_","").replace("1p0",""))
    with open(defaultfile, "rt") as fp:
        tempfile = open(destfile, "wt")
        for line in fp:
            m_val = re_wilson.search(line)
            if m_val and str(m_val.group(1)) in coords.keys():
                if inputs["generation mode"] == "reweight":
                    tempfile.write(line.replace("0.", "0." + str(counter)))
                    counter = counter + 1
                else:
                    val = str(coords[str(m_val.group(1))])
                    if "." in val:
                        tempfile.write(line.replace("0.", val))
                    else:
                        tempfile.write(line.replace("0.", val + "."))
            else:
                tempfile.write(line)

        print("Wrote restrict card {0}".format(destfile))


def prepare_coupling_order(inputs):
    import os

    re_wilson = re.compile("[ ](c.*)[ ]")
    coeffs = inputs["par"]
    modelfile = inputs["model"].split("model ")[-1].split("-")[0]
    #print(modelfile) debug
    coupling_orders_file = "{0}/coupling_orders.py".format(modelfile)

    if inputs["propagtor_corrections"]:
        prop_corr_coupling_orders_file = "{0}/coupling_orders_prop.py".format(modelfile)
        runline = "cp {0} {1}".format(
            prop_corr_coupling_orders_file, coupling_orders_file
        )
        #print("Running {0}".format(runline)) debug
        os.system(runline)
    else:
        no_prop_corr_coupling_orders_file = "{0}/coupling_orders_no_prop.py".format(
            modelfile
        )
        runline = "cp {0} {1}".format(
            no_prop_corr_coupling_orders_file, coupling_orders_file
        )
        #print("Running {0}".format(runline)) debug
        os.system(runline)


def load_json(file0):
    with open(file0, "r") as f:
        dict0 = json.load(f)
    return dict0


def mkdir(dir0):
    if os.path.isdir(dir0):
        return
    else:
        os.makedirs(dir0)


def is_xsec_zero(val):
    return abs(val["xsec"]["val"] / val["xsec"]["err"]) <= 2.0


# https://gitlab.cern.ch/atlas_higgs_combination/projects/eft2022/-/blob/master/parameterisation/mc_HiggsProd_MGPy8.py#L78-132
# SMHLOOP not used (more info https://arxiv.org/pdf/2012.11343.pdf Chapter 5)
def get_genlines(inputs, case, npsqtag, npprop="", custom_process=""):
    
    process_line = ""
    if len(custom_process) > 0:
        # set the process as the custom process
        process_line = custom_process

    else:
        # Check if case is in the list of supported cases and then set that as the process
        lines = {
#        "GGF": "generate p p > h QED=1  {NPprop} {NPsqtag}\nadd process p p > h j QED=1 {NPprop} {NPsqtag}\nadd process p p > h j j QED=1 {NPprop} {NPsqtag}\nadd process p p > 
        # h b b~ QED=1 {NPprop} {NPsqtag}\n",
            "GGF_SMEFTatNLO": "generate p p > h QED=1 [QCD] {NPsqtag}\n",
            "GGF_gamma": "generate p p > h QED=1 NP=0, h > l+ l- l+ l- SMHLOOP=1 NP=1\n",
            "GGF_nogamma": "generate p p > h QED=1 NP=0, h > l+ l- l+ l- / a SMHLOOP=0 NP=1\n",
#        "GGF": "generate p p > h > l+ l- l+ l- {NPsqtag} SMHLOOP=0 \n",
            "VBF": "generate p p > h j j QCD=0 {NPprop} {NPsqtag}\n",  # checked
            "ZH": "define l+ = l+ ta+ \ndefine l- = l- ta- \ngenerate p p > h l+ l- {NPprop} {NPsqtag}\nadd process p p > h vl vl~ {NPprop} {NPsqtag}\n",  # checked #updated
            "WH": "define l+ = l+ ta+ \ndefine l- = l- ta- \ngenerate p p > h l+ vl {NPprop} {NPsqtag}\nadd process p p > h l- vl~ {NPprop} {NPsqtag}\n",  # checked
            "TTH": "generate p p > h t t~ {NPprop} {NPsqtag}\n",  # checked
            "THJB": "generate p p > h t b~ j {NPprop} {NPsqtag}\nadd process p p > h t~ b j {NPprop} {NPsqtag}\n",  # checked
            "THWB": "define p = p b b~ \ngenerate p p > h t w- {NPprop} {NPsqtag}\nadd process p p > h t~ w+ {NPprop} {NPsqtag}\n",  # verify p = p b b~ not used in default
            "h_ww_lvlv": "generate h > e+ vl mu- vl~ {NPprop} {NPsqtag} @0\n add process h > e- vl mu+ vl~ {NPprop} {NPsqtag} @1",
            "h_eevv": "generate h > e+ vl e- vl~ {NPprop} {NPsqtag} @0 ",
            "h_mmvv": "generate h > mu+ vl mu- vl~ {NPprop} {NPsqtag} @0 ",
            "h_tauvtauv": "generate h > ta+ vt ta- vt~ {NPprop} {NPsqtag} @0 ",
            "h_lvtauv": "generate h > l+ vl ta- vt~  {NPprop} {NPsqtag} @0 \n add process h > ta+ vt l- vl~ {NPprop} {NPsqtag} @1 ",
            "h_lvjj": "generate h > l+ vl j j {NPprop} {NPsqtag} @0 \n add process h > j j l- vl~ {NPprop} {NPsqtag} @1 ",
            "h_tauvjj": "generate h > ta+ vt j j {NPprop} {NPsqtag} @0 \n add process h > j j ta- vt~ {NPprop} {NPsqtag} @1 ",
            "h_qqqq": "define q = u c d s u~ c~ d~ s~ \n generate h > q q q q   {NPprop} {NPsqtag} @0",
            "h_4l": "generate h > l+ l- l+ l- {NPprop} {NPsqtag} @0 ",
            "h_4ta": "generate h > ta+ ta- ta+ ta-  {NPprop} {NPsqtag} @0 ",
            "h_lla": "generate h > a l- l+ {NPprop} {NPsqtag} @0 ",
            "h_4nu": "generate h > vl vl~ vl vl~ {NPprop} {NPsqtag} @0 ",
            "h_lltautau": "generate h > l+ l- ta+ ta-   {NPprop} {NPsqtag} @0 ",
            "h_vvtautau": "generate h > vl vl~ ta+ ta- {NPprop} {NPsqtag} @0 ",
            "h_lljj": "generate h > j j l+ l-   {NPprop} {NPsqtag} @0",
            "h_jjtautau": "generate h > j j ta+ ta-   {NPprop} {NPsqtag} @0",
            "h_vvjj": "generate h > j j vl vl~   {NPprop} {NPsqtag} @0",
            "h_aa": "generate h > a a {NPprop} {NPsqtag} @0",
            "h_bb": "generate h > b b~ {NPprop} {NPsqtag} @0",
            "h_cc": "generate h > c c~ {NPprop} {NPsqtag} @0",
            "h_ss": "generate h > s s~ {NPprop} {NPsqtag} @0",
            "h_ee": "generate h > e+ e- {NPprop} {NPsqtag} @0",
            "h_mumu": "generate h > mu+ mu- {NPprop} {NPsqtag} @0",
            "h_tautau": "generate h > ta+ ta- {NPprop} {NPsqtag} @0",
            "h_za": "generate h > z a {NPprop} {NPsqtag} @0",
            "h_gg": "generate h > g g [noborn=QCD] {NPprop} {NPsqtag} @0",
            "ggh_4l": "generate p p > h > l+ l- l+ l-  QCD=2 QED=4 [QCD] {NPsqtag} ",
            "ggh_4l_NLO": "generate p p > h > l+ l- l+ l-  QCD=3 QED=4 [QCD] {NPsqtag} ",
            "ggh": "generate p p > h QCD=2 QED=2 [QCD] {NPsqtag} ",
        }
        process_line = lines[case]    
        
    
    if npprop:
        return process_line.replace("{NPsqtag}", "").replace("{NPprop}","SMHLOOP={0} NP=0 NPprop<=2 NPprop^2==2".format(inputs["SMHLOOP"]))
    else:
        return process_line.replace("{NPsqtag}", npsqtag).replace("{NPprop}", " ")


def get_model(inputs,modelpath, sym, input_scheme):

    # MFV, U35, general, topU3l, top
    # Mw, alpha
    # define j = p (j does not include b, only light flavours)in https://gitlab.cern.ch/atlas_higgs_combination/projects/eft2022/-/blob/master/parameterisation/mc_HiggsProd_MGPy8.py#L140
    if inputs["gen-model"] == "SMEFTsim":
        return "#set gauge unitary\nimport model {0}/SMEFTsim_v3_0_2/UFO_models/SMEFTsim_{1}_{2}Scheme_UFO-{{param}} \nset group_subprocesses True\ndefine p = g u c d s b u~ c~ d~ s~ b~\n".format(
       modelpath, sym, input_scheme
        )
    elif inputs["gen-model"] == "SMEFTatNLO":
        return "#set gauge unitary\nimport model {0}/SMEFTatNLO-{{param}} \nset group_subprocesses True\n".format(
       modelpath, sym, input_scheme
    )
    else:
        print("Model not supported")
        sys.exit(1)

def get_coeff_names(model,sym):
    """Returns a list of the names of the coefficients in the model. 
    Choosing the ones that appear in the model folder.
    Parameters beginning with c are picked excluding cth which corresponds to cos(theta). 
    Note for SMEFTsim the flavour symmetry is also taken into account
    """
    import os,sys
    path = os.environ.get("PACKAGEPATH")
    if model == "SMEFTatNLO":
        modelpath = sys.path.append(os.path.join(path, "models", "SMEFTatNLO"))
    elif model == "SMEFTsim":
        smeftsim_flavours = ["U35","MFV","topU3l","top","general"]
        if sym in smeftsim_flavours:
            print(os.path.join(path, "models", "SMEFTsim_v3_0_2", "UFO_models", "SMEFTsim_"+sym+"_MwScheme_UFO"))
            modelpath = sys.path.append(os.path.join(path, "models", "SMEFTsim_v3_0_2", "UFO_models", "SMEFTsim_"+sym+"_MwScheme_UFO"))
        else:
            raise ValueError("Flavour symmetry {0} not supported for SMEFTsim,\n please choose from {1}.\n".format(sym,",".join(smeftsim_flavours)))
    
    from parameters import all_parameters

    eftpars = []
    for param in all_parameters:
        if param.name[0]=='c' and param.name != 'cth':
            if param.type == 'real':
                eftpars.append(param.name)
            elif param.type == 'complex':
                eftpars = eftpars + param.value.replace("*complex(0,1)","").split(" + ")
    # remove cth
    # add SM
    eftpars.append("SM")
    
    return eftpars


def find_lha_code(parname, all_parameters):
    for par in all_parameters:
        if par.name == parname:
            return par.lhacode[0]


def find_lhablock(parname, all_parameters):
    for par in all_parameters:
        if par.name == parname:
            return par.lhablock


def get_lhablock_id(parname, sym):
    if sym == "U35":
        from SMEFTsim_v3_0_2.UFO_models.SMEFTsim_U35_MwScheme_UFO.parameters import (
            all_parameters,
        )

        return find_lha_code(parname, all_parameters)

    elif sym == "topU3l":
        from SMEFTsim_v3_0_2.UFO_models.SMEFTsim_U35_MwScheme_UFO.parameters import (
            all_parameters,
        )

        return find_lha_code(parname, all_parameters)
    else:
        return


def get_lhablock(parname, sym):
    if sym == "U35":
        from SMEFTsim_v3_0_2.UFO_models.SMEFTsim_U35_MwScheme_UFO.parameters import (
            all_parameters,
        )

        return find_lhablock(parname, all_parameters)

    elif sym == "topU3l":
        from SMEFTsim_v3_0_2.UFO_models.SMEFTsim_U35_MwScheme_UFO.parameters import (
            all_parameters,
        )

        return find_lhablock(parname, all_parameters)

    else:
        return


def insert_spacer(char):
    return "".join(["\n"] + ["#"] + [char for i in range(82)] + ["\n"])


def write_process(f, inputs):
    str0 = 'process = """{0}{1}\noutput -f"""\n'.format(inputs["model"], inputs["process"])
    str0 = str0.replace("{param}", inputs["restrict tag"])
    f.write(str0)


def write_dict(f, inputs, dictname):
    f.write("{0} = {{\n".format(dictname))
    for key, val in inputs[dictname].items():
        f.write("      '{0}':{1},\n".format(key, val))
    f.write("}\n\n")


def write_generate(f):
    
    f.write(
        "generate(process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode)\n"
    )
    f.write(
        "arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)\n"
    )
    #f.write("os.environ[\"ATHENA_PROC_NUMBER\"] = \"8\"\n")

def write_metadata(f,inputs):
    f.write(insert_spacer("-"))
    f.write("# Metadata\n")   
    if "metadata" in inputs.keys():
        if "description" in inputs["metadata"].keys():
            f.write("evgenConfig.description = {0}\n".format(inputs["metadata"]["description"]))
        if "keywords" in inputs["metadata"].keys():
            f.write("evgenConfig.keywords = {0}\n".format(inputs["metadata"]["keywords"]))
        if "contact" in inputs["metadata"].keys():
            f.write("evgenConfig.contact = {0}\n".format(inputs["metadata"]["contact"]))
        if "generators" in inputs["metadata"].keys():
            f.write("evgenConfig.generators = {0}\n".format(inputs["metadata"]["generators"]))
    
    else:
        f.write("evgenConfig.description = 'MadGraphSMEFT'\n")
        f.write("evgenConfig.keywords = [ 'Higgs', 'mH125']\n")
        f.write("evgenConfig.contact = ['xyz@cern.ch']\n")
        f.write("evgenConfig.generators = ['MadGraph']\n")

def write_pythia(f,inputs):
    f.write(insert_spacer("-"))
    f.write("# Shower\n")
    f.write("from MadGraphControl.MadGraphUtils import *\n")
    if inputs["run type"] == "production":
        f.write('include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")\n')
        f.write('include("Pythia8_i/Pythia8_MadGraph.py")\n')
        f.write("#genSeq.Pythia8.Commands += [ '25:onMode = off'] # decay of Higgs \n")
    if "pythia8" in inputs.keys():
        f.write('include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")\n')
        f.write('include("Pythia8_i/Pythia8_MadGraph.py")\n')
        f.write(inputs["pythia8"])

def get_higgs_prodmod(prodmod):
    if prodmod == "GGF":
        return "GGF"
    elif prodmod == "VBF":
        return "VBF"
    elif prodmod == "ZH":
        return "QQ2ZH"
    elif prodmod == "WH":
        return "WH"
    elif prodmod == "TTH":
        return "TTH"
    elif prodmod == "THJB":
        return "TH"
    elif prodmod == "THWB":
        return "TH"
    else:
        #print("Did not find production mode {0}".format(prodmod))
        #sys.exit()
        return "NONE"

def write_initial(f):
    f.write("import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment\n")
    f.write("from MadGraphControl.MadGraphUtils import *\n")
    f.write("import subprocess,os\n")
    f.write("#from os.path import join as pathjoin \n")
    f.write("fcard = open('proc_card_mg5.dat','w')\n")
    f.write("safefactor=2.0\n")
    f.write("nevents=runArgs.maxEvents*safefactor\n")
    f.write("runName='run_01'# General settings \n")
    f.write(
        "nevents = runArgs.maxEvents*safefactor if runArgs.maxEvents>0 else safefactor*evgenConfig.nEventsPerJob\n"
    )
    f.write("gridpack_mode=False\n")


#   f.write("os.environ[\"ATHENA_PROC_NUMBER\"] = \"4\" \n")

def prepare_reweight_process(reweight_str):
    reweight_str = reweight_str.replace("generate", "change process")
    
    if "add process" in reweight_str:
        # split by new line and append --add flag
        split_process = reweight_str.split("\n")
        reweight_str = ""
        for split_line in split_process:
            if "add process" in split_line:
                split_line_new = split_line.replace("add process", "change process") + " --add\n"
                reweight_str = reweight_str + split_line_new
            else: 
                reweight_str = reweight_str + split_line + "\n"

    return reweight_str

def get_reweight_parvals_pair(par1,par2,val,inputs):
    val_str = ""
    for par_set in inputs["par"]:
        if par_set == "SM": val_str = val_str + "".format(par_set) 
        else:
            if par1 == par_set: val_str = val_str +  "set {0} {1}\n".format(par_set, val)
            elif par2 == par_set: val_str = val_str +  "set {0} {1}\n".format(par_set, val)
            else: val_str = val_str + "set {0} 1e-9\n".format(par_set)
    return val_str


def get_reweight_parvals_single(par,val,inputs):
    val_str = ""
    for par_set in inputs["par"]:
        if par_set == "SM": val_str = val_str + "".format(par_set) 
        else:
            if par == par_set:val_str = val_str +  "set {0} {1}\n".format(par_set, val)
            else: val_str = val_str + "set {0} 0\n".format(par_set)
    return val_str

def get_reweight_launch(parlist, val):
    sign = "p"
    if val < 0.0: sign = "m"
    valabs = str(abs(val)).replace(".", "p")
    launch = "launch --rwgt_name={0}_{1}_{2}\n".format("_".join(parlist), sign, valabs)
    return launch

def get_reweight_string(inputs):
    val = 1.0
    
    process = prepare_reweight_process(inputs["reweight process"])
    reweight_str = ""
    val_str = get_reweight_parvals_single(["SM"],val,inputs)
    launch_str = get_reweight_launch(["SM"],val)
    reweight_str = "{0}\n".format(process)


    if inputs["interference"] or inputs["square"]:
        for par in inputs["par"]:
            launch_str = get_reweight_launch([par],val)
            reweight_str = "{0}\n{1}".format(reweight_str,launch_str)
            val_str = get_reweight_parvals_single(par,val,inputs)
            reweight_str = reweight_str + val_str
            
    elif inputs["cross"]:
        par1 = inputs["par"][0]
        for par2 in inputs["par"][1:]:
            launch_str = get_reweight_launch([par1,par2],val)
            reweight_str = "{0}\n{1}".format(reweight_str,launch_str)
            val_str = get_reweight_parvals_pair(par1,par2,val,inputs)
            reweight_str = reweight_str + val_str

    #reweight_str = reweight_str + "done\n"
    return reweight_str


def write_reweight(f, inputs):
    f.write(
        "#==================================================================================\n"
    )
    f.write("#Building param_card setting to 0 c_is that are not of interest\n")
    #   write_dict(f,inputs,"smeft")
    #   write_dict(f,inputs,"yukawa")
    #   write_dict(f,inputs,"params")
    f.write(
        "#==================================================================================\n"
    )
    f.write("rcard = open('reweight_card.dat','w')\n")
    f.write('reweightCommand="""{0}"""\n'.format(get_reweight_string(inputs)))
    f.write("rcard.write(reweightCommand)\n")
    f.write("rcard.close()\n")
    f.write(
        "subprocess.call('cp reweight_card.dat ' + process_dir+'/Cards/', shell=True)\n"
    )
    
def write_close(f, inputs):
    f.write("\n")
    if inputs["run type"] == "decay":
        f.write("theApp.finalize()\n")
        f.write("theApp.exit()\n")


def writeJO(f, inputs):
    write_initial(f)
    write_wc_restrict(inputs)
    prepare_coupling_order(inputs)
    f.write(insert_spacer("-"))
    write_process(f, inputs)
    f.write("process_dir = new_process(process)\n")
    ####### Modify mb Start
    #    f.write(insert_spacer("-"))
    #    f.write("masses = {}\nmasses['5'] = 0.\n")
    #    f.write("modify_param_card(process_dir=process_dir,params={'MASS':masses})")
    ####### Modify mb End
    f.write(insert_spacer("-"))
    write_dict(f, inputs, "settings")

    f.write(
        "modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)"
    )
    f.write(insert_spacer("-"))
    # Building param_card setting to 0 c_is that are not of interest
    # f.write("params={}\n")
    if inputs["generation mode"] == "reweight":
        write_reweight(f, inputs)
    write_generate(f)
    write_metadata(f,inputs)
    write_pythia(f,inputs)
    write_close(f, inputs)


def grepy(file, expr):
    file = open(file, "r")
    for line in file:
        if re.search(expr, line):
            return line

    return None


def get_xsec_val(str0):
    vals = (
        str0.split("Cross-section :")[-1]
        .replace("pb", "")
        .replace("\n", "")
        .split("+-")
    )
    return float(vals[0]), float(vals[1])


def get_xsecs(
    datafolder, model, sym, prodmodes, non_zero_only=False, savefilepath="", verbose=False
):
    xsec = {}
    for prodmod in prodmodes:
        xsec[prodmod] = {}
        for par in get_coeff_names(model, sym):
            logfile_path = "{0}/{1}/{2}/{3}/test/log.generate".format(
                datafolder, sym, prodmod, par
            )
            if os.path.isfile(logfile_path):
                match = grepy(logfile_path, "Cross-section")
                if match != None:
                    if verbose:
                        print("match found : xsec for par {1} in {2}".format(match, par, prodmod))
                    xsec_val, xsec_err = get_xsec_val(match)
                    val = {"xsec": {"val": xsec_val, "err": xsec_err, "unit": "pb"}}
                    if is_xsec_zero(val) and non_zero_only:
                        continue
                    else:
                        xsec[prodmod][par] = {
                            "xsec": {"val": xsec_val, "err": xsec_err, "unit": "pb"}
                        }

                else:
                    match_zero_xsec = grepy(logfile_path, "NoDiagramException")
                    if match_zero_xsec:
                        if verbose:
                            print(
                                "match found : NoDiagramException for par {0} in {1}".format(
                                    par, prodmod
                                )
                            )
                        xsec_val, xsec_err = 0.0, 0.0
                        if not non_zero_only:
                            xsec[prodmod][par] = {
                                "xsec": {"val": xsec_val, "err": xsec_err, "unit": "pb"}
                            }
                    #                    print("no diagrams for {0}".format(par))
                    else:
                        if verbose:
                            print(
                                "check what happened for {1} {0}".format(par, prodmod)
                            )

    if len(savefilepath):
        with open(savefilepath, "w") as f:
            json.dump(xsec, f, indent=2)

    return xsec

def write_rivetmerge(f, inputs, mergerivetloc):
    paths = inputs["paths"]
    line = "source {0} {1} batch {2} && ".format(
        paths["setupRivet"], paths["out_dir"], paths["model"]
    )
    input = "{0}.*/test/rivetfile.yoda".format(paths["out_dir"])
    files = glob.glob(input)
    inputfiles = " ".join(files)
    if len(files) == 0:
        print("These files {0} do not exist !\n Exiting...".format(input))
    outfilename = paths["out_dir"].split("/")[-1]
    outputfile = "{0}/{1}.yoda".format(mergerivetloc, outfilename)
    rivetline = "yodamerge -o {0} {1}".format(outputfile, inputfiles)

    line = line + " mkdir -vp {0} && ".format(mergerivetloc)
    line = line + rivetline

    if inputs["writeSubmit"] == "bsub":
        tag = "rivet_merge" + paths["jo_file"].split("/")[-1]
        f.write('bsub -J {0} "{1} "\n'.format(tag, line))
    else:
        line = line + "\n"
        f.write(line)

def get_rivet_lines(inputs):
    paths = inputs["paths"]
    output = "rivetfile"
    input = "{0}/test/evnt.root".format(paths["out_dir"])
    inputs["output-rivetfile"] = "{0}/test/{1}.yoda".format(paths["out_dir"],output)

    line = ""
    higgsprodmode = get_higgs_prodmod(inputs["case"])
    prodmodline = "export HIGGSPRODMODE='{0}' && ".format(higgsprodmode)
    rivetfile = inputs["rivet-file"]
    rivetline = 'cp $RIVETJOPATH/{0}*.* ./ && rivet-buildplugin Rivet{0}.so {0}.cc && athena.py -c \'inFileName=\\"{1}\\"; histoFileName=\\"{2}\\"\' $RIVETJOPATH/RivetAnalysis.py ;'.format(rivetfile, input, output)
    line = line + prodmodline
    line = line + rivetline
    return line 

def get_gen_tf_lines(inputs):
    paths = inputs["paths"]
    line = ""
    line = line + "source {0} {1} batch {2} && ".format(paths["setupGeneration"], paths["out_dir"], paths["model"])
    line = line + "mkdir -vp {0} && ".format(paths["jo_folder"])
    line = line + "cp {0} {1}/ && ".format(paths["jo_file"], paths["jo_folder"])
    line = line + "Gen_tf.py --ecmEnergy=13000 --firstEvent=1 --randomSeed={0} --jobConfig={1} --outputEVNTFile=evnt.root --maxEvents={2} ; ".format(inputs["random_seed"], paths["jo_folder"], inputs["settings"]["nevents"])
    return line

def get_cleanup_line(inputs):
    cleanup_str_wo_evnt_root = " MGC_LHAPDF/ PROC_*/ env.txt histoFile.yoda jobReport.json lhapdf-config* mem.full.generate mem.summary.generate.json proc_card_mg5.dat py.py runargs.generate.py runwrapper.generate.sh tmp_LHE_events tmp_LHE_events.events  2014Inclusive_17.dec AtRndmGenSvc.out Bdecays0.dat Bs2Jpsiphi.DEC config.pickle DECAY.DEC eventLoopHeartBeat.txt events.lhe tmp_LHE_events.events G4particle_whitelist.txt inclusiveP8DsDPlus.pdt ntuple.pmon.gz ParticleData.local.orig.xml ParticleData.local.xml PDGTABLE.MeV pdt.table PoolFileCatalog.xml PoolFileCatalog.xml.BAK Settings_after.log Settings_before.log susyParticlePdgid.txt TestHepMC.root"
    if not inputs["write EVNT"]:
        return "rm -rf {0} {1} {2}".format(" evnt.root ", cleanup_str_wo_evnt_root, " ;")
    else:
        return "rm -rf {0} {1}".format(cleanup_str_wo_evnt_root, " ;")


def write_runline(f, inputs):
    paths = inputs["paths"]
    line = ""
    
    if not inputs["run-rivet"]: line = line + get_gen_tf_lines(inputs)
    else: line = line + get_gen_tf_lines(inputs) + get_rivet_lines(inputs)

    line = line + get_cleanup_line(inputs)
    line = line + " cd {0};".format(paths["current_dir"])

    if inputs["writeSubmit"] == "bsub":
        tag = paths["jo_file"].split("/")[-1]
        f.write('bsub -J {0} "{1} "\n'.format(tag, line))

    else:
        line = line + "\n"
        f.write(line)


def get_texname(sym, par):
    labels = {}
    labels["U35"] = {
        "SM": "\\text{SM}",
        "cdd": "c_{\\text{dd}}",
        "cuW": "c_{\\text{uW}}",
        "cdBRe": "\\text{cdBRe}",
        "clq3": "c_{\\text{lq}}^3",
        "clq1": "c_{\\text{lq}}^1",
        "cuG": "c_{\\text{uG}}",
        "cuB": "c_{\\text{uB}}",
        "cuH": "c_{\\text{uH}}",
        "cll": "c_{\\text{ll}}",
        "cquqd81Re": "\\text{cquqd81Re}",
        "cuu": "c_{\\text{uu}}",
        "cdG": "c_{\\text{dG}}",
        "cdB": "c_{\\text{dB}}",
        "cld": "c_{\\text{ld}}",
        "cle": "c_{\\text{le}}",
        "cdH": "c_{\\text{dH}}",
        "cledqRe": "\\text{cledqRe}",
        "cdW": "c_{\\text{dW}}",
        "cquqd81": "c_{\\text{quqd}}^{8 \\text{Prime}}",
        "clu": "c_{\\text{lu}}",
        "cqq11": "c_{\\text{qq}}^{\\text{Prime}}",
        "cee": "c_{\\text{ee}}",
        "ced": "c_{\\text{ed}}",
        "clequ3Im": "\\text{clequ3Im}",
        "ceu": "c_{\\text{eu}}",
        "cquqd11": "c_{\\text{quqd}}^{\\text{Prime}}",
        "ceB": "c_{\\text{eB}}",
        "cquqd1Re": "\\text{cquqd1Re}",
        "ceH": "c_{\\text{eH}}",
        "ceW": "c_{\\text{eW}}",
        "cuGIm": "\\text{cuGIm}",
        "cquqd8Re": "\\text{cquqd8Re}",
        "cdHRe": "\\text{cdHRe}",
        "cHWBtil": "c_{B H \\tilde{W}}",
        "clequ3Re": "\\text{clequ3Re}",
        "ceWRe": "\\text{ceWRe}",
        "cHGtil": "c_{H \\tilde{G}}",
        "cHq1": "c_{\\text{Hq}}^1",
        "cuu1": "c_{\\text{uu}}^{\\text{Prime}}",
        "ceWIm": "\\text{ceWIm}",
        "cHWtil": "c_{H \\tilde{W}}",
        "cHWB": "c_{\\text{HWB}}",
        "cuGRe": "\\text{cuGRe}",
        "cdWIm": "\\text{cdWIm}",
        "cud1": "c_{\\text{ud}}^1",
        "cdWRe": "\\text{cdWRe}",
        "cud8": "c_{\\text{ud}}^8",
        "cqd1": "c_{\\text{qd}}^1",
        "cqd8": "c_{\\text{qd}}^8",
        "cHbox": "c_{H \\square }",
        "ceBRe": "\\text{ceBRe}",
        "cqu1": "c_{\\text{qu}}^1",
        "cqu8": "c_{\\text{qu}}^8",
        "cuBRe": "\\text{cuBRe}",
        "cdGIm": "\\text{cdGIm}",
        "cHudIm": "\\text{cHudIm}",
        "cquqd8Im": "\\text{cquqd8Im}",
        "cuWRe": "\\text{cuWRe}",
        "cuHRe": "\\text{cuHRe}",
        "cdd1": "c_{\\text{dd}}^{\\text{Prime}}",
        "cuHIm": "\\text{cuHIm}",
        "cuBIm": "\\text{cuBIm}",
        "ceHIm": "\\text{ceHIm}",
        "cGtil": "c_{\\tilde{G}}",
        "cHl1": "c_{\\text{Hl}}^1",
        "cHl3": "c_{\\text{Hl}}^3",
        "cll1": "c_{\\text{ll}}^{\\text{Prime}}",
        "cH": "c_H",
        "cquqd8": "c_{\\text{quqd}}^8",
        "cG": "c_G",
        "cqe": "c_{\\text{qe}}",
        "cquqd11Re": "\\text{cquqd11Re}",
        "cHDD": "c_{\\text{HD}}",
        "cquqd1": "c_{\\text{quqd}}^1",
        "cW": "c_W",
        "cquqd1Im": "\\text{cquqd1Im}",
        "clequ1Im": "\\text{clequ1Im}",
        "clequ1Re": "\\text{clequ1Re}",
        "cquqd11Im": "\\text{cquqd11Im}",
        "cdBIm": "\\text{cdBIm}",
        "cledq": "c_{\\text{ledq}}",
        "cHud": "c_{\\text{Hud}}",
        "cdHIm": "\\text{cdHIm}",
        "cdGRe": "\\text{cdGRe}",
        "clequ3": "c_{\\text{lequ}}^3",
        "clequ1": "c_{\\text{lequ}}^1",
        "cWtil": "c_{\\tilde{W}}",
        "ceHRe": "\\text{ceHRe}",
        "cHBtil": "c_{H \\tilde{B}}",
        "cqq31": "c_{\\text{qq}}^{3 \\text{Prime}}",
        "cHq3": "c_{\\text{Hq}}^3",
        "cHudRe": "\\text{cHudRe}",
        "cHd": "c_{\\text{Hd}}",
        "cHe": "c_{\\text{He}}",
        "cHu": "c_{\\text{Hu}}",
        "ceBIm": "\\text{ceBIm}",
        "cqq3": "c_{\\text{qq}}^3",
        "cqq1": "c_{\\text{qq}}^1",
        "cHB": "c_{\\text{HB}}",
        "cHG": "c_{\\text{HG}}",
        "cuWIm": "\\text{cuWIm}",
        "cth": "c_{\\theta }",
        "cledqIm": "\\text{cledqIm}",
        "cHW": "c_{\\text{HW}}",
        "cquqd81Im": "\\text{cquqd81Im}",
    }
    labels["topU3l"] = {
        "SM": "\\text{SM}",
        "cbGRe": "\\text{cbGRe}",
        "cju1": "c_{\\text{ju}}^1",
        "ctj1": "c_{\\text{tj}}^1",
        "cju8": "c_{\\text{ju}}^8",
        "ctd1": "c_{\\text{td}}^1",
        "ctj8": "c_{\\text{tj}}^8",
        "cuG": "c_{\\text{uG}}",
        "ctHH": "c_{\\text{tH}}",
        "cHBtil": "c_{H \\tilde{B}}",
        "cQQ8": "c_{\\text{QQ}}^8",
        "cbHRe": "\\text{cbHRe}",
        "cutbd8Im": "\\text{cutbd8Im}",
        "cQQ1": "c_{\\text{QQ}}^1",
        "cuH": "c_{\\text{uH}}",
        "cll": "c_{\\text{ll}}",
        "cledj": "c_{\\text{ledj}}",
        "ctb8": "c_{\\text{tb}}^8",
        "cuW": "c_{\\text{uW}}",
        "cdB": "c_{\\text{dB}}",
        "cld": "c_{\\text{ld}}",
        "cle": "c_{\\text{le}}",
        "ctb1": "c_{\\text{tb}}^1",
        "cdH": "c_{\\text{dH}}",
        "cQtjd8Re": "\\text{cQtjd8Re}",
        "ctWIm": "\\text{ctWIm}",
        "cjQtu1": "c_{\\text{jQtu}}^1",
        "clu": "c_{\\text{lu}}",
        "cQtQb1Re": "\\text{cQtQb1Re}",
        "cQujb8": "c_{\\text{Qujb}}^8",
        "cee": "c_{\\text{ee}}",
        "ced": "c_{\\text{ed}}",
        "cjQtu8Re": "\\text{cjQtu8Re}",
        "cQujb1": "c_{\\text{Qujb}}^1",
        "cjujd1": "c_{\\text{jujd}}^1",
        "cQd1": "c_{\\text{Qd}}^1",
        "ceu": "c_{\\text{eu}}",
        "cth": "c_{\\theta }",
        "cQd8": "c_{\\text{Qd}}^8",
        "cjtQd8Re": "\\text{cjtQd8Re}",
        "cHQ3": "c_{\\text{HQ}}^3",
        "cHQ1": "c_{\\text{HQ}}^1",
        "ceB": "c_{\\text{eB}}",
        "cQt1": "c_{\\text{Qt}}^1",
        "ceH": "c_{\\text{eH}}",
        "cleQt1Im": "\\text{cleQt1Im}",
        "cjd1": "c_{\\text{jd}}^1",
        "cleju3Re": "\\text{cleju3Re}",
        "cjtQd1": "c_{\\text{jtQd}}^1",
        "cQujb8Re": "\\text{cQujb8Re}",
        "cQl1": "c_{\\text{Ql}}^1",
        "cjd8": "c_{\\text{jd}}^8",
        "cjtQd8": "c_{\\text{jtQd}}^8",
        "cdHRe": "\\text{cdHRe}",
        "cuB": "c_{\\text{uB}}",
        "clebQ": "c_{\\text{lebQ}}",
        "cutbd1Im": "\\text{cutbd1Im}",
        "cHWBtil": "c_{B H \\tilde{W}}",
        "ctGIm": "\\text{ctGIm}",
        "ctGRe": "\\text{ctGRe}",
        "cjujd8Im": "\\text{cjujd8Im}",
        "cHGtil": "c_{H \\tilde{G}}",
        "cjQtu8": "c_{\\text{jQtu}}^8",
        "cutbd8": "c_{\\text{utbd}}^8",
        "cledjIm": "\\text{cledjIm}",
        "cjtQd1Im": "\\text{cjtQd1Im}",
        "cuu1": "c_{\\text{uu}}^1",
        "ceWIm": "\\text{ceWIm}",
        "cHWtil": "c_{H \\tilde{W}}",
        "cHWB": "c_{\\text{HWB}}",
        "cHl3": "c_{\\text{Hl}}^3",
        "cuu8": "c_{\\text{uu}}^8",
        "cuGRe": "\\text{cuGRe}",
        "cdWIm": "\\text{cdWIm}",
        "cud1": "c_{\\text{ud}}^1",
        "cdWRe": "\\text{cdWRe}",
        "cQt8": "c_{\\text{Qt}}^8",
        "cuGIm": "\\text{cuGIm}",
        "cud8": "c_{\\text{ud}}^8",
        "cleju3": "c_{\\text{leju}}^3",
        "cbWRe": "\\text{cbWRe}",
        "cjuQb8Re": "\\text{cjuQb8Re}",
        "cHbox": "c_{H \\square }",
        "cbHIm": "\\text{cbHIm}",
        "ceBRe": "\\text{ceBRe}",
        "cjujd81Im": "\\text{cjujd81Im}",
        "ctW": "c_{\\text{tW}}",
        "cQujb1Re": "\\text{cQujb1Re}",
        "cuBRe": "\\text{cuBRe}",
        "cbBRe": "\\text{cbBRe}",
        "cdGIm": "\\text{cdGIm}",
        "ctWRe": "\\text{ctWRe}",
        "cHudIm": "\\text{cHudIm}",
        "ctt": "c_{\\text{tt}}",
        "cjtQd8Im": "\\text{cjtQd8Im}",
        "cQu1": "c_{\\text{Qu}}^1",
        "cQl3": "c_{\\text{Ql}}^3",
        "cleju1": "c_{\\text{leju}}^1",
        "cQu8": "c_{\\text{Qu}}^8",
        "cjQbd8": "c_{\\text{jQbd}}^8",
        "cuWRe": "\\text{cuWRe}",
        "cuHRe": "\\text{cuHRe}",
        "ctG": "c_{\\text{tG}}",
        "cjujd1Re": "\\text{cjujd1Re}",
        "cdd8": "c_{\\text{dd}}^8",
        "ctl": "c_{\\text{tl}}",
        "clj3": "c_{\\text{lj}}^3",
        "cQtQb1Im": "\\text{cQtQb1Im}",
        "cdd1": "c_{\\text{dd}}^1",
        "cjQbd8Re": "\\text{cjQbd8Re}",
        "cuHIm": "\\text{cuHIm}",
        "cbu8": "c_{\\text{bu}}^8",
        "ctB": "c_{\\text{tB}}",
        "cuBIm": "\\text{cuBIm}",
        "ctBIm": "\\text{ctBIm}",
        "ceHIm": "\\text{ceHIm}",
        "cGtil": "c_{\\tilde{G}}",
        "cutbd1": "c_{\\text{utbd}}^1",
        "ctu1": "c_{\\text{tu}}^1",
        "ceBIm": "\\text{ceBIm}",
        "cHl1": "c_{\\text{Hl}}^1",
        "cbGIm": "\\text{cbGIm}",
        "ctu8": "c_{\\text{tu}}^8",
        "cll1": "c_{\\text{ll}}^{\\text{Prime}}",
        "cjuQb1Re": "\\text{cjuQb1Re}",
        "cHtbIm": "\\text{cHtbIm}",
        "cQtQb8Re": "\\text{cQtQb8Re}",
        "cQj38": "c_{\\text{Qj}}^{38}",
        "cH": "c_H",
        "cjQbd8Im": "\\text{cjQbd8Im}",
        "ceW": "c_{\\text{eW}}",
        "cQj31": "c_{\\text{Qj}}^{31}",
        "cG": "c_G",
        "cQtQb1": "c_{\\text{QtQb}}^1",
        "cdW": "c_{\\text{dW}}",
        "cHDD": "c_{\\text{HD}}",
        "cjujd8Re": "\\text{cjujd8Re}",
        "cQtQb8": "c_{\\text{QtQb}}^8",
        "cW": "c_W",
        "cjQbd1Re": "\\text{cjQbd1Re}",
        "cleju3Im": "\\text{cleju3Im}",
        "ctHIm": "\\text{ctHIm}",
        "cjujd81Re": "\\text{cjujd81Re}",
        "cjujd11Re": "\\text{cjujd11Re}",
        "cQujb1Im": "\\text{cQujb1Im}",
        "cHB": "c_{\\text{HB}}",
        "cjujd11Im": "\\text{cjujd11Im}",
        "cjuQb1": "c_{\\text{juQb}}^1",
        "cbj1": "c_{\\text{bj}}^1",
        "cdBRe": "\\text{cdBRe}",
        "cledjRe": "\\text{cledjRe}",
        "cjuQb8": "c_{\\text{juQb}}^8",
        "cdBIm": "\\text{cdBIm}",
        "cleQt1": "c_{\\text{leQt}}^1",
        "cbj8": "c_{\\text{bj}}^8",
        "cleQt3": "c_{\\text{leQt}}^3",
        "cHud": "c_{\\text{Hud}}",
        "cdHIm": "\\text{cdHIm}",
        "cleQt3Im": "\\text{cleQt3Im}",
        "ctBRe": "\\text{ctBRe}",
        "cQtjd8Im": "\\text{cQtjd8Im}",
        "cQtQb8Im": "\\text{cQtQb8Im}",
        "cQtjd1Im": "\\text{cQtjd1Im}",
        "cjujd81": "c_{\\text{jujd}}^{8 \\text{Prime}}",
        "cjj38": "c_{\\text{jj}}^{38}",
        "clebQRe": "\\text{clebQRe}",
        "cdGRe": "\\text{cdGRe}",
        "cleju1Re": "\\text{cleju1Re}",
        "clebQIm": "\\text{clebQIm}",
        "cjj31": "c_{\\text{jj}}^{31}",
        "cbb": "c_{\\text{bb}}",
        "cleju1Im": "\\text{cleju1Im}",
        "cbe": "c_{\\text{be}}",
        "cbu1": "c_{\\text{bu}}^1",
        "ctHRe": "\\text{ctHRe}",
        "cbl": "c_{\\text{bl}}",
        "cQj11": "c_{\\text{Qj}}^{11}",
        "cHtbRe": "\\text{cHtbRe}",
        "cjujd11": "c_{\\text{jujd}}^{\\text{Prime}}",
        "cQj18": "c_{\\text{Qj}}^{18}",
        "cjuQb1Im": "\\text{cjuQb1Im}",
        "clj1": "c_{\\text{lj}}^1",
        "cWtil": "c_{\\tilde{W}}",
        "ceHRe": "\\text{ceHRe}",
        "ctd8": "c_{\\text{td}}^8",
        "cte": "c_{\\text{te}}",
        "cutbd1Re": "\\text{cutbd1Re}",
        "cbH": "c_{\\text{bH}}",
        "cHj3": "c_{\\text{Hj}}^3",
        "cHj1": "c_{\\text{Hj}}^1",
        "cje": "c_{\\text{je}}",
        "cbWIm": "\\text{cbWIm}",
        "cbW": "c_{\\text{bW}}",
        "cjQtu1Im": "\\text{cjQtu1Im}",
        "cjQbd1Im": "\\text{cjQbd1Im}",
        "cjj18": "c_{\\text{jj}}^{18}",
        "cHtb": "c_{\\text{Htb}}",
        "ceWRe": "\\text{ceWRe}",
        "cHudRe": "\\text{cHudRe}",
        "cHd": "c_{\\text{Hd}}",
        "cHe": "c_{\\text{He}}",
        "cjQtu1Re": "\\text{cjQtu1Re}",
        "cbBIm": "\\text{cbBIm}",
        "cjtQd1Re": "\\text{cjtQd1Re}",
        "cjQtu8Im": "\\text{cjQtu8Im}",
        "cQe": "c_{\\text{Qe}}",
        "cjujd8": "c_{\\text{jujd}}^8",
        "cHt": "c_{\\text{Ht}}",
        "cHu": "c_{\\text{Hu}}",
        "cleQt1Re": "\\text{cleQt1Re}",
        "cjuQb8Im": "\\text{cjuQb8Im}",
        "cdG": "c_{\\text{dG}}",
        "cjujd1Im": "\\text{cjujd1Im}",
        "cbG": "c_{\\text{bG}}",
        "cQb8": "c_{\\text{Qb}}^8",
        "cleQt3Re": "\\text{cleQt3Re}",
        "cQtjd8": "c_{\\text{Qtjd}}^8",
        "cHG": "c_{\\text{HG}}",
        "cuWIm": "\\text{cuWIm}",
        "cjj11": "c_{\\text{jj}}^{11}",
        "cQb1": "c_{\\text{Qb}}^1",
        "cQtjd1": "c_{\\text{Qtjd}}^1",
        "cbBB": "c_{\\text{bB}}",
        "cQujb8Im": "\\text{cQujb8Im}",
        "cbd8": "c_{\\text{bd}}^8",
        "cutbd8Re": "\\text{cutbd8Re}",
        "cHbq": "c_{\\text{Hb}}",
        "cHW": "c_{\\text{HW}}",
        "cQtjd1Re": "\\text{cQtjd1Re}",
        "cbd1": "c_{\\text{bd}}^1",
        "cjQbd1": "c_{\\text{jQbd}}^1",
    }
    return labels[sym][par]


def get_zero_xsec_pars(xsec_dict):
    zero_xsec = {}
    for prodmod, par_xsec_vals in xsec_dict.items():
        zero_xsec[prodmod] = []
        for par, val in par_xsec_vals.items():
            if val["xsec"]["val"] == 0.0:
                zero_xsec[prodmod].append(par)
            elif is_xsec_zero(val):
                zero_xsec[prodmod].append(par)

    return zero_xsec


def get_non_zero_xsec_pars(xsec_dict):
    non_zero_xsec = {}
    for prodmod, par_xsec_vals in xsec_dict.items():
        non_zero_xsec[prodmod] = [
            par
            for par, val in par_xsec_vals.items()
            if val["xsec"]["val"] != 0.0
            and abs(val["xsec"]["val"] / val["xsec"]["err"]) >= 2.0
        ]

    return non_zero_xsec

