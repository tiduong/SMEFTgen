import math
import sys
import numpy as np
import json,yaml
import os, yaml
import pprint

from SMEFTgen.stxs_defs import *

def isPoi(x):
    if "mu" in x.GetName():
        if "_YY" in x.GetName() or "_BB" in x.GetName() or "_ZZ" in x.GetName(): return True
    return False

def isCoeff(x):
    if "c" in x.GetName() and len(x.GetName()) < 7: return True
    return False

# average of list of tuples
def tupleaverage(list_of_tuples):
    import math
    import numpy as np
    N = len(list_of_tuples)
    if isinstance(list_of_tuples[0], float):
        val = float(np.average([x for x in list_of_tuples]))
        return {"val":val, "err":0.0}
    if isinstance(list_of_tuples[0], dict):
       val = float(np.average([x["val"] for x in list_of_tuples]))
       err = float(math.sqrt(np.average([x["err"]*x["err"] for x in list_of_tuples])*1/N))
       return {"val":val, "err":err}


# average of list of tuples
def tuplesum(list_of_tuples):
    import math
    import numpy as np
    N = len(list_of_tuples)

    for x in list_of_tuples:
        if "err" not in x or "val" not in x:
            print("ERROR: tuplesum called with tuple without error or val")
            print(x)
            sys.exit(1)
    val = float(N*np.average([x["val"] for x in list_of_tuples]))
    err = float(math.sqrt(N*np.average([x["err"]*x["err"] for x in list_of_tuples])))

    return {"val":val, "err":err}


# subtracting two central val, error tuple
def tuplesub(a,b):
    import math
    if len(a) == 0 and len(b) > 0:
        return b
    elif len(b) == 0 and len(a) > 0:
        return a    
    val = a["val"] - b["val"]
    err = math.sqrt(a["err"]*a["err"] + b["err"]*b["err"])
    return {"val":val,"err":err}

# adding two central val, error tuples
def tupleadd(a,b): 
    import math
    if len(a) == 0 and len(b) > 0:
        return b
    elif len(b) == 0 and len(a) > 0:
        return a
    val = a["val"] + b["val"]
    if "err" not in a:
        print(a)
    if "err" not in b:
        print(b)
    err = math.sqrt(a["err"]*a["err"] + b["err"]*b["err"])
    return {"val":val,"err":err}

# dividing two central val, error tuples
def tupledivide(a,b):
    import math
    if b["val"] == 0:
        return b
    val = a["val"]/b["val"]
    err = math.sqrt((a["err"]*a["err"])/(b["val"]*b["val"])+
    (b["err"]*b["err"]*a["val"]*a["val"])/(b["val"]*b["val"]*b["val"]*b["val"]))

    return {"val":val,"err":err}

# adding the predictions for two production modes
def addpars(vals1, vals2):
    samples = []
    for input0 in [vals1.keys(),vals2.keys()]:
        for sample in input0:
            if sample not in samples: samples.append(sample)
    vals = {}
    for sample in samples:
        vals[sample] = {}
        par_keys = vals1.keys()
        for stxsbin in vals1[par_keys[0]].keys():
            if sample in vals1.keys() and sample in vals2.keys():
                vals[sample][stxsbin]  = tupleadd(vals1[sample][stxsbin],vals2[sample][stxsbin])
            elif sample in vals1.keys():
                vals[sample][stxsbin]  = vals1[sample][stxsbin]
            elif sample in vals2.keys():
                vals[sample][stxsbin]  = vals2[sample][stxsbin]
    return vals

# print the parameterisations given the dict of dict of x-sec values
def printpar(poipardict, val_threshold=0.0, print_unc=True,rel_unc_threshold=None,select_param=[]):
    if rel_unc_threshold != None:
       print("Showing only parameterisation with ratio of param value / param err < {0}".format(rel_unc_threshold))
    for poi in sorted(poipardict.keys()):
        string = ""
        if len(select_param) : 
            eftpars = select_param
        else:
            eftpars = sorted(poipardict[poi]) 
        for par in eftpars :
            sign = " + "
            if poipardict[poi][par]["val"] < 0: sign = " - "
            if poipardict[poi][par]["val"] == 0 and poipardict[poi][par]["err"] == 0:
                continue
            if "SM" in sorted(poipardict[poi]): 
                normpar = tupledivide(poipardict[poi][par],poipardict[poi]["SM"])
            else: 
                normpar = poipardict[poi][par]
  
            if abs(normpar["val"]) < val_threshold: 
                continue

            if rel_unc_threshold != None:
                if abs(normpar["err"]) / abs(normpar["val"]) < rel_unc_threshold:
                    continue
                   
            if print_unc:
                val = "({0:.3f}+/-{1:.3f})".format(abs(normpar["val"]),abs(normpar["err"]))
            else:  val = "{0:.3f}".format(abs(normpar["val"]))
            string = "{0}{1}{2} {3}".format(string,sign,val,par)

        if len(string) != 0:
            print("{0} : {1}".format(poi.ljust(30), string))

# pad the parameterisation with zeros if the stxs doesnt depend on the wilson coeff.
def paddedpar(poipar):
    import copy
    poipar_padded = copy.deepcopy(poipar)
    for poi in sorted(poipar_padded.keys()):
        for par in allpars:
            if par not in poipar_padded[poi].keys():
                poipar_padded[poi][par] = {"val":0.0,"err":0.0}
    return poipar_padded

# given a TMatrix return an array
def tmat2arr(inmat):
    arr = []
    for icol in range(0,inmat.GetNcols()):
        tmp = []
        for irow in range(0,inmat.GetNrows()):
            tmp.append(inmat[irow][icol])
        arr.append(tmp)
    return arr


def correlation_from_covariance(covariance):
    import numpy as np
    v = np.sqrt(np.diag(covariance))
    outer_v = np.outer(v, v)
    correlation = covariance / outer_v
    correlation[covariance == 0] = 0
    return correlation

def getdiagmat(inarr):
    dim = len(inarr)
    diagmat = []
    for i1 in range(0,dim):
        tmparr = []
        for i2 in range(0,dim):
            if i1 == i2: tmparr.append(inarr[i1])
            else: tmparr.append(0.0)
        diagmat.append(tmparr)
    return diagmat

def getpaddmat(arrdict):
    paddmat = []
    for par in sorted(allpars):
        tmparr = []
        zerocount = 0
        for iev in range(0,len(allpars)):
            key = str(iev+1)
            if par not in arrdict[key].keys():
                zerocount = zerocount + 1
                arrdict[key][par] = 0.0
            tmparr.append(arrdict[key][par])
        paddmat.append(tmparr)
    return paddmat

def last_letter(word):
    return word[::-2]

def norm(mat, rows=True, cols= False):
    import numpy as np
    if cols: mat = np.transpose(mat)
    norm_mat = [[val/np.amax([abs(x) for x in row]) for val in row] for row in mat]
    if cols: norm_mat = np.transpose(norm_mat)
    return norm_mat
    
def geterr(val):
    print(1/math.sqrt(val))

def check_symmetric(a, rtol=1e-05, atol=1e-08):
    return np.allclose(a, a.T, rtol=rtol, atol=atol)

def getParName(int_string):

    if int_string:
        non_zero_params = varyEFTParam(int_string, 1.0)
        return  str(non_zero_params.keys()[0])

    else:
        return "SM"


def varyEFTParam(par, val):

#    print("Vary parameter nb. ", par)
    non_zero_params = dict()

    if par == 0:
        print("Compute SM-limit (all EFT param set to 0).")

    elif par == 1:
        non_zero_params={'cG':val}
    elif par == 2:
        non_zero_params={'cW':val}
    elif par == 3:
        non_zero_params={'cH':val}
    elif par == 4:
        non_zero_params={'cHbox':val}
    elif par == 5:
        non_zero_params={'cHDD':val}
    elif par == 6:
        non_zero_params={'cHG':val}
    elif par == 7:
        non_zero_params={'cHW':val}
    elif par == 8:
        non_zero_params={'cHB':val}
    elif par == 9:
        non_zero_params={'cHWB':val}
    elif par == 10:
        non_zero_params={'cuHRe':val}
    elif par == 11:
        non_zero_params={'ctHRe':val}
    elif par == 12:
        non_zero_params={'cdHRe':val}
    elif par == 13:
        non_zero_params={'cbHRe':val}
    elif par == 14:
        non_zero_params={'cuGRe':val}
    elif par == 15:
        non_zero_params={'ctGRe':val}
    elif par == 16:
        non_zero_params={'cuWRe':val}
    elif par == 17:
        non_zero_params={'ctWRe':val}
    elif par == 18:
        non_zero_params={'cuBRe':val}
    elif par == 19:
        non_zero_params={'ctBRe':val}
    elif par == 20:
        non_zero_params={'cdGRe':val}
    elif par == 21:
        non_zero_params={'cbGRe':val}
    elif par == 22:
        non_zero_params={'cdWRe':val}
    elif par == 23:
        non_zero_params={'cbWRe':val}
    elif par == 24:
        non_zero_params={'cdBRe':val}
    elif par == 25:
        non_zero_params={'cbBRe':val}
    elif par == 26:
        non_zero_params={'cHj1':val}
    elif par == 27:
        non_zero_params={'cHQ1':val}
    elif par == 28:
        non_zero_params={'cHj3':val}
    elif par == 29:
        non_zero_params={'cHQ3':val}
    elif par == 30:
        non_zero_params={'cHu':val}
    elif par == 31:
        non_zero_params={'cHt':val}
    elif par == 32:
        non_zero_params={'cHd':val}
    elif par == 33:
        non_zero_params={'cHbq':val}
    elif par == 34:
        non_zero_params={'cHudRe':val}
    elif par == 35:
        non_zero_params={'cHtbRe':val}
    elif par == 36:
        non_zero_params={'cjj11':val}
    elif par == 37:
        non_zero_params={'cjj18':val}
    elif par == 38:
        non_zero_params={'cjj31':val}
    elif par == 39:
        non_zero_params={'cjj38':val}
    elif par == 40:
        non_zero_params={'cQj11':val}
    elif par == 41:
        non_zero_params={'cQj18':val}
    elif par == 42:
        non_zero_params={'cQj31':val}
    elif par == 43:
        non_zero_params={'cQj38':val}
    elif par == 44:
        non_zero_params={'cQQ1':val}
    elif par == 45:
        non_zero_params={'cQQ8':val}
    elif par == 46:
        non_zero_params={'cuu1':val}
    elif par == 47:
        non_zero_params={'cuu8':val}
    elif par == 48:
        non_zero_params={'ctt':val}
    elif par == 49:
        non_zero_params={'ctu1':val}
    elif par == 50:
        non_zero_params={'ctu8':val}
    elif par == 51:
        non_zero_params={'cdd1':val}
    elif par == 52:
        non_zero_params={'cdd8':val}
    elif par == 53:
        non_zero_params={'cbb':val}
    elif par == 54:
        non_zero_params={'cbd1':val}
    elif par == 55:
        non_zero_params={'cbd8':val}
    elif par == 56:
        non_zero_params={'cud1':val}
    elif par == 57:
        non_zero_params={'ctb1':val}
    elif par == 58:
        non_zero_params={'ctd1':val}
    elif par == 59:
        non_zero_params={'cbu1':val}
    elif par == 60:
        non_zero_params={'cud8':val}
    elif par == 61:
        non_zero_params={'ctb8':val}
    elif par == 62:
        non_zero_params={'ctd8':val}
    elif par == 63:
        non_zero_params={'cbu8':val}
    elif par == 66:
        non_zero_params={'cju1':val}
    elif par == 67:
        non_zero_params={'cQu1':val}
    elif par == 68:
        non_zero_params={'cju8':val}
    elif par == 69:
        non_zero_params={'cQu8':val}
    elif par == 70:
        non_zero_params={'ctj1':val}
    elif par == 71:
        non_zero_params={'ctj8':val}
    elif par == 72:
        non_zero_params={'cQt1':val}
    elif par == 73:
        non_zero_params={'cQt8':val}
    elif par == 74:
        non_zero_params={'cjd1':val}
    elif par == 75:
        non_zero_params={'cjd8':val}
    elif par == 76:
        non_zero_params={'cQd1':val}
    elif par == 77:
        non_zero_params={'cQd8':val}
    elif par == 78:
        non_zero_params={'cbj1':val}
    elif par == 79:
        non_zero_params={'cbj8':val}
    elif par == 80:
        non_zero_params={'cQb1':val}
    elif par == 81:
        non_zero_params={'cQb8':val}
    elif par == 100:
        non_zero_params={'ceHRe':val}
    elif par == 101:
        non_zero_params={'ceWRe':val}
    elif par == 102:
        non_zero_params={'ceBRe':val}
    elif par == 103:
        non_zero_params={'cHl1':val}
    elif par == 104:
        non_zero_params={'cHl3':val}
    elif par == 105:
        non_zero_params={'cHe':val}
    elif par == 106:
        non_zero_params={'cll':val}
    elif par == 107:
        non_zero_params={'cll1':val}
    elif par == 108:
        non_zero_params={'clj1':val}
    elif par == 109:
        non_zero_params={'clj3':val}
    elif par == 110:
        non_zero_params={'cQl1':val}
    elif par == 111:
        non_zero_params={'cQl3':val}
    elif par == 112:
        non_zero_params={'cee':val}
    elif par == 113:
        non_zero_params={'ceu':val}
    elif par == 114:
        non_zero_params={'cte':val}
    elif par == 115:
        non_zero_params={'ced':val}
    elif par == 116:
        non_zero_params={'cbe':val}
    elif par == 117:
        non_zero_params={'cje':val}
    elif par == 118:
        non_zero_params={'cQe':val}
    elif par == 119:
        non_zero_params={'clu':val}
    elif par == 120:
        non_zero_params={'ctl':val}
    elif par == 121:
        non_zero_params={'cld':val}
    elif par == 122:
        non_zero_params={'cbl':val}
    elif par == 123:
        non_zero_params={'cle':val}

    else: 
        raise RuntimeError("runNumber %i not recognised in these jobOptions."%par)

    return non_zero_params

def readvals_from_ROOT(filename,histoname,apply_int_map,debug):
    import ROOT
    import pprint
    par = getPar(filename,apply_int_map,debug)
    vals = {stxsbin:{} for stxsbin in stxsdict}
    tfile  = ROOT.TFile.Open(filename)
    hist = tfile.Get(histoname)
    for stxsbin in stxsdict:
        binpos = stxsdict[stxsbin]%100 + offset[stxsdict[stxsbin]/100]
        vals[stxsbin][par] = {"val" : hist.GetBinContent(binpos+1), "err" : hist.GetBinError(binpos+1)}
    #pprint.pprint(vals)
    return vals

def readvals_from_ROOT_reweight(filename,histoname,apply_int_map,debug,stxsdict=stxsdict_fine):
    import yoda
    vals = {stxsbin:{} for stxsbin in stxsdict}
    tfile  = ROOT.TFile.Open(filename)
    folder = tfile.Get(histoname)
    for key in folder.GetListOfKeys():
        if "STXS_stage1_2_fine_pTjet30" in key.GetName():
            hist = folder.Get(key.GetName())
            par = key.GetName().split("[")[-1].split("_")[0].replace("]","")
            if par in ["AUX","STXS"] : continue
            for stxsbin in stxsdict:
                binpos = stxsdict[stxsbin]%100 + offset[stxsdict[stxsbin]/100]
                vals[stxsbin][par] = {"val" : hist.GetBinContent(binpos+1), "err" : hist.GetBinError(binpos+1)}

    return vals

def readvals_from_yoda(filename,histoname,apply_int_map,stxs_dict = stxsdict_fine,debug=False):
    import yoda
    par = getPar(filename,apply_int_map,debug)
    yodafile = yoda.read(filename)
    yodahist = yodafile[histoname]
    if len(stxs_dict):
        vals = {stxsbin:{} for stxsbin in stxs_dict}
        for stxsbin in stxs_dict:
            binpos = stxs_dict[stxsbin]%100 + offset[stxs_dict[stxsbin]/100]
            vals[stxsbin][par] = {"val" : yodahist.bin(binpos).sumW(), "err" : math.sqrt(yodahist.bin(binpos).sumW2())}
    else:
        vals = {str(binpos):{} for binpos in range(yodahist.numBins())}
        for binpos in range(yodahist.numBins()):
            vals[str(binpos)][par] = {"val" : yodahist.bin(binpos).sumW(), "err" : math.sqrt(yodahist.bin(binpos).sumW2())}
    #pprint.pprint(vals)
    return vals

def readvals_from_yoda_reweight(filename,histoname,apply_int_map,debug,stxsdict=stxsdict_fine):
    import yoda
    vals = {stxsbin:{} for stxsbin in stxsdict}
    yodafile = yoda.read(filename)
    yodahist = yodafile[histoname]
    for key in yodafile.keys():
        if "STXS_stage1_2_fine_pTjet30" in key:
            hist = yodafile[key]
            par = key.split("[")[-1].split("_")[0].replace("]","")
            neglect_par = False
            for remove_tag in ["AUX","STXS"] : 
                if remove_tag in par: neglect_par = True
                
            if neglect_par: continue
            for stxsbin in stxsdict:
                binpos = stxsdict[stxsbin]%100 + offset[stxsdict[stxsbin]/100]
                vals[stxsbin][par] = {"val" : hist.bin(binpos).sumW(), "err" : math.sqrt(yodahist.bin(binpos).sumW2())}

    return vals

def getPar(filename,apply_int_map=False,debug=False):
    filename = filename.split("/")[-1]
    filename = filename.split(".")[0]

    if apply_int_map:
        parcode = filename.split("_")[1]
        return getParName(int(parcode))

    if debug : print("Found parameter {0}".format(filename))
    return filename

def checkifdir(outfilename):
    outfilepath = os.path.split(outfilename)[0]
    if len(outfilepath) == 0:
        return

    if not os.path.isdir(outfilepath):
        print("Creating directory {0}".format(outfilepath))
        os.makedirs(outfilepath)

def writepar(vals,name,outfilename,threshold=1e-3):
    coefficient_terms = []
    for binname,binpar in vals.items():
        for par,val in binpar.items():
            coefficient_terms.append(par)

    coefficient_terms = sorted(list(set(coefficient_terms)))
    pardict = { str(binname):{} for binname,binpar in vals.items()}
    for binname,binpar in vals.items():
        for par,val in binpar.items():
            if "SM" not in binpar: 
                print("Not found SM in bin {0}".format(binname))
                sys.exit(1)
            normval = tupledivide(val, binpar["SM"])
            if par == "SM": continue
            if abs(normval["val"]) >= threshold :
                pardict[binname][par] = normval


    allbins = [x for x in pardict.keys()]
    for bin in allbins:
        if len(pardict[bin]) == 0:
            pardict.pop(bin)

    observables = [str(x) for x in sorted(pardict.keys())]
    skim_pars = list(set({par for obs,param in pardict.items() for par in param}))
    outdict = {"name" : name,
               "parameterisation": pardict,
               "coefficient terms": skim_pars,
               "observables": observables,
               "sm xsec": {binname : vals[binname]["SM"] for binname in pardict}
               }

    checkifdir(outfilename)

    with open(outfilename, "w") as f:
        if outfilename.endswith(".json"):
            json.dump(outdict, f, indent=2)
            print("Write parameterisation of {0} to {1}".format(name, outfilename))
        elif outfilename.endswith(".yaml") or outfilename.endswith(".yml"):
            yaml.dump(outdict, f, allow_unicode=False, default_flow_style=False)
            print("Write parameterisation of {0} to {1}".format(name, outfilename))
        else:
            print("Please provide yaml or json as output, Nothing written !")
            sys.exit()


def readvals_from_ROOT(filename,histoname,apply_int_map,debug,stxsdict=stxsdict_fine):
    import ROOT
    par = getPar(filename,apply_int_map,debug)
    vals = {stxsbin:{} for stxsbin in stxsdict}
    tfile  = ROOT.TFile.Open(filename)
    hist = tfile.Get(histoname)
    for stxsbin in stxsdict:
        binpos = stxsdict[stxsbin]%100 + offset[stxsdict[stxsbin]/100]
        vals[stxsbin][par] = {"val" : hist.GetBinContent(binpos+1), "err" : hist.GetBinError(binpos+1)}

    return vals


def getPar(filename,apply_int_map=False,debug=False):
    filename = filename.split("/")[-1]
    filename = filename.split(".")[0]
        
    if apply_int_map:
        parcode = filename.split("_")[1]
        return getParName(int(parcode))

    if debug : print("Found parameter {0}".format(filename))
    return filename

def checkifdir(outfilename):
    outfilepath = os.path.split(outfilename)[0]
    if len(outfilepath) == 0:
        return
        
    if not os.path.isdir(outfilepath):
        print("Creating directory {0}".format(outfilepath))
        os.makedirs(outfilepath)



def extract_cenval_err(line):
    line = line.split("Width :")[-1]
    unit = line.split(" ")[-1].replace("\n","")
    line = " ".join(line.split(" ")[0:-1])
    vals = [ (x.replace(" ","")) for x in line.split("+-")]
    vals = [ float(x.replace(" ","")) for x in line.split("+-")]
    return {"val":vals[0], "err":vals[1], "unit" : unit}

def extract_width_info(logfiles):
    allvals = {}

    for logfile in logfiles:
        case, mode = "",""
 #       if "SM" in logfile : 
 #           print(logfile)
        vals = {}
        for x in logfile.split("/"):
            if x.startswith("h_") : mode = x
            if "_int" in x or "_cross" in x or "_square" in x : case = x.split(".")[0]
            if case == "":
                case = "SM_SM" 

        tag = "{0}_{1}".format(mode, case)
        allvals[tag] = {}
        vals["case"] = case
#        print(case, mode)

        with open(logfile,"r") as f:
           #print("opening logfile {0}".format(logfile))
           lines = f.readlines()
        widthlines = [line for line in lines if "Width :" in line]
        if len(widthlines) == 0: 
            res = {"val":0.0, "err":0.0,"unit" :"GeV"}
        # just get the latest line in log file containing the width
        else: 
            widthlines = widthlines[-1]
            res = extract_cenval_err(widthlines)
        #print(logfile)
        vals["err"] = res["err"]
        vals["unit"] = res["unit"]
        vals["val"] = res["val"]
        allvals[tag]["mode"] = mode.replace("_","").upper()
        allvals[tag]["case"] = vals["case"].split("_")[-1]
        allvals[tag]["par"] = "*".join(vals["case"].split("_")[:-1])
        if allvals[tag]["case"] == "square" and len(vals["case"].split("_")[:-1]) == 1:
            allvals[tag]["par"] = "{0}*{0}".format(vals["case"].split("_")[:-1][0])
        if allvals[tag]["case"] == "cross" and len(vals["case"].split("_")[:-1]) == 2:
            allvals[tag]["par"] = "{0}".format(vals["case"].replace("_cross","").replace("_","*")) 
        allvals[tag]["err"] = res["err"]
        allvals[tag]["unit"] = res["unit"]
        allvals[tag]["val"] = res["val"]
        if res["val"] == 0.0:
            #print("popping because empty")
            allvals.pop(tag)
#    pprint.pprint(allvals)
    allpars  = list(set([allvals[tag]["par"] for tag in allvals]))
    allmodes = list(set([allvals[tag]["mode"] for tag in allvals]))
    cases = [tag for tag in allvals]
    br_vals = {
        "h_bb" :  0.5809  ,
        "h_gg" : 0.0818  ,
        "h_za" :  0.001541  ,
        "h_cc" :  0.02884  ,
        "h_aa" :  0.00227  ,
        "h_tautau" :  0.06256  ,
        "h_qqqq" : 0.1097  ,
        "h_ww_lvlv" : 0.005074 ,
        "h_vvtautau" : 0.0078,
        "h_eevv":  0.0078,
        "h_mmvv" :  0.0078,
        "h_tauvtauv" : 0.002786934688  ,
        "h_lvtauv" :  0.005226114784  ,
        "h_lvjj" :  0.0632,
        "h_tauvjj" :  0.01582,
        "h_4l" : 0.0001251  ,
        "h_4ta" : 0.0000353777796  ,
        "h_4nu" :  0.0010564  ,
        "h_lltautau" :  0.000059885695  ,
        "h_lljj" : 0.002466,
        "h_jjtautau" :0.001233,
        "h_vvjj" :  0.007381  ,
        "h_ss" :  0.0003  ,
        "h_ee" :  0.00000001  ,
        "h_mumu" :  0.0002171  ,
        }

#    lo_SM_vals = {run_case:val  for run_case,val in allvals.copy().items() if "SM_SM" in run_case}

    pprint.pprint(allvals.keys())
    for run_case in allvals:
        if "_SM_SM" in run_case:
            continue
        match = None
        for val in br_vals:
            if val + "_" in run_case:
                match = val
                print("found match for {0}".format(match))

        if match is None:
            print("not found match for {0}".format(run_case))
            sys.exit()

                    
        best_known_br = br_vals[match]
#        print("scaling {0} by {1}".format(run_case, best_known_br / lo_SM_vals[match+"_SM_SM"]["val"]) )
        allvals[run_case]['val'] = allvals[run_case]['val'] * best_known_br / allvals[match+"_SM_SM"]["val"]

    for run_case in allvals:
        if "_SM_SM" in run_case:
            match = None
            for val in br_vals:
                if val + "_" in run_case:
                    match = val
                    print("found match for {0}".format(match))

            if match is None:
                print("not found match for {0}".format(run_case))
                sys.exit()          
            best_known_br = br_vals[match]
#        print("scaling {0} by {1}".format(run_case, best_known_br / lo_SM_vals[match+"_SM_SM"]["val"]) )
            allvals[run_case]['val'] = allvals[run_case]['val'] * best_known_br / allvals[match+"_SM_SM"]["val"]

#    pprint.pprint(allvals)

    # This part is for the computation of the total width
    # loop over all the parameters, and sum the different cases
    for par in allpars:
        htot_tag = "HTOT_{0}_int".format(par)
        allvals[htot_tag] = {}
        allvals[htot_tag]["mode"] = "HTOT"
        allvals[htot_tag]["par"] = par
        allvals[htot_tag]["case"] = "int"
        res = tuplesum([{"val":allvals[tag]["val"],"err":allvals[tag]["err"]} for tag in cases if allvals[tag]["par"] == par ])
        param_check = 0.0
        if htot_tag == "HTOT_cHj3_int":
            for tag in cases:
                if allvals[tag]["par"] == par:
                    print(tag,allvals[tag]["val"], allvals[tag.replace("cHj3_int","SM_SM")]["val"])
                    param_check = param_check + allvals[tag]["val"]
                    
        print("param check",param_check)
        allvals[htot_tag]["val"] = res["val"]
        allvals[htot_tag]["err"] = res["err"]
        allvals[htot_tag]["unit"] = "GeV"
    
#    pprint.pprint(allvals)

    with open("test.json","w") as f:
        json.dump(allvals,f,indent=4)
        
    return allvals   

def load_map(infile,histoname):
    with open(infile,"r") as f:
        map = json.load(f)
    return map[histoname]

def load_param(paramfile):
    with open(paramfile) as f:
        data = yaml.safe_load(f)
    return data
