def getParName(int_string):

    non_zero_params = varyEFTParam(int_string, 1.0)

    return str(non_zero_params.keys()[0])

def varyEFTParam(par, val):

    print("Vary parameter nb. ", par)

    non_zero_params = dict()

    if par == 0:
        print("Compute SM-limit (all EFT param set to 0).")

    elif par == 1:
        non_zero_params={'cG':val}
    elif par == 2:
        non_zero_params={'cW':val}
    elif par == 3:
        non_zero_params={'cH':val}
    elif par == 4:
        non_zero_params={'cHbox':val}
    elif par == 5:
        non_zero_params={'cHDD':val}
    elif par == 6:
        non_zero_params={'cHG':val}
    elif par == 7:
        non_zero_params={'cHW':val}
    elif par == 8:
        non_zero_params={'cHB':val}
    elif par == 9:
        non_zero_params={'cHWB':val}
    elif par == 10:
        non_zero_params={'cuHRe':val}
    elif par == 11:
        non_zero_params={'ctHRe':val}
    elif par == 12:
        non_zero_params={'cdHRe':val}
    elif par == 13:
        non_zero_params={'cbHRe':val}
    elif par == 14:
        non_zero_params={'cuGRe':val}
    elif par == 15:
        non_zero_params={'ctGRe':val}
    elif par == 16:
        non_zero_params={'cuWRe':val}
    elif par == 17:
        non_zero_params={'ctWRe':val}
    elif par == 18:
        non_zero_params={'cuBRe':val}
    elif par == 19:
        non_zero_params={'ctBRe':val}
    elif par == 20:
        non_zero_params={'cdGRe':val}
    elif par == 21:
        non_zero_params={'cbGRe':val}
    elif par == 22:
        non_zero_params={'cdWRe':val}
    elif par == 23:
        non_zero_params={'cbWRe':val}
    elif par == 24:
        non_zero_params={'cdBRe':val}
    elif par == 25:
        non_zero_params={'cbBRe':val}
    elif par == 26:
        non_zero_params={'cHj1':val}
    elif par == 27:
        non_zero_params={'cHQ1':val}
    elif par == 28:
        non_zero_params={'cHj3':val}
    elif par == 29:
        non_zero_params={'cHQ3':val}
    elif par == 30:
        non_zero_params={'cHu':val}
    elif par == 31:
        non_zero_params={'cHt':val}
    elif par == 32:
        non_zero_params={'cHd':val}
    elif par == 33:
        non_zero_params={'cHbq':val}
    elif par == 34:
        non_zero_params={'cHudRe':val}
    elif par == 35:
        non_zero_params={'cHtbRe':val}
    elif par == 36:
        non_zero_params={'cjj11':val}
    elif par == 37:
        non_zero_params={'cjj18':val}
    elif par == 38:
        non_zero_params={'cjj31':val}
    elif par == 39:
        non_zero_params={'cjj38':val}
    elif par == 40:
        non_zero_params={'cQj11':val}
    elif par == 41:
        non_zero_params={'cQj18':val}
    elif par == 42:
        non_zero_params={'cQj31':val}
    elif par == 43:
        non_zero_params={'cQj38':val}
    elif par == 44:
        non_zero_params={'cQQ1':val}
    elif par == 45:
        non_zero_params={'cQQ8':val}
    elif par == 46:
        non_zero_params={'cuu1':val}
    elif par == 47:
        non_zero_params={'cuu8':val}
    elif par == 48:
        non_zero_params={'ctt':val}
    elif par == 49:
        non_zero_params={'ctu1':val}
    elif par == 50:
        non_zero_params={'ctu8':val}
    elif par == 51:
        non_zero_params={'cdd1':val}
    elif par == 52:
        non_zero_params={'cdd8':val}
    elif par == 53:
        non_zero_params={'cbb':val}
    elif par == 54:
        non_zero_params={'cbd1':val}
    elif par == 55:
        non_zero_params={'cbd8':val}
    elif par == 56:
        non_zero_params={'cud1':val}
    elif par == 57:
        non_zero_params={'ctb1':val}
    elif par == 58:
        non_zero_params={'ctd1':val}
    elif par == 59:
        non_zero_params={'cbu1':val}
    elif par == 60:
        non_zero_params={'cud8':val}
    elif par == 61:
        non_zero_params={'ctb8':val}
    elif par == 62:
        non_zero_params={'ctd8':val}
    elif par == 63:
        non_zero_params={'cbu8':val}
    elif par == 66:
        non_zero_params={'cju1':val}
    elif par == 67:
        non_zero_params={'cQu1':val}
    elif par == 68:
        non_zero_params={'cju8':val}
    elif par == 69:
        non_zero_params={'cQu8':val}
    elif par == 70:
        non_zero_params={'ctj1':val}
    elif par == 71:
        non_zero_params={'ctj8':val}
    elif par == 72:
        non_zero_params={'cQt1':val}
    elif par == 73:
        non_zero_params={'cQt8':val}
    elif par == 74:
        non_zero_params={'cjd1':val}
    elif par == 75:
        non_zero_params={'cjd8':val}
    elif par == 76:
        non_zero_params={'cQd1':val}
    elif par == 77:
        non_zero_params={'cQd8':val}
    elif par == 78:
        non_zero_params={'cbj1':val}
    elif par == 79:
        non_zero_params={'cbj8':val}
    elif par == 80:
        non_zero_params={'cQb1':val}
    elif par == 81:
        non_zero_params={'cQb8':val}
    elif par == 100:
        non_zero_params={'ceHRe':val}
    elif par == 101:
        non_zero_params={'ceWRe':val}
    elif par == 102:
        non_zero_params={'ceBRe':val}
    elif par == 103:
        non_zero_params={'cHl1':val}
    elif par == 104:
        non_zero_params={'cHl3':val}
    elif par == 105:
        non_zero_params={'cHe':val}
    elif par == 106:
        non_zero_params={'cll':val}
    elif par == 107:
        non_zero_params={'cll1':val}
    elif par == 108:
        non_zero_params={'clj1':val}
    elif par == 109:
        non_zero_params={'clj3':val}
    elif par == 110:
        non_zero_params={'cQl1':val}
    elif par == 111:
        non_zero_params={'cQl3':val}
    elif par == 112:
        non_zero_params={'cee':val}
    elif par == 113:
        non_zero_params={'ceu':val}
    elif par == 114:
        non_zero_params={'cte':val}
    elif par == 115:
        non_zero_params={'ced':val}
    elif par == 116:
        non_zero_params={'cbe':val}
    elif par == 117:
        non_zero_params={'cje':val}
    elif par == 118:
        non_zero_params={'cQe':val}
    elif par == 119:
        non_zero_params={'clu':val}
    elif par == 120:
        non_zero_params={'ctl':val}
    elif par == 121:
        non_zero_params={'cld':val}
    elif par == 122:
        non_zero_params={'cbl':val}
    elif par == 123:
        non_zero_params={'cle':val}

    else: 
        raise RuntimeError("runNumber %i not recognised in these jobOptions."%par)

    return non_zero_params
