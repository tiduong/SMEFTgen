#!/bin/bash

WORKDIR=$(pwd)
SETUPSOURCEDIR=$(cd -P -- "$(dirname -- "$0")" && printf '%s\n' "$(pwd -P)")
cd $WORKDIR

if [ $# -lt 1 ]
then
    echo "Usage: source $SETUPSOURCEDIR/setup.sh [working folder] [grid (optional)]"
else
    if [[ -d $3 ]]; then
        SETUPSOURCEDIR="$(cd $3; pwd)"
    fi
    WORKTESTDIR=$(mkdir -p $1; cd $1; pwd)
    echo "setting up in $WORKTESTDIR from $SETUPSOURCEDIR"
    if [ -n "$WORKTESTDIR" ]  && [ -d $WORKTESTDIR ]; then
	      echo "setting up athena in $WORKTESTDIR"
	      cd $WORKTESTDIR	      
	      export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
	      source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
              #setupATLAS -c slc6

              source $AtlasSetup/scripts/asetup.sh AthGeneration,21.6.97,here
           #   source $AtlasSetup/scripts/asetup.sh AthGeneration,21.6.73,here 
	   #   cd $SETUPSOURCEDIR/Classification
              source setupRivet.sh
           #   rivet-build RivetHiggsTemplateCrossSections.so HiggsTemplateCrossSections.cc
            #  lsetup "python 3.8.13-fix1-x86_64-centos7"
            #  export ALRB_rootVersion=6.26.04-x86_64-centos7-gcc11-opt
            #  lsetup root
	     mkdir -p $WORKTESTDIR/test
             cd $WORKTESTDIR/test
             echo "linking to $SETUPSOURCEDIR"
             #ln -s $SETUPSOURCEDIR/code
	     ln -s $SETUPSOURCEDIR/MG5aMC_PLUGIN
             ln -s $SETUPSOURCEDIR/models
 #            cp -r $SETUPSOURCEDIR/SMEFT ./
             #ln -s $SETUPSOURCEDIR/scripts/submit.py

	     #export PYTHONPATH="${PYTHONPATH}:$SETUPSOURCEDIR/:$SETUPSOURCEDIR/MG5aMC_PLUGIN"
	     export JOBOPTSEARCHPATH=$SETUPSOURCEDIR:$JOBOPTSEARCHPATH

#             for file in $(ls -1 $SETUPSOURCEDIR/models/); do
#                 ln -s $SETUPSOURCEDIR/models/$file
#             done
        
	      function aversion(){
	          if [ -d $AtlasArea ]; then
		            echo ${AtlasArea##*/}
	          else
		            echo "?.?.?.?"
	          fi
	      }
	      
	      echo "successfully setup the working environment under athena $(aversion)"
    fi
fi
