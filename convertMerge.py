import os, sys

# Usage: python convertToCERNBatch.py <script_name>.sh
# Produces batch inputs in directory: batch_submission/<script_name>/
# To submit to CERN batch:
# cd batch_submission/<script_name>/
# condor_submit batch_<script_name>.submit

if(len(sys.argv)<2):
    print("Need to specify a submission script as input!")
    exit()

script_name = sys.argv[1]
batch_dir = 'batch_submission/'+script_name[:-3]+'/Merge'

if(not os.path.exists('batch_submission')):
    os.mkdir('batch_submission')

if(not os.path.exists(batch_dir)):
    os.mkdir(batch_dir)

script = open(script_name,'r')

job_list = []

for line in script:
    
    if(line[0:4] == 'bsub'):
        
        

        job_id = line.split()[2][28:-3] 
        # add job_id to job_list.txt
        job_list.append(job_id)

        # find start of quotation marks
             # find start of quotation marks
        strt = line.find('"source')
        command = line[strt:]
        command = command[1:-2]
 
 
                
        # write to .sh file
        out = open(batch_dir+'/'+job_id+'.sh','w')
        out.write('#!/bin/bash \n')
        out.write('cd /afs/cern.ch/user/t/tiduong/private/SMEFTgen/ \n')
        out.write('source build/setup.sh \n')
        out.write(command)
        out.close()


# write jobs to job_list.txt
jlist = open(batch_dir+'/job_list.txt','w')
for job in job_list:
    jlist.write(job+'\n')
jlist.close()


submit="""
universe       = vanilla
executable     = $(jobname).sh
output         = $(jobname).out
error          = $(jobname).err
log            = $(jobname).log
stream_error   = True
stream_output  = True
+JobFlavour    = "espresso"
requirements   = OpSysAndVer == "CentOS7"
JobBatchName   = jobs_test.$(ClusterId).$(ProcId)
queue jobname from job_list.txt
"""

submission = open(batch_dir+'/batch_'+'Merge_'+script_name[:-3]+'.submit','w')
submission.write(submit)
submission.close()

print("Made batch submission scripts in: "+batch_dir+'/')
